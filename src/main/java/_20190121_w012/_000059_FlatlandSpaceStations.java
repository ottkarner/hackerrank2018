package _20190121_w012;

import java.util.Arrays;

public class _000059_FlatlandSpaceStations {

    public static void main(String[] args) {
        System.out.println(flatlandSpaceStations(10, new int[]{5}));
        System.out.println(flatlandSpaceStationsEffective(10, new int[]{5}));
        System.out.println(flatlandSpaceStations(10, new int[]{0, 9}));
        System.out.println(flatlandSpaceStationsEffective(10, new int[]{0, 9}));
        System.out.println(flatlandSpaceStations(10, new int[]{3, 8, 9}));
        System.out.println(flatlandSpaceStationsEffective(10, new int[]{3, 8, 9}));
        System.out.println(flatlandSpaceStations(6, new int[]{0, 1, 2, 4, 3, 5}));
        System.out.println(flatlandSpaceStationsEffective(6, new int[]{0, 1, 2, 4, 3, 5}));
    }

    private static int flatlandSpaceStationsEffective(int n, int[] c) {
        Arrays.sort(c);
        int maxDistanceToNearestStation = c[0];
        for (int i = 1; i < c.length; i++) {
            int distance = (c[i] - c[i - 1]) / 2;
            if (maxDistanceToNearestStation < distance) maxDistanceToNearestStation = distance;
        }
        int lastDistance = (n - 1) - c[c.length - 1];
        return (lastDistance < maxDistanceToNearestStation) ? maxDistanceToNearestStation : lastDistance;
    }

    // Complete the flatlandSpaceStations function below.
    private static int flatlandSpaceStations(int n, int[] c) {
        Arrays.sort(c);
        int maxDistanceToNearestStation = 0;
        for (int city = 0; city < n; city++) {
            int distanceToNearestStation = findDistanceToNearestStation(city, c);
//            System.out.println("City:" + city + " -> nearest station:" + distanceToNearestStation);
            maxDistanceToNearestStation = Math.max(distanceToNearestStation, maxDistanceToNearestStation);
        }
        return maxDistanceToNearestStation;
    }

    private static int findDistanceToNearestStation(int city, int[] stations) {
        int distanceToNearestStation = Math.abs(city - stations[0]);

        // Kui linnas on esimene kosmosejaam või linn jääb sellest ette poole (sh. ka juht, kus kosmosejaamu ainult üks)
        if (city <= stations[0]) return Math.abs(city - stations[0]);

        // Kui linnas on viimane kosmosejaam või linn jääb sellest taha poole (sh. ka juht, kus kosmosejaamu ainult üks)
        if (city >= stations[stations.length - 1]) return Math.abs(city - stations[stations.length - 1]);

        for (int i = 0; i < stations.length; i++) {

            /*// Kui kosmosejaamu on ainult üks
            if (stations.length == 1) return Math.abs(city - stations[i]);*/

            // Kui selgub, et linnas on kosmosejaam, on see lähim, mis olla saab
            if (city == stations[i]) return 0;

            // Linnad, mis jäävad kahe kosmosejaama vahele
            if (city > stations[i] && city < stations[i + 1] && i < stations.length - 1 && stations.length > 1) {
                int distance1 = Math.abs(city - stations[i]);
                int distance2 = Math.abs(city - stations[i + 1]);
                return Math.min(distance1, distance2);
            }

        }

        return distanceToNearestStation;
    }

}
