package _20190121_w012;

public class _000060_FairRations {

    public static void main(String[] args) {
        System.out.println(fairRations(new int[]{2, 3}));
        System.out.println(fairRations(new int[]{2, 3, 4, 5, 6}));

        System.out.println(fairRations2(new int[]{2, 3}));
        System.out.println(fairRations2(new int[]{2, 3, 4, 5, 6}));
    }

    // Complete the fairRations function below.
    private static String fairRations(int[] B) {
        int minimumNumberOfLoaves = 0;
        for (int i = 0; i < B.length - 1; i++) {
            if (B[i] % 2 != 0) {
                B[i]++;
                B[i + 1]++;
                minimumNumberOfLoaves += 2;
            }
        }
        if (B[B.length - 1] % 2 != 0) return "NO";
        return String.valueOf(minimumNumberOfLoaves);
    }

    private static String fairRations2(int[] B) {
        int minimumNumberOfLoaves = 0;
        int sum = 0;
        for (int i = 0; i < B.length; i++) {
            sum += B[i];
            if (sum % 2 != 0) {
                minimumNumberOfLoaves += 2;
            }
        }
        if (sum % 2 != 0) return "NO";
        return String.valueOf(minimumNumberOfLoaves);
    }

}
