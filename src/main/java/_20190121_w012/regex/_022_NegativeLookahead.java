package _20190121_w012.regex;

public class _022_NegativeLookahead {

    public static void main(String[] args) {
        RegexTester regexTester = new RegexTester();
        String regexPattern = "(.)(?!\\1)";

        regexTester.checkerCount("gooooo", regexPattern);
        regexTester.checkerCount("###$$$$", regexPattern);
    }

}
