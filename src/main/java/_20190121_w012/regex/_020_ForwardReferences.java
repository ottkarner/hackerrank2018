package _20190121_w012.regex;

public class _020_ForwardReferences {

    public static void main(String[] args) {
        RegexTester regexTester = new RegexTester();
        String regexPattern = "^(\\2tic|(tac)){2,}$";

        regexTester.checker("tactactic", regexPattern);
        regexTester.checker("tactactictactic", regexPattern);
        regexTester.checker("tactactictactictictac", regexPattern);
        regexTester.checker("tactictac", regexPattern);
    }

}
