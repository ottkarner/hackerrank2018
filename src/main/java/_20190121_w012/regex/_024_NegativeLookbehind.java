package _20190121_w012.regex;

public class _024_NegativeLookbehind {

    public static void main(String[] args) {
        RegexTester regexTester = new RegexTester();
        String regexPattern = "(?<![aeiouAEIOU]).";

        regexTester.checkerCount("1o1s", regexPattern);
    }

}
