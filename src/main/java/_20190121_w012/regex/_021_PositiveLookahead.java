package _20190121_w012.regex;

public class _021_PositiveLookahead {

    public static void main(String[] args) {
        RegexTester regexTester = new RegexTester();
        String regexPattern = "o(?=oo)";

        regexTester.checkerCount("goooo!", regexPattern);
    }

}
