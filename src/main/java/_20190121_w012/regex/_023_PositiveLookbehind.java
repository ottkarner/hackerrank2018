package _20190121_w012.regex;

public class _023_PositiveLookbehind {

    public static void main(String[] args) {
        RegexTester regexTester = new RegexTester();
        String regexPattern = "(?<=[13579])\\d";

        regexTester.checkerCount("123Go!", regexPattern);
    }

}
