package _20190121_w012.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class _019_BackreferencesToFailedGroups {

    public static void main(String[] args) {
        RegexTester regexTester = new RegexTester();
        String regexPattern = "^\\d{2}(-?)(\\d{2}\\1){2}\\d{2}$";

        regexTester.checker("12345678", regexPattern);
        regexTester.checker("12-34-56-78", regexPattern);
        regexTester.checker("12-45-7810", regexPattern);

    }

}

class RegexTester {

    public void checker(String testString, String regexPattern) {
        Pattern p = Pattern.compile(regexPattern);
        Matcher m = p.matcher(testString);
        System.out.println(m.find());
    }

    public void checkerCount(String testString, String regexPattern) {
        Pattern p = Pattern.compile(regexPattern);
        Matcher m = p.matcher(testString);
        int count = 0;
        while (m.find()) count++;
        System.out.println("Number of matches: " + count);
    }

}