package _20190101_w009.javaproficiency;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class _038_JavaBigDecimal {

    public static void main(String[] args) {
        //Input
        String[] s = {"-100", "50", "0", "56.6", "90", "0.12", ".12", "02.34", "000.000", null, null};
        int n = s.length - 2;
/*
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        String[] s = new String[n + 2];
        for (int i = 0; i < n; i++) {
            s[i] = sc.next();
        }
        sc.close();
*/
        //Write your code here
        // sort1(s);
        // sort2(s);
        sort3(s);
        // sort4(s, n);

        //Output
        for (int i = 0; i < n; i++) {
            System.out.println(s[i]);
        }
    }

    private static void sort1(String[] s) {
        for (int i = 0; i < s.length; i++) {
            for (int j = 0; j < s.length; j++) {
                if (s[j] != null && s[j + 1] != null && !s[j].isEmpty() && !s[j + 1].isEmpty()) {
                    if (new BigDecimal(s[j]).compareTo(new BigDecimal(s[j + 1])) == -1) {
                        String temp = s[j];
                        s[j] = s[j + 1];
                        s[j + 1] = temp;
                    }
                }
            }
        }
    }

    private static void sort2(String[] s) {
        Arrays.sort(s, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                if (o1 == null || o2 == null) {
                    return 0;
                }
                BigDecimal o1bd = new BigDecimal(o1);
                BigDecimal o2bd = new BigDecimal(o2);
                return o2bd.compareTo(o1bd);
            }
        });
    }

    private static void sort3(String[] s) {
        Arrays.sort(s, new BigDCompare());
    }

    private static void sort4(String[] s, int n) {
        Comparator<String> customComparator = new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                BigDecimal a = new BigDecimal(s1);
                BigDecimal b = new BigDecimal(s2);
                return b.compareTo(a); // descending order
            }
        };

        Arrays.sort(s, 0, n, customComparator);
    }

}

class BigDCompare implements Comparator<String> {

    public int compare(String a, String b) {
        if (a == null) return 1;
        if (b == null) return -1;
        return (new BigDecimal(b)).compareTo(new BigDecimal(a));
    }

}