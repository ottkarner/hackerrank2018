package _20190101_w009.javaproficiency;

import java.util.HashSet;
import java.util.Set;

public class _039_JavaHashset {

    public static void main(String[] args) {
/*
        Scanner s = new Scanner(System.in);
        int t = s.nextInt();
        String[] pair_left = new String[t];
        String[] pair_right = new String[t];

        for (int i = 0; i < t; i++) {
            pair_left[i] = s.next();
            pair_right[i] = s.next();
        }
*/
        String[] pair_left = {"john", "john", "john", "mary", "mary"};
        String[] pair_right = {"tom", "mary", "tom", "anna", "anna"};

        Set<String> pairs = new HashSet<>();
        for (int i = 0; i < pair_left.length; i++) {
            pairs.add(pair_left[i] + " " + pair_right[i]);
            System.out.println(pairs.size());
        }
    }

}
