package _20190101_w009.javaproficiency;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class _050_JavaComparator {

    public static void main(String[] args) {
     /* Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();

        Player[] player = new Player[n];
        Checker checker = new Checker();

        for (int i = 0; i < n; i++) {
            player[i] = new Player(scan.next(), scan.nextInt());
        }
        scan.close(); */

        Player[] player = {
                new Player("amy", 100),
                new Player("david", 100),
                new Player("heraldo", 50),
                new Player("aakansha", 75),
                new Player("aleksa", 150)
        };

//         Arrays.sort(player, new Checker());
        Arrays.sort(player, new Checker2());

        for (int i = 0; i < player.length; i++) {
            System.out.printf("%s %s\n", player[i].name, player[i].score);
        }
    }

}

class Player {
    String name;
    int score;

    Player(String name, int score) {
        this.name = name;
        this.score = score;
    }
}

class Checker implements Comparator<Player> {
    @Override
    public int compare(Player o1, Player o2) {
        return o1.score < o2.score ? 1 : o1.score > o2.score ? -1 : o1.name.compareTo(o2.name);
    }
}

class Checker2 implements Comparator<Player> {
    @Override
    public int compare(Player player1, Player player2) {
        return Comparator.comparingInt((Player p) -> p.score)
                .reversed()
                .thenComparing((Player p) -> p.name)
                .compare(player1, player2);
    }
}