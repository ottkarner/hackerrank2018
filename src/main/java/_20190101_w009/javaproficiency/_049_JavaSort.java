package _20190101_w009.javaproficiency;

import java.util.*;

public class _049_JavaSort {

    public static void main(String[] args) {
/*
        Scanner in = new Scanner(System.in);
        int testCases = Integer.parseInt(in.nextLine());
*/

        List<Student2> studentList = new ArrayList<Student2>();
/*
        while (testCases > 0) {
            int id = in.nextInt();
            String fname = in.next();
            double cgpa = in.nextDouble();

            Student2 st = new Student2(id, fname, cgpa);
            studentList.add(st);

            testCases--;
        }
*/
        studentList.add(new Student2(33, "Rumpa", 3.68));
        studentList.add(new Student2(85, "Ashis", 3.85));
        studentList.add(new Student2(56, "Samiha", 3.75));
        studentList.add(new Student2(19, "Samara", 3.75));
        studentList.add(new Student2(22, "Fahim", 3.76));

//        sort0(studentList);
        sort1(studentList);
//        sort2(studentList);
//        sort3(studentList);
//        sort4(studentList);

        for (Student2 st : studentList) {
            System.out.println(st.getFname());
        }
    }

    private static void sort0(List<Student2> studentList) {
        Collections.sort(studentList, new Comparator<Student2>() {
            @Override
            public int compare(Student2 o1, Student2 o2) {
                int cgpaC = Double.compare(o1.getCgpa(), o2.getCgpa());
                if (cgpaC != 0) {
                    return -cgpaC;
                } else {
                    int fNameC = o1.getFname().compareTo(o2.getFname());
                    if (fNameC != 0) {
                        return fNameC;
                    } else {
                        int getIdC = Integer.compare(o1.getId(), o2.getId());
                        if (getIdC != 0) {
                            return getIdC;
                        } else {
                            return 0;
                        }
                    }
                }
            }
        });
    }

    private static void sort1(List<Student2> studentList) {
        Collections.sort(studentList, new Comparator<Student2>() {
            @Override
            public int compare(Student2 o1, Student2 o2) {
                if (o1.getCgpa() > o2.getCgpa()) {
                    return -1;
                } else if (o1.getCgpa() < o2.getCgpa()) {
                    return 1;
                } else {
                    int fNameOrder = o1.getFname().compareTo(o2.getFname());
                    if (fNameOrder != 0) {
                        return fNameOrder;
                    } else {
                        if (o1.getId() > o2.getId()) {
                            return 1;
                        } else if (o1.getId() < o2.getId()) {
                            return -1;
                        } else {
                            return 0;
                        }
                    }
                }
            }
        });
    }

    private static void sort2(List<Student2> studentList) {
        Collections.sort(studentList, Comparator.comparing(Student2::getCgpa).reversed()
                .thenComparing(Student2::getFname)
                .thenComparing(Student2::getId)
        );
    }

    private static void sort3(List<Student2> studentList) {
        Collections.sort(studentList, Comparator.comparingDouble(Student2::getCgpa).reversed()
                .thenComparing(Student2::getFname)
                .thenComparingInt(Student2::getId)
        );
    }

    private static void sort4(List<Student2> studentList) {
        studentList.sort(
                (s1, s2) -> s1.getId() - s2.getId());
        studentList.sort(
                (s1, s2) -> s1.getFname().compareTo(s2.getFname()));
        studentList.sort(
                (Comparator.comparing(Student2::getCgpa)).reversed());
    }

}

class Student2 {

    private int id;
    private String fname;
    private double cgpa;

    public Student2(int id, String fname, double cgpa) {
        super();
        this.id = id;
        this.fname = fname;
        this.cgpa = cgpa;
    }

    public int getId() {
        return id;
    }

    public String getFname() {
        return fname;
    }

    public double getCgpa() {
        return cgpa;
    }

}