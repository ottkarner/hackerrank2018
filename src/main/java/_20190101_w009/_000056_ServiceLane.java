package _20190101_w009;

import java.util.Arrays;

public class _000056_ServiceLane {

    public static void main(String[] args) {
        int[] width = {2, 3, 1, 2, 3, 2, 3, 3};
        int[][] cases = {{0, 3}, {4, 6}, {6, 7}, {3, 5}, {0, 7},};
        System.out.println(Arrays.toString(serviceLane(width, cases)));
    }

    // Complete the serviceLane function below.
    static int[] serviceLane(int[] width, int[][] cases) {
        int[] result = new int[cases.length];
        for (int i = 0; i < cases.length; i++) {
            int widthMin = Integer.MAX_VALUE;
            for (int j = cases[i][0]; j <= cases[i][1]; j++) {
                widthMin = widthMin > width[j] ? widthMin = width[j] : widthMin;
            }
            result[i] = widthMin;
        }
        return result;
    }

}
