package _20190101_w009;

public class _000057_LisasWorkbook {

    public static void main(String[] args) {
        System.out.println(workbookClean(3, new int[]{4, 2, 6, 1, 10}));
        System.out.println(workbook2(3, new int[]{4, 2, 6, 1, 10}));
        System.out.println(workbookClean(5, new int[]{3, 8, 15, 11, 14, 1, 9, 2, 24, 31}));
        System.out.println(workbook2(5, new int[]{3, 8, 15, 11, 14, 1, 9, 2, 24, 31}));
    }

    // Complete the workbook function below.
    private static int workbook(int k, int[] arr) {
        int count = 0;
        int chapterPagesCount = 0;
        int chapterBeginPage = 0;
        int chapterEndPage = 0;
        for (int i = 0; i < arr.length; i++) {
            chapterBeginPage = chapterEndPage + 1;
//            chapterPagesCount = arr[i] >= k ? arr[i] / k + arr[i] % k : 1;
            chapterPagesCount = arr[i] % k == 0 ? arr[i] / k : arr[i] / k + 1;
//            System.out.println("Ch" + (i + 1) + " has " + chapterPagesCount + "pages");
            chapterEndPage = chapterBeginPage + chapterPagesCount - 1;
            for (int page = chapterBeginPage; page <= chapterEndPage; page++) {
//                if (arr[i] >= chapterBeginPage) {
                for (int problem = chapterBeginPage; problem <= arr[i]; problem++) {
                    int problemPage = problem % k == 0 ?
                            chapterBeginPage + problem / k - 1 :
                            chapterBeginPage + problem / k;
                    if (page == problemPage && page == problem) {
                        count++;
                        System.out.println("Ch " + (i + 1) +
                                " -> (pages " + chapterBeginPage + "-" + chapterEndPage + ")" +
                                " -> observable page: " + page + " -> observable problem: " + problem);
                    }
                }
//                }
            }
        }
        return count;
    }

    private static int workbookClean(int k, int[] arr) {
        int count = 0;
        int chapterPagesCount = 0;
        int chapterBeginPage = 0;
        int chapterEndPage = 0;
        for (int i = 0; i < arr.length; i++) {
            chapterBeginPage = chapterEndPage + 1;
            chapterPagesCount = arr[i] % k == 0 ? arr[i] / k : arr[i] / k + 1;
            chapterEndPage = chapterBeginPage + chapterPagesCount - 1;
            for (int page = chapterBeginPage; page <= chapterEndPage; page++) {
                for (int problem = chapterBeginPage; problem <= arr[i]; problem++) {
                    int problemPage = problem % k == 0 ?
                            chapterBeginPage + problem / k - 1 :
                            chapterBeginPage + problem / k;
                    if (page == problemPage && page == problem) {
                        count++;
                    }
                }
            }
        }
        return count;
    }

    private static int workbook2(int k, int[] arr) {
        int page = 1;
        int count = 0;
        for (int a : arr) {
            for (int i = 1; i <= a; i++) {
                if (i == page) count++;
                if (i % k == 0) page++;
            }
            if (a % k != 0) page++;
        }
        return count;
    }

}
