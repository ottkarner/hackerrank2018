package _20190128_w013.regex;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class _027_FindASubWord {

    public static void main(String[] args) {
//        task();
        try {
            taskUsingScanner();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void task() {
        String[] lines = {
                "existing pessimist optimist this is"
        };
        System.out.println(
                findOccurrencesOfSubWord(lines, "is")
        );
    }

    private static void taskUsingScanner() throws IOException {
        BufferedReader bufferedReader = Files.newBufferedReader(
                Paths.get("C:\\Users\\devel\\Desktop\\hackerrank\\20190128_w013\\regex\\" +
                        "27__find-substring-English_input01.txt"
                )
        );
        Scanner scanner = new Scanner(bufferedReader);

        int n = Integer.valueOf(scanner.nextLine());
        String[] lines = new String[n];
        for (int i = 0; i < n; i++) {
            lines[i] = scanner.nextLine();
        }

        int q = Integer.valueOf(scanner.nextLine());
        String subWords[] = new String[q];
        for (int i = 0; i < q; i++) {
            subWords[i] = scanner.nextLine();
        }

        scanner.close();
        bufferedReader.close();

        for (String subWord : subWords) {
            System.out.println(
                    findOccurrencesOfSubWord(lines, subWord)
            );
        }
    }

    private static int findOccurrencesOfSubWord(String[] lines, String subWord) {
        String regexPattern = "[^\\s\\W]+" + subWord + "[^\\s\\W]+";
        Pattern pattern = Pattern.compile(regexPattern);

        int count = 0;
        for (String line : lines) {
            Matcher matcher = pattern.matcher(line);
            while (matcher.find()) {
                String wordWithSubWord = matcher.group();
                System.out.println(wordWithSubWord);
                count++;
            }
        }

        return count;
    }

}
