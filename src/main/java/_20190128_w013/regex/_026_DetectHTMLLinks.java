package _20190128_w013.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class _026_DetectHTMLLinks {

    public static void main(String[] args) {
        String[] lines = {"<a href=\"http://www.hackerrank.com\"><h1>HackerRank</h1></a>",
                "<a href=\"http://www.hackerrank.com\"><h1><b>HackerRank</b></h1></a>"
        };

        String[] lines1 = {
                "<p><a href=\"http://www.quackit.com/html/tutorial/html_links.cfm\">Example Link</a></p>",
                "<div class=\"more-info\"><a href=\"http://www.quackit.com/html/examples/html_links_examples.cfm\">More Link Examples...</a></div>"
        };

        String[] lines2 = {
                "<center>",
                "<div class=\"noresize\" style=\"height: 242px; width: 600px; \"><map name=\"ImageMap_1_971996215\" id=\"ImageMap_1_971996215\">",
                "<area href=\"/wiki/File:Pardalotus_punctatus_female_with_nesting_material_-_Risdon_Brook.jpg\" shape=\"rect\" coords=\"3,3,297,238\" alt=\"Female\" title=\"Female\" />",
                "<area href=\"/wiki/File:Pardalotus_punctatus_male_with_nesting_material_-_Risdon_Brook.jpg\" shape=\"rect\" coords=\"302,2,597,238\" alt=\"Male\" title=\"Male\" /></map><img alt=\"Pair of Spotted Pardalotes\" src=\"//upload.wikimedia.org/wikipedia/commons/thumb/5/5b/Female_and_male_Pardalotus_punctatus.jpg/600px-Female_and_male_Pardalotus_punctatus.jpg\" width=\"600\" height=\"242\" srcset=\"//upload.wikimedia.org/wikipedia/commons/thumb/5/5b/Female_and_male_Pardalotus_punctatus.jpg/900px-Female_and_male_Pardalotus_punctatus.jpg 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/5/5b/Female_and_male_Pardalotus_punctatus.jpg/1200px-Female_and_male_Pardalotus_punctatus.jpg 2x\" usemap=\"#ImageMap_1_971996215\" />",
                "<div style=\"margin-left: 0px; margin-top: -20px; text-align: left;\"><a href=\"/wiki/File:Female_and_male_Pardalotus_punctatus.jpg\" title=\"About this image\"><img alt=\"About this image\" src=\"//bits.wikimedia.org/static-1.22wmf7/extensions/ImageMap/desc-20.png\" style=\"border: none;\" /></a></div>",
                "</div>",
                "</center>"
        };

        String[] lines3 = {
                "<div class=\"portal\" role=\"navigation\" id='p-lang'>",
                "<h3>Languages</h3>",
                "<div class=\"body\">",
                "<ul>",
                "<li class=\"interwiki-simple\"><a href=\"//simple.wikipedia.org/wiki/\" title=\"\" lang=\"simple\" hreflang=\"simple\">Simple English</a></li>",
                "<li class=\"interwiki-ar\"><a href=\"//ar.wikipedia.org/wiki/\" title=\"\" lang=\"ar\" hreflang=\"ar\"></a></li>",
                "<li class=\"interwiki-id\"><a href=\"//id.wikipedia.org/wiki/\" title=\"\" lang=\"id\" hreflang=\"id\">Bahasa Indonesia</a></li>",
                "<li class=\"interwiki-ms\"><a href=\"//ms.wikipedia.org/wiki/\" title=\"\" lang=\"ms\" hreflang=\"ms\">Bahasa Melayu</a></li>",
                "<li class=\"interwiki-bg\"><a href=\"//bg.wikipedia.org/wiki/\" title=\"\" lang=\"bg\" hreflang=\"bg\"></a></li>",
                "<li class=\"interwiki-ca\"><a href=\"//ca.wikipedia.org/wiki/\" title=\"\" lang=\"ca\" hreflang=\"ca\">Catal</a></li>",
                "<li class=\"interwiki-cs\"><a href=\"//cs.wikipedia.org/wiki/\" title=\"\" lang=\"cs\" hreflang=\"cs\">esky</a></li>",
                "<li class=\"interwiki-da\"><a href=\"//da.wikipedia.org/wiki/\" title=\"\" lang=\"da\" hreflang=\"da\"><b>Dansk</b></a></li>",
                "<li class=\"interwiki-de\"><a href=\"//de.wikipedia.org/wiki/\" title=\"\" lang=\"de\" hreflang=\"de\">Deutsch</a></li>",
                "<li class=\"interwiki-et\"><a href=\"//et.wikipedia.org/wiki/\" title=\"\" lang=\"et\" hreflang=\"et\">Eesti</a></li>",
                "<li class=\"interwiki-el\"><a href=\"//el.wikipedia.org/wiki/\" title=\"\" lang=\"el\" hreflang=\"el\"></a></li>",
                "<li class=\"interwiki-es\"><a href=\"//es.wikipedia.org/wiki/\" title=\"\" lang=\"es\" hreflang=\"es\">Espaol</a></li>",
                "<li class=\"interwiki-eo\"><a href=\"//eo.wikipedia.org/wiki/\" title=\"\" lang=\"eo\" hreflang=\"eo\">Esperanto</a></li>",
                "<li class=\"interwiki-eu\"><a href=\"//eu.wikipedia.org/wiki/\" title=\"\" lang=\"eu\" hreflang=\"eu\">Euskara</a></li>",
                "<li class=\"interwiki-fa\"><a href=\"//fa.wikipedia.org/wiki/\" title=\"\" lang=\"fa\" hreflang=\"fa\"></a></li>",
                "<li class=\"interwiki-fr\"><a href=\"//fr.wikipedia.org/wiki/\" title=\"\" lang=\"fr\" hreflang=\"fr\">Franais</a></li>",
                "<li class=\"interwiki-gl\"><a href=\"//gl.wikipedia.org/wiki/\" title=\"\" lang=\"gl\" hreflang=\"gl\">Galego</a></li>",
                "<li class=\"interwiki-ko\"><a href=\"//ko.wikipedia.org/wiki/\" title=\"\" lang=\"ko\" hreflang=\"ko\"></a></li>",
                "<li class=\"interwiki-he\"><a href=\"//he.wikipedia.org/wiki/\" title=\"\" lang=\"he\" hreflang=\"he\"></a></li>",
                "<li class=\"interwiki-hr\"><a href=\"//hr.wikipedia.org/wiki/\" title=\"\" lang=\"hr\" hreflang=\"hr\">Hrvatski</a></li>",
                "<li class=\"interwiki-it\"><a href=\"//it.wikipedia.org/wiki/\" title=\"\" lang=\"it\" hreflang=\"it\">Italiano</a></li>",
                "<li class=\"interwiki-lt\"><a href=\"//lt.wikipedia.org/wiki/\" title=\"\" lang=\"lt\" hreflang=\"lt\">Lietuvi</a></li>",
                "<li class=\"interwiki-hu\"><a href=\"//hu.wikipedia.org/wiki/\" title=\"\" lang=\"hu\" hreflang=\"hu\">Magyar</a></li>",
                "<li class=\"interwiki-nl\"><a href=\"//nl.wikipedia.org/wiki/\" title=\"\" lang=\"nl\" hreflang=\"nl\">Nederlands</a></li>",
                "<li class=\"interwiki-ja\"><a href=\"//ja.wikipedia.org/wiki/\" title=\"\" lang=\"ja\" hreflang=\"ja\"></a></li>",
                "<li class=\"interwiki-no\"><a href=\"//no.wikipedia.org/wiki/\" title=\"\" lang=\"no\" hreflang=\"no\">Norsk bokml</a></li>",
                "<li class=\"interwiki-nn\"><a href=\"//nn.wikipedia.org/wiki/\" title=\"\" lang=\"nn\" hreflang=\"nn\">Norsk nynorsk</a></li>",
                "<li class=\"interwiki-pl\"><a href=\"//pl.wikipedia.org/wiki/\" title=\"\" lang=\"pl\" hreflang=\"pl\">Polski</a></li>",
                "<li class=\"interwiki-pt\"><a href=\"//pt.wikipedia.org/wiki/\" title=\"\" lang=\"pt\" hreflang=\"pt\">Portugus</a></li>",
                "<li class=\"interwiki-ro\"><a href=\"//ro.wikipedia.org/wiki/\" title=\"\" lang=\"ro\" hreflang=\"ro\">Romn</a></li>",
                "<li class=\"interwiki-ru\"><a href=\"//ru.wikipedia.org/wiki/\" title=\"\" lang=\"ru\" hreflang=\"ru\"></a></li>",
                "<li class=\"interwiki-sk\"><a href=\"//sk.wikipedia.org/wiki/\" title=\"\" lang=\"sk\" hreflang=\"sk\">Slovenina</a></li>",
                "<li class=\"interwiki-sl\"><a href=\"//sl.wikipedia.org/wiki/\" title=\"\" lang=\"sl\" hreflang=\"sl\">Slovenina</a></li>",
                "<li class=\"interwiki-sr\"><a href=\"//sr.wikipedia.org/wiki/\" title=\"\" lang=\"sr\" hreflang=\"sr\"> / srpski</a></li>",
                "<li class=\"interwiki-sh\"><a href=\"//sh.wikipedia.org/wiki/\" title=\"\" lang=\"sh\" hreflang=\"sh\">Srpskohrvatski / </a></li>",
                "<li class=\"interwiki-fi\"><a href=\"//fi.wikipedia.org/wiki/\" title=\"\" lang=\"fi\" hreflang=\"fi\">Suomi</a></li>",
                "<li class=\"interwiki-sv\"><a href=\"//sv.wikipedia.org/wiki/\" title=\"\" lang=\"sv\" hreflang=\"sv\">Svenska</a></li>",
                "<li class=\"interwiki-th\"><a href=\"//th.wikipedia.org/wiki/\" title=\"\" lang=\"th\" hreflang=\"th\"></a></li>",
                "<li class=\"interwiki-vi\"><a href=\"//vi.wikipedia.org/wiki/\" title=\"\" lang=\"vi\" hreflang=\"vi\">Ting Vit</a></li>",
                "<li class=\"interwiki-tr\"><a href=\"//tr.wikipedia.org/wiki/\" title=\"\" lang=\"tr\" hreflang=\"tr\">Trke</a></li>",
                "<li class=\"interwiki-uk\"><a href=\"//uk.wikipedia.org/wiki/\" title=\"\" lang=\"uk\" hreflang=\"uk\"></a></li>",
                "<li class=\"interwiki-zh\"><  a   href  =   \"//zh.wikipedia.org/wiki/\" title=\"\" lang=\"zh\" hreflang=\"zh\">  dsafs   <  /a   ></li>",
                "</ul>",
                "</div>",
                "</div>"
        };

        printOutLinksAndLinksTexts2(lines);
        printOutLinksAndLinksTexts2(lines1);
        printOutLinksAndLinksTexts2(lines2);
        printOutLinksAndLinksTexts2(lines3);
    }

    private static void printOutLinksAndLinksTexts(String[] lines) {
//        String regexPattern = "<a href=\"([^\"]*)\"([^>]*)?>(<([^>]+)>)*?\\s*([^<]*?)(<\\/\\4>)*?<\\/a>";
        // The following two are the same.
        // The difference from the last regex is that they allow space and doesn't capture space in the beginning and in the end of text.
        // The problem at the moment is that if there are more than one tag between a-tag and the text then the text will not be captured
//        String regexPattern = "<\\s*a\\s*href\\s*=\\s*\"([^\"]*)\"([^>]*)?>(<\\s*([^>]+)\\s*>)*?\\s*([^<]*?)\\s*(<\\s*\\/\\4\\s*>)*?<\\s*\\/a\\s*>";
        String regexPattern = "<\\s*a\\s*" +
                "href\\s*=\\s*" +
                "\"([^\"]*)\"" +
                "([^>]*)?>" +
                "(<\\s*([^>]+)\\s*>)*?" +
                "\\s*([^<]*?)\\s*" +
                "(<\\s*\\/\\4\\s*>)*?" +
                "<\\s*\\/a\\s*>";

        Pattern pattern = Pattern.compile(regexPattern);

        for (String line : lines) {
            Matcher matcher = pattern.matcher(line);
            while (matcher.find()) {
                String link = matcher.group(1);
                String text = matcher.group(5);
                System.out.println(link + "," + text.trim());
            }
        }
    }

    private static void printOutLinksAndLinksTexts2(String[] lines) {
//        String regexPattern = "<a href=\"([^\"]*)\"([^>]*)?>(<.*?>)*?\\s*([^><]*?)\\s*(<\\/.*?>)*?<\\/a>";
        // The following two are the same.
        // The difference from the last regex is that they allow space and doesn't capture space in the beginning and in the end of text.
        // This regex captures text even if there are more than one tag between a-tag and the text
//        String regexPattern = "<\\s*a\\s*href\\s*=\\s*\"([^\"]*)\"([^>]*)?>(<.*?>)*?\\s*([^><]*?)\\s*(<\\/.*?>)*?<\\s*\\/a\\s*>";
        String regexPattern = "<\\s*a\\s*" +
                "href\\s*=\\s*" +
                "\"([^\"]*)\"" +
                "([^>]*)?>" +
                "(<.*?>)*?" +
                "\\s*([^><]*?)\\s*" +
                "(<\\/.*?>)*?" +
                "<\\s*\\/a\\s*>";

        Pattern pattern = Pattern.compile(regexPattern);

        for (String line : lines) {
            Matcher matcher = pattern.matcher(line);
            while (matcher.find()) {
                String link = matcher.group(1);
                String text = matcher.group(4);
                System.out.println(link + "," + text.trim());
            }
        }
    }

}
