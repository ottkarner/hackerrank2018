package _20190128_w013.regex;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class _025_DetectHTMLTags {

    public static void main(String[] args) {
        String[] lines = {
                "<p><t></t></p>",
                "<o i=\"dsl jfsl\"></o>",
                "<p><a href=\"http://www.quackit.com/html/tutorial/html_links.cfm\">Example Link</a></p>",
                "<div class=\"more-info\"><a href=\"http://www.quackit.com/html/examples/html_links_examples.cfm\">More Link Examples...</a></div>"
        };

//        String regexPattern = "<(\\w+)( \\w+=\\\".*\\\")?>.*?<\\/\\1?>|<\\w+\\/>";
        String regexPattern = "<\\b(\\w+)\\b.*?>";
//        String regexPattern = "<(\\\"[^\\\"]*\\\"|'[^']*'|[^'\\\">])*>";
        Pattern pattern = Pattern.compile(regexPattern);

        List<String> tags = new ArrayList<>();

        for (String line : lines) {
//            System.out.println(line);
            Matcher matcher = pattern.matcher(line);
            while (matcher.find()) {
                String tag = matcher.group(1);
//                if (!tags.contains(tag)) {
                tags.add(tag);
//                }
            }
        }

        Collections.sort(tags);

        StringBuilder result = new StringBuilder();
        for (String tag : tags) result.append(tag).append(";");
        result.deleteCharAt(result.length() - 1);

        System.out.println(result);
    }

}

