package _20190128_w013;

public class _000061_CavityMap {

    public static void main(String[] args) {
        printMap(cavityMap(new String[]{
                "989",
                "191",
                "111"
        }));

        printMap(cavityMap(new String[]{
                "1112",
                "1912",
                "1892",
                "1234"
        }));
    }

    // Complete the cavityMap function below.
    private static String[] cavityMap(String[] grid) {
        int[][] intGrid = new int[grid.length][grid.length];

        for (int i = 0; i < grid.length; i++) {
            String line = grid[i];
            for (int j = 0; j < grid.length; j++) {
                intGrid[i][j] = Integer.valueOf(line.charAt(j));
            }
        }

        for (int i = 1; i < intGrid.length - 1; i++) {
            StringBuilder line = new StringBuilder(grid[i]);
            boolean isXDiscovered = false;

            for (int j = 1; j < intGrid.length - 1; j++) {
                int current = intGrid[i][j];

                if (current > intGrid[i][j - 1] && current > intGrid[i][j + 1] &&
                        current > intGrid[i - 1][j] && current > intGrid[i + 1][j]
                ) {
                    isXDiscovered = true;
                    line.setCharAt(j, 'X');
                    j++;
                }

                if (isXDiscovered) {
                    grid[i] = line.toString();
                }
            }
        }

        return grid;
    }

    private static void printMap(String[] cavityMap) {
        for (String s : cavityMap) {
            System.out.println(s);
        }
        System.out.println();
    }

}
