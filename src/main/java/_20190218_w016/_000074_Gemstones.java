package _20190218_w016;

public class _000074_Gemstones {

    public static void main(String[] args) {
        String[] strings = {"abcdde", "baccd", "eeabg"};

        System.out.println(gemstones(strings));
    }

    // Complete the gemstones function below.
    static int gemstones(String[] arr) {
        int gemstonesCount = 0;
        String alphabet = "abcdefghijklmnopqrstuvwxyz";

        loop1:
        for (int i = 0; i < alphabet.length(); i++) {
            char checkChar = alphabet.charAt(i);

            for (String s : arr) {
                if (s.indexOf(checkChar) == -1) {
                    continue loop1;
                }
            }

            gemstonesCount++;
        }

        return gemstonesCount;
    }

}
