package _20190218_w016.regex;

import java.util.Scanner;

public class _031_FindHackerRank {

    public static void main(String[] args) {
        String[] lines = {
                "i love hackerrank",
                "hackerrank is an awesome place for programmers",
                "hackerrank",
                "i think hackerrank is a great place to hangout"
        };
        for (String line : lines) {
            System.out.println(findHackerRank(line));
        }
    }

    private static void taskWithScanner() {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.valueOf(scanner.nextLine());
        String[] strings = new String[n];
        for (int i = 0; i < n; i++) {
            strings[i] = scanner.nextLine();
        }
        scanner.close();
        for (String string : strings) {
            System.out.println(findHackerRank(string));
        }
    }

    private static int findHackerRank(String line) {
        if (line.matches("^.+hackerrank.+$")) return -1;
        if (line.matches("^(hackerrank)(.*\\1)?$")) return 0;
        if (line.matches(".*(?<!hackerrank)$")) return 1;
        if (line.matches("^(?!hackerrank).*")) return 2;
        return -1;
    }

}
