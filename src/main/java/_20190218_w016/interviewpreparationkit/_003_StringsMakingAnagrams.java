package _20190218_w016.interviewpreparationkit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class _003_StringsMakingAnagrams {

    public static void main(String[] args) {
        System.out.println(makeAnagram("cde", "dcf"));
        System.out.println(makeAnagramEffective("cde", "dcf"));
        System.out.println(makeAnagram("cde", "abc"));
        System.out.println(makeAnagramEffective("cde", "abc"));
    }

    static int makeAnagramEffective(String a, String b) {
        int minimumDeletions = 0;

        int minimumDeletionsByChars[] = new int[26];

        for (char c : a.toCharArray()) minimumDeletionsByChars[c - 'a']++;
        for (char c : b.toCharArray()) minimumDeletionsByChars[c - 'a']--;

        for (int minimumDeletionsByChar : minimumDeletionsByChars) {
            minimumDeletions += Math.abs(minimumDeletionsByChar);
        }

        return minimumDeletions;
    }

    // Complete the makeAnagram function below.
    static int makeAnagram(String a, String b) {
        int minimumDeletions = 0;

        Set<Character> characterSet = provideCharacterSet(a, b);

        List<Character> characterListA = provideCharacterList(a);
        List<Character> characterListB = provideCharacterList(b);

        for (Character character : characterSet) {
            int characterFrequencyInListA = Collections.frequency(characterListA, character);
            int characterFrequencyInListB = Collections.frequency(characterListB, character);
            minimumDeletions += Math.abs(characterFrequencyInListA - characterFrequencyInListB);
        }

        return minimumDeletions;
    }

    private static Set<Character> provideCharacterSet(String... strings) {
        Set<Character> result = new HashSet<>();
        for (String string : strings) {
            for (char c : string.toCharArray()) {
                result.add(c);
            }
        }
        return result;
    }

    private static List<Character> provideCharacterList(String s) {
        List<Character> result = new ArrayList<>();
        for (char c : s.toCharArray()) {
            result.add(c);
        }
        return result;
    }

}
