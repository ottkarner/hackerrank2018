package _20190218_w016;

public class _000075_TheLoveLetterMystery {

    public static void main(String[] args) {
        String[] strings = {"cde", "abc", "abcba", "abcd", "cba"};
        for (String string : strings) {
            System.out.println(string + " " + theLoveLetterMystery(string));
        }
    }

    // Complete the theLoveLetterMystery function below.
    static int theLoveLetterMystery(String s) {
        int operationsCount = 0;

        int middle = s.length() % 2 == 0 ? s.length() / 2 : s.length() / 2 + 1;
//        System.out.print(s + " -> middle=" + middle);

        for (int i = middle; i < s.length(); i++) {
//            System.out.print(" (" + (s.length() - i - 1) + "," + i + ") ");
            char charInFirstHalf = s.charAt(s.length() - i - 1);
            char charInSecondHalf = s.charAt(i);
            int charsDifference = Math.abs(charInFirstHalf - charInSecondHalf);
//            System.out.print("characters difference=" + charsDifference);
            operationsCount += charsDifference;
        }

        return operationsCount;
    }

}
