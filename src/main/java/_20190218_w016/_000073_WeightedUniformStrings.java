package _20190218_w016;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class _000073_WeightedUniformStrings {

    public static void main(String[] args) {
        String strings1 = "abccddde";
        int[] queries1 = {1, 3, 12, 5, 9, 10};
        System.out.println(
                Arrays.toString(weightedUniformStrings(strings1, queries1))
        );

        String strings2 = "aaabbbbcccddd";
        int[] queries2 = {9, 7, 8, 12, 5};
        System.out.println(
                Arrays.toString(weightedUniformStrings(strings2, queries2))
        );
    }

    // Complete the weightedUniformStrings function below.
    static String[] weightedUniformStrings(String s, int[] queries) {
        String[] result = new String[queries.length];

        Set<Integer> weights = createWeightsSet(s);

        for (int i = 0; i < queries.length; i++) {
            result[i] = weights.contains(queries[i]) ? "Yes" : "No";
        }

        return result;
    }

    private static Set<Integer> createWeightsSet(String s) {
        Set<Integer> weights = new HashSet<>();

        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        int currentCharWeight = 0;

        for (int i = 0; i < s.length(); i++) {
            char currentChar = s.charAt(i);

            if (i > 0 && currentChar == s.charAt(i - 1)) {
                currentCharWeight += alphabet.indexOf(currentChar) + 1;
            } else {
                currentCharWeight = alphabet.indexOf(currentChar) + 1;
            }

            weights.add(currentCharWeight);
        }

        return weights;
    }

}
