package _20190311_w019.regex;

public class _037_DetectingValidLatitudeAndLongitudePairs {

    public static void main(String[] args) {
        String[] strings = {
                "(75, 180)",
                "(+90.0, -147.45)",
                "(77.11112223331, 149.99999999)",
                "(+90, +180)",
                "(90, 180)",
                "(-90.00000, -180.0000)",
                "(75, 280)",
                "(+190.0, -147.45)",
                "(77.11112223331, 249.99999999)",
                "(+90, +180.2)",
                "(90., 180.)",
                "(-090.00000, -180.0000)"
        };
        for (String string : strings) {
            System.out.println(validate(string));
        }
    }

    private static String validate(String string) {
        return string.matches(
                "^\\((\\+|-)?(90(\\.0+)?|([1-8][0-9]|\\d)(\\.\\d+)?), " +
                        "(\\+|-)?(180(\\.0+)?|(1[0-7][0-9]|\\d\\d?)(\\.\\d+)?)\\)$") ?
                "VALID" : "INVALID";
    }

}
