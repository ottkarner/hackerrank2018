package _20190311_w019.datastructures;

import java.util.Arrays;

public class _002_LeftRotation {

    public static void main(String[] args) {
        int d = 1;
        int[] a = {1, 2, 3, 4, 5};

        int[] rotatedA = new int[a.length];
        for (int i = 0; i < a.length; i++) {
            rotatedA[(i + (a.length - d)) % a.length] = a[i];
        }
        a = rotatedA;

        for (int i : a) {
            System.out.print(i + " ");
        }
    }

}