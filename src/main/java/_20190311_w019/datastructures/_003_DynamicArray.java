package _20190311_w019.datastructures;

import java.util.ArrayList;
import java.util.List;

public class _003_DynamicArray {

    public static void main(String[] args) {
        int n = 2, q = 5;
        String[] strings = {
                "1 0 5",
                "1 1 7",
                "1 0 3",
                "2 1 0",
                "2 1 1"
        };

        List<List<Integer>> queries = new ArrayList<>();

        for (String string : strings) {
            List<Integer> integerList = new ArrayList<>();
            for (String valueFromString : string.split(" ")) {
                integerList.add(Integer.valueOf(valueFromString));
            }
            queries.add(integerList);
        }

        /*IntStream.range(0, q).forEach(i -> {
            queries.add(
                    Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                            .map(Integer::parseInt)
                            .collect(toList())
            );
        });*/

        for (Integer integer : dynamicArray(n, queries)) {
            System.out.println(integer);
        }
    }

    // Complete the dynamicArray function below.
    static List<Integer> dynamicArray(int n, List<List<Integer>> queries) {
        List<Integer> result = new ArrayList<>();

        List<List<Integer>> seqList = new ArrayList<>();
        while (seqList.size() < n) {
            seqList.add(new ArrayList<>());
        }
        int lastAnswer = 0;

        for (int i = 0; i < queries.size(); i++) {
            List<Integer> query = queries.get(i);

            Integer queryType = query.get(0);
            Integer x = query.get(1);
            Integer y = query.get(2);

            List<Integer> seq = seqList.get((x ^ lastAnswer) % n);
            if (queryType == 1) {
                seq.add(y);
            }
            if (queryType == 2) {
                lastAnswer = seq.get(y % seq.size());
                result.add(lastAnswer);
            }
        }

        return result;
    }

}