package _20190311_w019.datastructures;

import java.util.Arrays;

public class _001_ArraysDS {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(reverseArray(new int[]{1, 2, 5, 8})));
    }

    // Complete the reverseArray function below.
    static int[] reverseArray(int[] a) {
        int[] reversedA = new int[a.length];
        for (int i = 0; i < a.length; i++) {
            reversedA[(a.length - 1) - i] = a[i];
        }
        return reversedA;
    }

}
