package _20190204_w014;

public class _000066_HackerRankInAString {

    public static void main(String[] args) {
        String[] words = {
                "hereiamstackerrank",
                "hhaacckkekraraannk",
                "rhbaasdndfsdskgbfefdbrsdfhuyatrjtcrtyytktjjt"
        };

        for (String word : words) {
            System.out.println(hackerrankInString(word));
        }
    }

    // Complete the hackerrankInString function below.
    static String hackerrankInString(String s) {
        String regex = "^\\w*h\\w*a\\w*c\\w*k\\w*e\\w*r\\w*r\\w*a\\w*n\\w*k\\w*$";
        return s.matches(regex) ? "YES" : "NO";
    }

}
