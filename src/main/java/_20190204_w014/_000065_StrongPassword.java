package _20190204_w014;

public class _000065_StrongPassword {

    public static void main(String[] args) {
        System.out.println(minimumNumber(5, "12a"));
        System.out.println(minimumNumber(5, "0abcdA"));
        System.out.println(minimumNumber(5, "AUzs-nV"));
    }

    // Complete the minimumNumber function below.
    private static int minimumNumber(int n, String password) {
        // Return the minimum number of characters to make the password strong
        int requiredLengthGap = 6 - password.length();
        int missingCharacterTypeCounter = 0;

        if (!password.matches("^.*\\d.*$")) missingCharacterTypeCounter++;
        if (!password.matches("^.*[a-z].*$")) missingCharacterTypeCounter++;
        if (!password.matches("^.*[A-Z].*$")) missingCharacterTypeCounter++;
        if (!password.matches("^.*[!@#$%^&*()\\-+].*$")) missingCharacterTypeCounter++;
        /*if (!password.matches("^(?=.*\\d).*$")) missingCharacterTypeCounter++;
        if (!password.matches("^(?=.*[a-z]).*$")) missingCharacterTypeCounter++;
        if (!password.matches("^(?=.*[A-Z]).*$")) missingCharacterTypeCounter++;
        if (!password.matches("^(?=.*[!@#$%^&*()\\-+]).*$")) missingCharacterTypeCounter++;*/

        return missingCharacterTypeCounter < requiredLengthGap ?
                requiredLengthGap : missingCharacterTypeCounter;
    }

}
