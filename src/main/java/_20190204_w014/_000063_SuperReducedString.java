package _20190204_w014;

public class _000063_SuperReducedString {

    public static void main(String[] args) {
        String[] strings = {
                "aaabccddd",
                "aa",
                "baab",
                "a",
                "aaa"
        };

        for (String string : strings) {
            System.out.println(superReducedString(string));
        }
    }

    static String superReducedString(String s) {
        StringBuilder stringBuilder = new StringBuilder(s);
        boolean checkAgain = true;
        int currentLength = stringBuilder.length();
        int i = 0;
        while (currentLength > 0) {
            if (i < currentLength - 1) {
                if (stringBuilder.charAt(i) == stringBuilder.charAt(i + 1)) {
                    stringBuilder.deleteCharAt(i + 1).deleteCharAt(i);
                    currentLength -= 2;
                    checkAgain = true;
                }
            }
            if (i < currentLength - 2) {
                i++;
            } else {
                if (checkAgain) {
                    checkAgain = false;
                    i = 0;
                } else {
                    break;
                }
            }
        }
        return currentLength > 0 ? stringBuilder.toString() : "Empty String";
    }

}