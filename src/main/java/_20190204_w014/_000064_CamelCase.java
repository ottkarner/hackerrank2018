package _20190204_w014;

public class _000064_CamelCase {

    public static void main(String[] args) {
        String[] strings = {
                "oneTwoThree",
                "saveChangesInTheEditor",
                ""
        };

        for (String string : strings) {
            System.out.println(camelcase(string));
            System.out.println(camelcase2(string));
            System.out.println(camelcase3(string));
            System.out.println(camelcase4(string));
        }
    }

    // Complete the camelcase function below.
    private static int camelcase(String s) {
        int wordsCount = 0;
        int wordLength = s.length();
        if (wordLength > 0) {
            wordsCount++;
            for (int i = 0; i < wordLength; i++) {
                if (Character.isUpperCase(s.charAt(i))) wordsCount++;
                // char currentChar = s.charAt(i);
                /*if (String.valueOf(currentChar).equals(
                        String.valueOf(currentChar).toUpperCase())) {
                    wordsCount++;
                }*/
                /*if (s.substring(i, i + 1).equals(
                        s.substring(i, i + 1).toUpperCase())) {
                    wordsCount++;
                }*/
            }

        }
        return wordsCount;
    }

    private static int camelcase2(String s) {
        return s.length() > 0 ? s.length() - s.replaceAll("[A-Z]", "").length() + 1 : 0;
    }

    private static int camelcase3(String s) {
        return s.length() > 0 ? s.split("[A-Z]").length : 0;
    }

    private static int camelcase4(String s) {
        return s.length() > 0 ? (int) (s.chars().filter(c -> Character.isUpperCase((char) c)).count() + 1) : 0;
    }

}
