package _20190204_w014.regex;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class _029_HackerRankTweets {

    public static void main(String[] args) {
        task();
        taskWithScanner();
    }

    private static void taskWithScanner() {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.valueOf(scanner.nextLine());
        String[] tweets = new String[n];
        for (int i = 0; i < n; i++) {
            tweets[i] = scanner.nextLine();
        }
        scanner.close();
        System.out.println(countHackerRankTweets(tweets));
    }

    private static void task() {
        String[] tweets = {
                "I love #hackerrank",
                "I just scored 27 points in the Picking Cards challenge on #HackerRank",
                "I just signed up for summer cup @hackerrankhackerrank",
                "interesting talk by hari, co-founder of hackerrank"
        };
        System.out.println(countHackerRankTweets(tweets));
    }

    private static int countHackerRankTweets(String[] tweets) {
        String regex = ".*hackerrank.*";
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        int count = 0;
        for (String tweet : tweets) {
            Matcher matcher = pattern.matcher(tweet);
            if (matcher.find()) {
                count++;
            }
        }
        return count;
    }

}
