package _20190204_w014.regex;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class _030_IPAddressValidation {

    public static void main(String[] args) {
        String[] lines = {
                "This line has junk text.",
                "121.18.19.20",
                "2001:0db8:0000:0000:0000:ff00:0042:8329"
        };

        for (String line : lines) {
            System.out.println(ipAddressType(line));
        }
    }

    private static void taskWithScanner() {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.valueOf(scanner.nextLine());
        String[] strings = new String[n];
        for (int i = 0; i < n; i++) {
            strings[i] = scanner.nextLine();
        }
        scanner.close();
        for (String string : strings) {
            System.out.println(ipAddressType(string));
        }
    }

    private static String ipAddressType(String string) {
//        String regexIPv4 = "([0-1]*[0-9]*[0-9].|2[0-5][0-6].){3}([0-1]*[0-9]*[1-9]|2[0-5][1-6])";
        String regexIPv4 = "^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$";
        String regexIPv6 = "^([0-9a-fA-F]{0,4}:){1,7}[0-9a-fA-F]{0,4}$";

        if (string.matches(regexIPv4)) return "IPv4";
        if (string.matches(regexIPv6)) return "IPv6";

        return "Neither";
    }

}


