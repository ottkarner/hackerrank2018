package _20190204_w014.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class _028_AlienUsername {

    public static void main(String[] args) {
        task();
    }

    private static void task() {
        String[] names = {
                "_0898989811abced_",
                "_abce",
                "_09090909abcD0"
        };

        printOutAlienNamesValidity(names);
    }

    private static void printOutAlienNamesValidity(String[] names) {
        String regex = "^[_.][0-9]+[a-zA-Z]*_?$";
        Pattern pattern = Pattern.compile(regex);

        for (String name : names) {
            Matcher matcher = pattern.matcher(name);
            System.out.println(matcher.matches() ? "VALID" : "INVALID");
        }
    }

}
