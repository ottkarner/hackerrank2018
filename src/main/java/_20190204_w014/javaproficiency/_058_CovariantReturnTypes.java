package _20190204_w014.javaproficiency;

public class _058_CovariantReturnTypes {

    public static void main(String[] args) {
        System.out.println(new Region().yourNationalFlower().whatsYourName());
        System.out.println(new WestBengal().yourNationalFlower().whatsYourName());
        System.out.println(new AndhraPradesh().yourNationalFlower().whatsYourName());

        printOutFlowerName(new Region());
        printOutFlowerName(new WestBengal());
        printOutFlowerName(new AndhraPradesh());
    }

    private static void printOutFlowerName(Region region) {
        Flower flower = region.yourNationalFlower();
        System.out.println(flower.whatsYourName());
    }

}

//Complete the classes below
class Flower {
    String whatsYourName() {
        return "I have many names and types";
    }
}

class Jasmine extends Flower {
    @Override
    String whatsYourName() {
        return "Jasmine";
    }
}

class Lily extends Flower {
    @Override
    String whatsYourName() {
        return "Lily";
    }
}

class Region {
    Flower yourNationalFlower() {
        return new Flower();
    }
}

class WestBengal extends Region {
    @Override
    Jasmine yourNationalFlower() {
        return new Jasmine();
    }
}

class AndhraPradesh extends Region {
    @Override
    Lily yourNationalFlower() {
        return new Lily();
    }
}