package _20181110_w001;

public class _000004_DiagonalDifference {

    public static void main(String[] args) {
        int matrix[][] = {{11, 2, 4},
                           {4, 5, 6},
                          {10, 8, -12}};
        System.out.println(diagonalDifference(matrix));
    }

    // Complete the diagonalDifference function below.
    static int diagonalDifference(int[][] arr) {
        int leftToRightDiagonalSum = 0;
        int rightToLeftDiagonalSum = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (i == j) {
                    leftToRightDiagonalSum = leftToRightDiagonalSum + arr[i][j];
                }
                if (arr[i].length - 1 - i == j) {
                    rightToLeftDiagonalSum = rightToLeftDiagonalSum + arr[i][j];
                }
            }
        }
        System.out.println(leftToRightDiagonalSum);
        for (int i = arr.length - 1; i >= 0; i--) {
            for (int j = i; j >= 0; j--) {
            }
        }
        System.out.println(rightToLeftDiagonalSum);
        return Math.abs(leftToRightDiagonalSum - rightToLeftDiagonalSum);
    }

}
