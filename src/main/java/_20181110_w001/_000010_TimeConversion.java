package _20181110_w001;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class _000010_TimeConversion {
    static String timeConversion(String s) {
        DateTimeFormatter f12 = DateTimeFormatter.ofPattern("h:mm:ssa");
        DateTimeFormatter f24 = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalTime time = LocalTime.parse(s, f12);
        return f24.format(time);
    }

    public static void main(String[] args) {
        System.out.println(timeConversion("07:05:45PM"));
    }
}
