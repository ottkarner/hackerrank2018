package _20181110_w001;

public class _000006_Staircase {
    public static void main(String[] args) {
        staircase(5);
    }

    static void staircase(int n) {
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                if (j > n - i) {
                    System.out.print("#");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
