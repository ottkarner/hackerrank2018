package _20181110_w001;

import java.util.Arrays;

public class _000008_MinMaxSum {

    static void miniMaxSum(int[] arr) {
        Arrays.sort(arr);

        boolean calculate = true;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < 1 || arr[i] > Math.pow(10, 9)) {
                calculate = false;
            }
        }

        if (calculate) {
            long sum = 0L;
            for (int i = 0; i < arr.length - 1; i++) {
                sum += arr[i];
            }
            System.out.print(sum + " ");

            sum = 0;
            for (int i = 1; i < arr.length; i++) {
                sum += arr[i];
            }
            System.out.print(sum);
        }
    }

}
