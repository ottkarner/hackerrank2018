package _20190211_w015;

import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

public class _000070_Pangrams {

    public static void main(String[] args) {
        String[] strings = {
                "We promptly judged antique ivory buckles for the next prize",
                "We promptly judged antique ivory buckles for the prize"
        };
        for (String string : strings) {
            System.out.println(pangrams(string));
        }
    }

    // Complete the pangrams function below.
    static String pangrams(String s) {
        Set<Character> letterSet = new TreeSet<>();
        for (int i = 0; i < s.length(); i++) {
            char currentChar = s.charAt(i);
            if (Character.isLetter(currentChar)) {
                letterSet.add(Character.toLowerCase(currentChar));
            }
        }
        StringBuilder uniqueLetters = new StringBuilder();
        for (Character character : letterSet) {
            uniqueLetters.append(character);
        }
        return uniqueLetters.toString().equals("abcdefghijklmnopqrstuvwxyz") ?
                "pangram" : "not pangram";
    }

}
