package _20190211_w015;

public class _000068_CaesarCipher {

    public static void main(String[] args) {
        String string = "middle-Outz";
        System.out.println(string + " -> " + caesarCipher(string, 2));
        System.out.println(string + " -> " + caesarCipher2(string, 2));
        String string2 = "Always-Look-on-the-Bright-Side-of-Life";
        System.out.println(string2 + " -> " + caesarCipher(string2, 5));
        System.out.println(string2 + " -> " + caesarCipher2(string2, 5));
    }

    // Complete the caesarCipher function below.
    static String caesarCipher(String s, int k) {
        StringBuilder encryption = new StringBuilder();
        char[] alphabet = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        for (int i = 0; i < s.length(); i++) {
            char currentChar = s.charAt(i);
            char encryptedChar = encryptCharacter(currentChar, alphabet, k);
            encryption.append(encryptedChar);
        }
        return encryption.toString();
    }

    private static char encryptCharacter(char character, char[] alphabet, int shift) {
        boolean isUpperCase = Character.isUpperCase(character);
        for (int i = 0; i < alphabet.length; i++) {
            if (Character.toLowerCase(character) == alphabet[i]) {
                character = alphabet[(i + shift) % alphabet.length];
                if (isUpperCase) {
                    character = Character.toUpperCase(character);
                }
                break;
            }
        }
        return character;
    }

    static String caesarCipher2(String s, int k) {
        StringBuilder encryption = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            char currentChar = s.charAt(i);
            char encryptedChar = encryptCharacterEffective(currentChar, k);
            encryption.append(encryptedChar);
        }
        return encryption.toString();
    }

    private static char encryptCharacterEffective(char character, int shift) {
        if (Character.isAlphabetic(character)) {
            int asciiOfA = Character.isLowerCase(character) ? 'a' : 'A';
            return (char) ((character + shift - asciiOfA) % 26 + asciiOfA);
        }
        return character;
    }

}
