package _20190211_w015.interviewpreparationkit;

import java.util.Arrays;

public class _002_ArraysLeftRotation {

    public static void main(String[] args) {
        int array1[] = {1, 2, 3, 4, 5};
        int array2[] = {41, 73, 89, 7, 10, 1, 59, 58, 84, 77, 77, 97, 58, 1, 86, 58, 26, 10, 86, 51};
        int array3[] = {33, 47, 70, 37, 8, 53, 13, 93, 71, 72, 51, 100, 60, 87, 97};
        System.out.println(Arrays.toString(rotLeft(array1, 4)));
        System.out.println(Arrays.toString(rotLeft(array2, 10)));
        System.out.println(Arrays.toString(rotLeft(array3, 13)));
    }

    // Complete the rotLeft function below.
    static int[] rotLeft(int[] a, int d) {
        int[] rotArray = new int[a.length];
        for (int i = 0; i < a.length; i++) {
            rotArray[(i + (a.length - d)) % a.length] = a[i];
        }
        return rotArray;
    }

}
