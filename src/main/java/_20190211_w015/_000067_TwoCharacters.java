package _20190211_w015;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class _000067_TwoCharacters {

    public static void main(String[] args) {
        String[] strings = {
                "abaacdabd",
                "beabeefeab",
                "rhbaasdndfsdskgbfefdbrsdfhuyatrjtcrtyytktjjt"
        };

        for (String string : strings) {
            System.out.println(alternate(string));
        }
    }

    // Complete the alternate function below.
    static int alternate(String s) {
        char[] uniqueCharacters = findUniqueCharacters(s);
        int longestStringLength = 0;
        for (int i = 0; i < uniqueCharacters.length - 1; i++) {
            for (int j = i + 1; j < uniqueCharacters.length; j++) {
                char char1 = uniqueCharacters[i];
                char char2 = uniqueCharacters[j];
                String stringOfChar1AndChar2 = s.replaceAll("[^" + char1 + char2 + "]", "");
                if (stringOfChar1AndChar2.matches("^(?=.+$)(([a-z])(?!\\2))+$")) {
                    longestStringLength = Math.max(longestStringLength, stringOfChar1AndChar2.length());
                }
            }
        }
        return longestStringLength;
    }

    private static char[] findUniqueCharacters(String s) {
        Set<Character> characterSet = new HashSet<>();
        for (int i = 0; i < s.length(); i++) {
            characterSet.add(s.charAt(i));
        }
        char[] uniqueCharacters = new char[characterSet.size()];
        int i = 0;
        for (Character character : characterSet) {
            uniqueCharacters[i++] = character;
        }
        return uniqueCharacters;
    }
}
