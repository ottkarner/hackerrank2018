package _20190211_w015.javaproficiency;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

public class _060_JavaSHA256 {

    public static void main(String[] args) {
        String[] strings = {"HelloWorld", "Javarmi123"};
        for (String string : strings) {
            System.out.println(sha256Encryption(string));
        }
    }

    private static String sha256Encryption(String string) {
        String sha256Hex = "";
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(string.getBytes());
            byte[] messageDigestSHA256 = messageDigest.digest();
            sha256Hex = DatatypeConverter.printHexBinary(messageDigestSHA256).toLowerCase();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return sha256Hex;
    }

    private static void taskWithScanner() {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        scanner.close();
        System.out.println(sha256Encryption(s));
    }

}
