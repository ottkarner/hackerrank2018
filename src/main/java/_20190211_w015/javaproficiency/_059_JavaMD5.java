package _20190211_w015.javaproficiency;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

public class _059_JavaMD5 {

    public static void main(String[] args) {
        String[] strings = {"HelloWorld", "Javarmi123"};
        for (String string : strings) {
            System.out.println(md5Encryption(string));
        }
    }

    private static String md5Encryption(String string) {
        String md5Hex = "";
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(string.getBytes());
            byte[] messageDigestMD5 = messageDigest.digest();
            md5Hex = DatatypeConverter.printHexBinary(messageDigestMD5).toLowerCase();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return md5Hex;
    }

    private static void taskWithScanner() {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        scanner.close();
        System.out.println(md5Encryption(s));
    }

}
