package _20190211_w015;

public class _000069_MarsExploration {

    public static void main(String[] args) {
        String[] strings = {
                "SOSSPSSQSSOR",
                "SOSSOT",
                "SOSSOSSOS",
                "SOSOOSOSOSOSOSSOSOSOSOSOSOS",
                "OOSDSSOSOSWEWSOSOSOSOSOSOSSSSOSOSOSS"
        };
        for (String string : strings) {
            System.out.println(marsExploration(string));
        }
    }

    // Complete the marsExploration function below.
    static int marsExploration(String s) {
        int alteredLettersCount = 0;
        for (int i = 0; i < s.length(); i += 3) {
            if (s.charAt(i + 0) != 'S') alteredLettersCount++;
            if (s.charAt(i + 1) != 'O') alteredLettersCount++;
            if (s.charAt(i + 2) != 'S') alteredLettersCount++;
        }
        return alteredLettersCount;
    }

}
