package _20190211_w015;

public class _000072_AlternatingCharacters {

    public static void main(String[] args) {
        String[] strings = {
                "AAAA",
                "BBBBB",
                "ABABABAB",
                "BABABA",
                "AAABBB"
        };
        for (String string : strings) {
            System.out.println(alternatingCharacters(string));
            System.out.println(alternatingCharactersEfficient(string));
        }
    }

    // Complete the alternatingCharacters function below.
    static int alternatingCharacters(String s) {
        int deletionsCount = 0;

        StringBuilder stringBuilder = new StringBuilder(s);
        for (int i = 1; i < stringBuilder.length(); i++) {
            if (stringBuilder.charAt(i) == stringBuilder.charAt(i - 1)) {
                stringBuilder.deleteCharAt(i);
                deletionsCount++;
                i = 0;
            }
        }

        return deletionsCount;
    }

    static int alternatingCharactersEfficient(String s) {
        int deletionsCount = 0;

        for (int i = 1; i < s.length(); i++) {
            if (s.charAt(i) == s.charAt(i - 1)) {
                deletionsCount++;
            }
        }

        return deletionsCount;
    }

}
