package _20190211_w015;

public class _000071_FunnyString {

    public static void main(String[] args) {
        String[] strings = {"lmnop", "acxz", "bcxz"};
//        String[] strings = {"lmnop"};

        for (String string : strings) {
            System.out.println(funnyString(string));
        }
    }

    // Complete the funnyString function below.
    static String funnyString(String s) {
        for (int i = 0; i < s.length() - 1; i++) {
            int currentAdjacentDifferenceInString =
                    Math.abs((int) s.charAt(i) - (int) s.charAt(i + 1));

            int currentAdjacentDifferenceInReverseString =
                    Math.abs((int) s.charAt(s.length() - (i + 1)) - (int) s.charAt(s.length() - (i + 2)));

            if (currentAdjacentDifferenceInString != currentAdjacentDifferenceInReverseString) {
                return "Not Funny";
            }

            /*System.out.println(
                    currentAdjacentDifferenceInString + " -> " + currentAdjacentDifferenceInReverseString
            );*/
        }

        return "Funny";
    }

    // Complete the funnyString function below.
    static String funnyString2(String s) {
        int[] stringAdjacentDifferences = provideStringAdjacentDifferences(s);
        int[] reverseStringAdjacentDifferences = provideStringAdjacentDifferences(
                new StringBuilder(s).reverse().toString()
        );

        return "";
    }

    private static int[] provideStringAdjacentDifferences(String s) {
        int[] stringAdjacentDifferences = new int[s.length() - 1];

        for (int i = 0; i < s.length(); i++) {
            System.out.print((int) s.charAt(i) + " ");
        }
        System.out.println();

        return stringAdjacentDifferences;
    }

}
