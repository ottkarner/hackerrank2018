package _20190107_w010.javaproficiency;

import java.util.BitSet;
import java.util.Scanner;

public class _051_JavaBitSet {

    public static void main(String[] args) {
        /*Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        String[] operations = new String[m];
        int[][] setPairsOrSetIndexPairs = new int[m][2];
        for (int i = 0; i < m; i++) {
            operations[i] = scanner.next();
            setPairsOrSetIndexPairs[i][0] = scanner.nextInt();
            setPairsOrSetIndexPairs[i][1] = scanner.nextInt();
        }
        scanner.close();*/

        String[] operations = {"AND", "SET", "FLIP", "OR"};
        int[][] setPairsOrSetIndexPairs = {{1, 2}, {1, 4}, {2, 2}, {2, 1}};
        int n = 5;
        int m = operations.length;

        BitSet b1 = new BitSet(n);
        BitSet b2 = new BitSet(n);

        for (int i = 0; i < m; i++) {
            String operation = operations[i];
            int[] setPairOrSetIndexPair = setPairsOrSetIndexPairs[i];

            boolean b1First = setPairOrSetIndexPair[0] == 1 ? true : false;
            int indexOperand = setPairOrSetIndexPair[1];

            switch (operation) {
                case "AND":
                    if (b1First) b1.and(b2);
                    else b2.and(b1);
                    break;
                case "OR":
                    if (b1First) b1.or(b2);
                    else b2.or(b1);
                    break;
                case "XOR":
                    if (b1First) b1.xor(b2);
                    else b2.xor(b1);
                    break;
                case "FLIP":
                    if (b1First) b1.flip(indexOperand);
                    else b2.flip(indexOperand);
                    break;
                case "SET":
                    if (b1First) b1.set(indexOperand);
                    else b2.set(indexOperand);
                    break;
                default:
                    break;
            }

            System.out.println(b1.cardinality() + " " + b2.cardinality());
        }
    }

}
