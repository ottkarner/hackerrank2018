package _20190107_w010.javaproficiency;

import java.util.*;

public class _055_JavaPriorityQueue {

    //    private final static Scanner scan = new Scanner(System.in);
    private final static Priorities priorities = new Priorities();

    public static void main(String[] args) {
        /*int totalEvents = Integer.parseInt(scan.nextLine());
        List<String> events = new ArrayList<>();

        while (totalEvents-- != 0) {
            String event = scan.nextLine();
            events.add(event);
        }*/
        List<String> events = new ArrayList<>();
        events.add("ENTER John 3.75 50");
        events.add("ENTER Mark 3.8 24");
        events.add("ENTER Shafaet 3.7 35");
        events.add("SERVED");
        events.add("SERVED");
        events.add("ENTER Samiha 3.85 36");
        events.add("SERVED");
        events.add("ENTER Ashley 3.9 42");
        events.add("ENTER Maria 3.6 46");
        events.add("ENTER Anik 3.95 49");
        events.add("ENTER Dan 3.95 50");
        events.add("SERVED");

        List<Student> students = priorities.getStudents(events);

        if (students.isEmpty()) {
            System.out.println("EMPTY");
        } else {
            for (Student st : students) {
                System.out.println(st.getName());
            }
        }
    }
}

class Priorities {
    public List<Student> getStudents(List<String> events) {
        List<Student> studentList = new ArrayList<>();
        if (events.size() > 0) {
            Queue<Student> priorityQueue = new PriorityQueue<>();
            for (String event : events) {
                if (event.equals("SERVED")) {
                    if (!priorityQueue.isEmpty()) {
                        priorityQueue.poll();
                    }
                } else {
                    String[] eventElements = event.split("\\s+");
                    String name = eventElements[1];
                    double cgpa = Double.valueOf(eventElements[2]);
                    int id = Integer.valueOf(eventElements[3]);
                    priorityQueue.add(new Student(id, name, cgpa));
                }
            }
            while (!priorityQueue.isEmpty()) {
                studentList.add(priorityQueue.poll());
            }
        }
        return studentList;
    }

}

class Student implements Comparable<Student>, Comparator<Student> {

    private int id;
    private String name;
    private double cgpa;

    Student(int id, String name, double cgpa) {
        this.id = id;
        this.name = name;
        this.cgpa = cgpa;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getCgpa() {
        return cgpa;
    }

    /*@Override
    public int compareTo(Student student) {
        int cgpaCompare = Double.compare(this.cgpa, student.getCgpa());
        if (cgpaCompare != 0) {
            return -cgpaCompare;
        } else {
            int nameCompare = this.name.compareTo(student.getName());
            if (nameCompare != 0) {
                return nameCompare;
            } else {
                return Integer.compare(this.id, student.getId());
            }
        }
    }*/

    @Override
    public int compareTo(Student student) {
        return Comparator.comparingDouble(Student::getCgpa).reversed()
                .thenComparing(Student::getName)
                .thenComparingInt(Student::getId).compare(this, student);
    }

    /*@Override
    public int compare(Student student1, Student student2) {
        return Comparator.comparingDouble(Student::getCgpa).reversed()
                .thenComparing(Student::getName)
                .thenComparingInt(Student::getId).compare(student1, student2);
    }*/

    @Override
    public int compare(Student student1, Student student2) {
        return student1.compareTo(student2);
    }

}
