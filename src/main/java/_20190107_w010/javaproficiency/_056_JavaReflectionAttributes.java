package _20190107_w010.javaproficiency;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class _056_JavaReflectionAttributes {

    public static void main(String[] args) {
        printClassMethods1();
        printClassMethods2();
    }

    static void printClassMethods1() {
        Class student = Student2.class;
        Method[] methods = student.getDeclaredMethods();

        ArrayList<String> methodList = new ArrayList<>();
        for (Method method : methods) {
            methodList.add(method.getName());
        }

        Collections.sort(methodList);
        for (String name : methodList) {
            System.out.println(name);
        }
    }

    private static void printClassMethods2() {
        Class student = Student2.class;
        Method[] methods = student.getDeclaredMethods();
        Arrays.stream(methods)
                .map(Method::getName)
                .sorted()
                .forEach(System.out::println);
    }

}

class Student2 {
    private String name;
    private String id;
    private String email;

    public String getName() {
        return name;
    }

    public void setId(String id) {
        this.id = id;
    }

    private void setEmail(String email) {
        this.email = email;
    }

    public void anothermethod() {
    }
    /*......
            ......
    some more methods
    ......*/
}
