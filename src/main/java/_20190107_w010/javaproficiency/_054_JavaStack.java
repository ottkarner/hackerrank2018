package _20190107_w010.javaproficiency;

import java.util.ArrayDeque;
import java.util.Deque;

public class _054_JavaStack {

    public static void main(String[] args) {
        /*Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            String input = sc.next();
            //Complete the code
            System.out.println(isBalancedString(input));
        }
        sc.close();*/

        String[] input = {"{}()", "({()})", "{}(", "[]", ""};
        for (String string : input) {
            System.out.println(isBalancedString(string));
        }
    }

    private static boolean isBalancedString(String string) {
        int stringLength = string.length();
        if (stringLength == 0) return true;
        if (stringLength % 2 != 0) return false;
        else {
            Deque<Character> stack = new ArrayDeque<>();
            for (int i = 0; i < stringLength; i++) {
                char c = string.charAt(i);
                if (c == '(' || c == '{' || c == '[') {
                    stack.push(c);
                } else if (c == ')' || c == '}' || c == ']') {
                    if (stack.isEmpty()) return false;
                    else {
                        if (c == ')') if (stack.pop() != '(') return false;
                        if (c == '}') if (stack.pop() != '{') return false;
                        if (c == ']') if (stack.pop() != '[') return false;
                    }
                } else return false;
            }
            return stack.isEmpty();
        }
    }

}