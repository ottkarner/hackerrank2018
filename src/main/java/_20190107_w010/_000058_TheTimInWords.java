package _20190107_w010;

import java.util.HashMap;
import java.util.Map;

public class _000058_TheTimInWords {

    public static void main(String[] args) {
        System.out.println(timeInWords(5, 47));
        System.out.println(timeInWords(3, 00));
        System.out.println(timeInWords(7, 15));
        System.out.println(timeInWords(7, 1));
        System.out.println(timeInWords(5, 59));
        System.out.println(timeInWords(1, 1));
    }

    // Complete the timeInWords function below.
    private static String timeInWords(int h, int m) {
        Map<Integer, String> numbersMap = getNumbersMap();
        if (m == 0) return numbersMap.get(h) + " o' clock";
        if (m == 1) return numbersMap.get(m) + " minute past " + numbersMap.get(h);
        if (m == 15) return "quarter past " + numbersMap.get(h);
        if (m < 30) return numbersMap.get(m) + " minutes past " + numbersMap.get(h);
        if (m == 30) return "half past " + numbersMap.get(h);
        if (m == 45) return "quarter to " + numbersMap.get(h + 1);
        if (m == 59) return "one minute to " + numbersMap.get(h + 1);
        return numbersMap.get(60 - m) + " minutes to " + numbersMap.get(h + 1);
    }

    private static Map<Integer, String> getNumbersMap() {
        Map<Integer, String> numbersMap = new HashMap<>();
        numbersMap.put(1, "one");
        numbersMap.put(2, "two");
        numbersMap.put(3, "three");
        numbersMap.put(4, "four");
        numbersMap.put(5, "five");
        numbersMap.put(6, "six");
        numbersMap.put(7, "seven");
        numbersMap.put(8, "eight");
        numbersMap.put(9, "nine");
        numbersMap.put(10, "ten");
        numbersMap.put(11, "eleven");
        numbersMap.put(12, "twelve");
        numbersMap.put(13, "thirteen");
        numbersMap.put(14, "fourteen");
        numbersMap.put(15, "fifteen");
        numbersMap.put(16, "sixteen");
        numbersMap.put(17, "seventeen");
        numbersMap.put(18, "eighteen");
        numbersMap.put(19, "nineteen");
        numbersMap.put(20, "twenty");
        numbersMap.put(21, "twenty one");
        numbersMap.put(22, "twenty two");
        numbersMap.put(23, "twenty three");
        numbersMap.put(24, "twenty four");
        numbersMap.put(25, "twenty five");
        numbersMap.put(26, "twenty six");
        numbersMap.put(27, "twenty seven");
        numbersMap.put(28, "twenty eight");
        numbersMap.put(29, "twenty nine");
        numbersMap.put(30, "twenty nine");
        return numbersMap;
    }

}
