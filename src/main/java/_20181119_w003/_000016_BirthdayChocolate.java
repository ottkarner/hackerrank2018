package _20181119_w003;

import java.util.Arrays;
import java.util.List;

public class _000016_BirthdayChocolate {
    // Complete the birthday function below.
    static int birthday(List<Integer> s, int d, int m) {
        int countPossibilites = 0;

        if (s.size() >= 1 && s.size() <= 100 && m >= 1 && m <= 12 && d >= 1 && d <= 31) {
            for (int i = 0; i <= s.size() - m; i++) {
                List<Integer> subList = s.subList(i, i + m);
                int subListSum = 0;
                for (int j = 0; j < subList.size(); j++) {
                    subListSum += subList.get(j);
                }
                if (subListSum == d) {
                    countPossibilites++;
                }
                System.out.println(subList);
            }
        }

        return countPossibilites;
    }

    public static void main(String[] args) {
        System.out.println(birthday(Arrays.asList(new Integer[]{2, 2, 1, 3, 2}), 4, 2));
        System.out.println(birthday(Arrays.asList(new Integer[]{1, 1, 1, 1, 1, 1}), 3, 2));
        System.out.println(birthday(Arrays.asList(new Integer[]{4}), 4, 1));
        System.out.println(birthday(Arrays.asList(new Integer[]{2, 2, 1, 3, 2}), 5, 3));

    }

}
