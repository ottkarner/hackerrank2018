package _20181119_w003.tutorial30daysofcode;

import java.util.Scanner;

public class Day7_ArraysSubmitted {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        if (n >= 1 && n <= 1000) {
            int[] arr = new int[n];

            String[] arrItems = scanner.nextLine().split(" ");
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            boolean proceed = true;
            for (int i = 0; i < n; i++) {
                int arrItem = Integer.parseInt(arrItems[i]);
                if (arrItem >= 1 && arrItem <= 10000) {
                    arr[i] = arrItem;
                } else {
                    proceed = false;
                    break;
                }
            }

            if (proceed) {
                for (int i = arr.length - 1; i >= 0; i--) {
                    System.out.print(arr[i] + " ");
                }
            }
        }
        scanner.close();
    }

}
