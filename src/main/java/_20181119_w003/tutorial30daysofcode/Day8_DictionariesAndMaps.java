package _20181119_w003.tutorial30daysofcode;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Day8_DictionariesAndMaps {
    public static void main(String[] argh) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        if (n >= 1 && n <= Math.pow(10, 5)) {
            Map<String, Integer> phoneBook = new HashMap<>();
            for (int i = 0; i < n; i++) {
                String name = in.next();
                int phone = in.nextInt();
                // Write code here
                phoneBook.put(name, phone);
            }
            int queryCount = 0;
            while (in.hasNext() && queryCount < Math.pow(10, 5)) {
                String s = in.next();
                // Write code here
                if (phoneBook.containsKey(s)) {
                    System.out.println(s + "=" + phoneBook.get(s));
                } else {
                    System.out.println("Not found");
                }
                queryCount++;
            }
        }
        in.close();
    }
}
