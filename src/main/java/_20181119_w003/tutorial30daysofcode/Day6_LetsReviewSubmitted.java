package _20181119_w003.tutorial30daysofcode;

import java.util.Scanner;

public class Day6_LetsReviewSubmitted {
    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        try {
            Scanner scanner = new Scanner(System.in);
            if (scanner.hasNextInt()) {
                int T = scanner.nextInt();
                if (T>=1 && T <=10){
                    for (int i=1; i<=T && scanner.hasNext(); i++) {
                        String s=scanner.next();
                        if (s.length()>=2 && s.length()<=10000) {
                            System.out.println(stringEvenIndices(s)+" "+stringOddIndices(s));
                        }
                    }
                }
            }
            scanner.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    static String stringEvenIndices(String s) {
        String evenIndices="";
        for (int i=0; i<s.length(); i++) {
            if (i%2==0) {
                evenIndices+=s.charAt(i);
            }
        }
        return evenIndices;
    }

    static String stringOddIndices(String s) {
        String oddIndices="";
        for (int i=0; i<s.length(); i++) {
            if (i%2!=0) {
                oddIndices+=s.charAt(i);
            }
        }
        return oddIndices;
    }
}
