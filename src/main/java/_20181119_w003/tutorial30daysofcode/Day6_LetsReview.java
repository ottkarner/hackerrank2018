package _20181119_w003.tutorial30daysofcode;

public class Day6_LetsReview {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. */
        String s = "Hacker";
        System.out.println(stringEvenIndices(s) + " " + stringOddIndices(s  ));
    }

    private static String stringEvenIndices(String s) {
        String evenIndices = "";
        for (int i = 0; i < s.length(); i++) {
            if (i % 2 == 0) {
                evenIndices += s.charAt(i);
            }
        }
        return evenIndices;
    }

    private static String stringOddIndices(String s) {
        String oddIndices = "";
        for (int i = 0; i < s.length(); i++) {
            if (i % 2 != 0) {
                oddIndices += s.charAt(i);
            }
        }
        return oddIndices;
    }

    private static String[] giveEvenAndOddIndicesOfString(String s) {
        String evenIndices = "", oddIndices = "";
        for (int i = 0; i < s.length(); i++) {
            if (i % 2 == 0) {
                evenIndices += s.charAt(i);
            } else {
                oddIndices += s.charAt(i);
            }
        }
        return new String[]{evenIndices, oddIndices};
    }

}
