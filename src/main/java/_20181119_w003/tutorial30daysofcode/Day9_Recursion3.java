package _20181119_w003.tutorial30daysofcode;

public class Day9_Recursion3 {

    // Complete the factorial function below.
    static int factorial(int n) {
        if (n >= 2 && n <= 12) {
            int result = n;
            for (int i = 1; i <= n; i++) {
                result = n * factorial(n - 1);
            }
            return result;
        }
        return 1;
    }

    public static void main(String[] args) {
        System.out.println(factorial(4));
    }

}
