package _20181119_w003;

import java.util.Arrays;
import java.util.Collection;

public class _000017_DivisibleSumPairs {

    // Complete the divisibleSumPairs function below.
    static int divisibleSumPairs(int n, int k, int[] ar) {
        int count = 0;
        if (ar.length >= 2 && ar.length <= 100 && k >= 1 && k <= 100) {
            for (int i = 0; i < ar.length - 1; i++) {
                for (int j = i + 1; j < ar.length; j++) {
//                    System.out.print("(" + ar[i] + "," + ar[j] + ")");
                    if ((ar[i] + ar[j]) % k == 0) {
                        count++;
                        System.out.print("(" + ar[i] + "," + ar[j] + ")");
                    }
                }
            }
        }
        System.out.println();
        return count;
    }

    public static void main(String[] args) {
        int[] integers;
        integers = new int[]{1, 2, 3, 4, 5, 6};
        System.out.println(divisibleSumPairs(integers.length, 5, integers));
        integers = new int[]{1, 3, 2, 6, 1, 2};
        System.out.println(divisibleSumPairs(integers.length, 3, integers));
    }

}
