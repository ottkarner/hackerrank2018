package _20181119_w003.javaproficiency;

import java.util.Scanner;

public class _005_java_JavaOutputFormatting {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("================================");
        for (int i = 0; i < 1; i++) {
            String s1 = sc.next();
            int x = sc.nextInt();
            //Complete this line
            System.out.printf("%-15s%03d%n", s1, x);
//            System.out.printf("%s", s1, "%d", x, "\n");
        }
        System.out.println("================================");

    }

}
