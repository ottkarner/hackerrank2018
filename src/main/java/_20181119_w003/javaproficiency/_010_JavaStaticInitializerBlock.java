package _20181119_w003.javaproficiency;

import java.util.Scanner;

public class _010_JavaStaticInitializerBlock {

    // Write your code here
    static boolean flag = true;
    static int B, H;

    static {
        try {
            Scanner scanner = new Scanner(System.in);
            if (scanner.hasNextInt()) {
                B = scanner.nextInt();
            } else {
                flag = false;
            }
            if (scanner.hasNextInt()) {
                H = scanner.nextInt();
            } else {
                flag = false;
            }
            scanner.close();
            if (B <= 0 || H <= 0 || B >= 100 || H >= 100) {
                flag = false;
            }
            if (!flag) {
                throw new Exception("Breadth and height must be positive");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static void main(String[] args) {
        if (flag) {
            int area = B * H;
            System.out.print(area);
        }
    }

}