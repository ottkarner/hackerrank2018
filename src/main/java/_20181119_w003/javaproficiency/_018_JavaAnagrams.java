package _20181119_w003.javaproficiency;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class _018_JavaAnagrams {

    public static void main(String[] args) {
        System.out.println((isAnagram("anagram", "margana")) ? "Anagrams" : "Not Anagrams");
    }

    private static boolean isAnagram(String a, String b) {
        // Complete the function
        if (a.length() == b.length() && a.length() >= 1 && a.length() <= 50 && a.matches("[a-zA-Z]+")) {
            boolean isAnagram = true;
            char[] charsA = a.toCharArray();
            char[] charsB = b.toCharArray();
            for (int i = 0; i < a.length(); i++) {
                if (charFrequencyInCharArrayNonCaseSensitive(charsA, charsA[i]) != charFrequencyInCharArrayNonCaseSensitive(charsB, charsA[i])) {
                    isAnagram = false;
                    break;
                }
            }
            return isAnagram;
        }
        return false;
    }

    private static int charFrequencyInCharArrayNonCaseSensitive(char[] chars, char c) {
        int count = 0;
        for (int i = 0; i < chars.length; i++) {
            if (Character.toLowerCase(chars[i]) == Character.toLowerCase(c)) {
                count++;
            }
        }
        return count;
    }

    static boolean isAnagram2(String a, String b) {
        // Complete the function
        if (a.length() == b.length() && a.length() >= 1 && a.length() <= 50) {
            List<Character> charsA = charArrayToCharacterLowercaseList(a.toCharArray());
            List<Character> charsB = charArrayToCharacterLowercaseList(b.toCharArray());
            boolean isAnagram = true;
            for (int i = 0; i < a.length(); i++) {
                if (Collections.frequency(charsA, charsA.get(i)) != (Collections.frequency(charsB, charsB.get(i)))) {
                    isAnagram = false;
                    break;
                }
            }
            return isAnagram;
        }
        return false;
    }

    static List<Character> charArrayToCharacterLowercaseList(char[] characters) {
        List<Character> characterList = new ArrayList<>();
        for (char character : characters) {
            character = Character.toLowerCase(character);
            characterList.add(character);
        }
        return characterList;
    }

}
