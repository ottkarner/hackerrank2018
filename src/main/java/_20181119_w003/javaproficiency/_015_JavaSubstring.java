package _20181119_w003.javaproficiency;

import java.util.Scanner;

public class _015_JavaSubstring {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        if (in.hasNext()) {
            String S = in.next();
            if (in.hasNextInt()) {
                int start = in.nextInt();
                if (in.hasNextInt()) {
                    int end = in.nextInt();
                    if (S.length() >= 1 && S.length() <= 100 && start >= 0 && start <= end && end <= S.length()) {
                        System.out.println(S.substring(start, end));
                    }
                }
            }
        }
        in.close();
    }
}
