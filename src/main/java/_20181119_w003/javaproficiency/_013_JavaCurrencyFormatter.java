package _20181119_w003.javaproficiency;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Scanner;

public class _013_JavaCurrencyFormatter {
    public static void main(String[] args) {

//        Locale list[] = DateFormat.getAvailableLocales();
//        for (Locale aLocale : list) {
//            System.out.println(aLocale.toString());
//        }

        Scanner scanner = new Scanner(System.in);
        double payment = scanner.nextDouble();
        scanner.close();

        // Write your code here.
        String us = NumberFormat.getCurrencyInstance(new Locale("en", "US")).format(payment);
        String india = NumberFormat.getCurrencyInstance(new Locale("en", "IN")).format(payment);
        String china = NumberFormat.getCurrencyInstance(new Locale("zh", "CN")).format(payment);
        String france = NumberFormat.getCurrencyInstance(new Locale("fr", "FR")).format(payment);

        System.out.println("US: " + us);
        System.out.println("India: " + india);
        System.out.println("China: " + china);
        System.out.println("France: " + france);
    }
}
