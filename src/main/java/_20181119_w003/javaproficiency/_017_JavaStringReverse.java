package _20181119_w003.javaproficiency;

import java.util.Scanner;

public class _017_JavaStringReverse {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String A = sc.next();
        /* Enter your code here. Print output to STDOUT. */
        System.out.println(A.equals(new StringBuilder(A).reverse().toString()) ? "Yes" : "No");
    }

}
