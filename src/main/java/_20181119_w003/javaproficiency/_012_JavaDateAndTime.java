package _20181119_w003.javaproficiency;

import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.Locale;

class Result2 {

    /*
     * Complete the 'findDay' function below.
     *
     * The function is expected to return a STRING.
     * The function accepts following parameters:
     *  1. INTEGER month
     *  2. INTEGER day
     *  3. INTEGER year
     */

    public static String findDay(int month, int day, int year) {
        LocalDate localDate = LocalDate.of(year, month, day);
        return localDate.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.ENGLISH).toUpperCase();
    }

}

public class _012_JavaDateAndTime {
    public static void main(String[] args) {
        System.out.println(Result2.findDay(8, 5, 2015));
    }
}
