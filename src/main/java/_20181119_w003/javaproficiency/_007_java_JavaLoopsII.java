package _20181119_w003.javaproficiency;

public class _007_java_JavaLoopsII {
    public static void main(String[] args) {

        //        int a = 0, b = 2, n = 10;
        int a = 5, b = 3, n = 5;

        int s = a;
        for (int i = 1; i <= n; i++) {
            s += b * Math.pow(2, i - 1);
            System.out.print(s + " ");
        }

    }
}
