package _20181119_w003;

import java.util.*;

public class _000018_MigratoryBirds {

    // Complete the migratoryBirds function below.
    static int migratoryBirds(List<Integer> arr) {
        int n = arr.size();
        int mostFrequentlySightedBirdWithLowestTypeNumber = 0;
        if (n >= 5 && n < 2 * Math.pow(10, 5)) {
            System.out.println();
            for (int i = 1; i <= 5; i++) {
                if (Collections.frequency(arr, i) > Collections.frequency(arr, mostFrequentlySightedBirdWithLowestTypeNumber)) {
                    mostFrequentlySightedBirdWithLowestTypeNumber = i;
                }
                System.out.println("i=" + i + " (" + Collections.frequency(arr, i) + "x) => mostFrequentlySightedBirdWithLowestTypeNumber=" + mostFrequentlySightedBirdWithLowestTypeNumber);
            }
        }
        return mostFrequentlySightedBirdWithLowestTypeNumber;
    }

    public static void main(String[] args) {
        System.out.println(migratoryBirds(new ArrayList<>(Arrays.asList(1, 1, 2, 2, 3))));
        System.out.println(migratoryBirds(Arrays.asList(1, 4, 4, 4, 5, 3)));
        System.out.println(migratoryBirds(Arrays.asList(1, 2, 3, 4, 5, 4, 3, 2, 1, 3, 4)));
    }

}
