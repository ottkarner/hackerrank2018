package _20190225_w017;

public class _000078_MakingAnagrams {

    public static void main(String[] args) {
        System.out.println(makingAnagrams("abc", "amnop"));
        System.out.println(makingAnagrams("cde", "abc"));
    }

    // Complete the makingAnagrams function below.
    static int makingAnagrams(String s1, String s2) {
        int minDeletions = 0;
        int[] charsCountsDifference = new int[26];
        for (int i = 0; i < s1.length(); i++) charsCountsDifference[s1.charAt(i) - 'a']++;
        for (int i = 0; i < s2.length(); i++) charsCountsDifference[s2.charAt(i) - 'a']--;
        for (int charCount : charsCountsDifference) minDeletions += Math.abs(charCount);
        return minDeletions;
    }

}
