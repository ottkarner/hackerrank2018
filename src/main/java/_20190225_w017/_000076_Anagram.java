package _20190225_w017;

public class _000076_Anagram {

    public static void main(String[] args) {
        String[] strings = {
                "aaabbb",
                "ab",
                "abc",
                "mnop",
                "xyyx",
                "xaxbbbxx"
        };
        for (String string : strings) {
            System.out.println(anagram(string));
        }
    }

    // Complete the anagram function below.
    static int anagram(String s) {
        if (s.length() % 2 != 0) {
            return -1;
        } else {
            StringBuilder sb1 = new StringBuilder(s.substring(0, s.length() / 2));
            StringBuilder sb2 = new StringBuilder(s.substring(s.length() / 2));
            int i = 0;
            while (i < sb1.length()) {
                int sb2Index = sb2.indexOf(sb1.substring(i, i + 1));
                if (sb2Index == -1) {
                    i++;
                } else {
                    sb1.deleteCharAt(i);
                    sb2.deleteCharAt(sb2Index);
                }
            }
            return sb1.length();
        }
    }

}
