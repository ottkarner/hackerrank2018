package _20190225_w017;

public class _000077_BeautifulBinaryString {

    public static void main(String[] args) {
        String[] strings = {
                "0101010",
                "01100",
                "0100101010"
        };
        for (String string : strings) {
            System.out.println(beautifulBinaryString(string));
        }
    }

    // Complete the beautifulBinaryString function below.
    static int beautifulBinaryString(String s) {
        int count = 0;
        for (int i = 0; i < s.length() - 2; i++) {
            if (s.substring(i, i + 3).equals("010")) {
                count++;
                i += 2;
            }
        }
        return count;
    }

    static int beautifulBinaryString2(String s) {
        int count = 0;
        StringBuilder sb = new StringBuilder(s);
        for (int i = 0; i < sb.length() - 2; i++) {
            if (sb.substring(i, i + 3).equals("010")) {
                sb.setCharAt(i + 2, '1');
                count++;
            }
            System.out.print(sb.substring(i, i + 3) + " ");
        }
        return count;
    }

}