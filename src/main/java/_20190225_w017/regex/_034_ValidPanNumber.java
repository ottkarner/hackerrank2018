package _20190225_w017.regex;

import java.util.Scanner;

public class _034_ValidPanNumber {

    public static void main(String[] args) {
        String[] strings = {
                "ABCDS1234Y",
                "ABAB12345Y",
                "avCDS1234Y"
        };
        for (String string : strings) {
            System.out.println(
                    isValidPAN(string) ? "YES" : "NO"
            );
        }
    }

    private static void taskWithScanner() {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.valueOf(scanner.nextLine());
        String[] strings = new String[n];
        for (int i = 0; i < n; i++) {
            strings[i] = scanner.nextLine();
        }
        scanner.close();
        for (String string : strings) {
            System.out.println(
                    isValidPAN(string) ? "YES" : "NO"
            );
        }
    }

    private static boolean isValidPAN(String s) {
        return s.matches("^[A-Z]{5}\\d{4}[A-Z]{1}$") ? true : false;
    }

}
