package _20190225_w017.regex;

import java.util.Scanner;

public class _033_UtopianIdentificationNumber {

    public static void main(String[] args) {
        String[] strings = {
                "abc012333ABCDEEEE",
                "0123AB"
        };
        for (String string : strings) {
            System.out.println(
                    findUtopianIDNumber(string)
            );
        }
    }

    private static void taskWithScanner() {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.valueOf(scanner.nextLine());
        String[] strings = new String[n];
        for (int i = 0; i < n; i++) {
            strings[i] = scanner.nextLine();
        }
        scanner.close();
        for (String string : strings) {
            System.out.println(findUtopianIDNumber(string));
        }
    }

    private static String findUtopianIDNumber(String string) {
        return string.matches("^[a-z]{0,3}\\d{2,8}[A-Z]{3,}") ? "VALID" : "INVALID";
    }

}
