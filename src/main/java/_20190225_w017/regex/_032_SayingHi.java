package _20190225_w017.regex;

public class _032_SayingHi {

    public static void main(String[] args) {
        String[] lines = {
                "Hi Alex how are you doing",
                "hI dave how are you doing",
                "Good by Alex",
                "hidden agenda",
                "Alex greeted Martha by saying Hi Martha",
                "etc oppose soil rope",
                "hi bite draft relax similarly",
                "arrest unfair surface lightly concentration"
        };
        for (String line : lines) {
            String result = findSayingHi(line);
            if (!result.isEmpty()) {
                System.out.println(result);
            }
        }
    }

    private static String findSayingHi(String line) {
        return line.matches("^[hH][iI]\\s{1,1}[^dD].*$") ? line : "";
    }

}
