package _20190225_w017.regex;

public class _035_HackerRankLanguage {

    public static void main(String[] args) {
        String[] strings = {
                "11011 LUA",
                "11022 BRAINFUCK",
                "11044 X"
        };
        for (String string : strings) {
            System.out.println(
                    validateLanguage(string)
            );
        }
    }

    private static String validateLanguage(String string) {
        String regex = "^\\d{5,6}\\s(C|CPP|JAVA|PYTHON|PERL|PHP|RUBY|CSHARP|HASKELL|CLOJURE|BASH|SCALA|ERLANG|CLISP|LUA|BRAINFUCK|JAVASCRIPT|GO|D|OCAML|R|PASCAL|SBCL|DART|GROOVY|OBJECTIVEC)$";
        return string.matches(regex) ? "VALID" : "INVALID";
    }

}
