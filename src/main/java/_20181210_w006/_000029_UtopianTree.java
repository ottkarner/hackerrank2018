package _20181210_w006;

public class _000029_UtopianTree {

    public static void main(String[] args) {
        int[] cases = {0, 1, 4};
        for (int n : cases) {
            System.out.println(utopianTree(n));
        }
    }

    // Complete the utopianTree function below.
    static int utopianTree(int n) {
        int height = 1;
        boolean spring = true;
        boolean summer = false;
        for (int i = 0; i < n; i++) {
            if (spring) {
                height *= 2;
                spring = false;
                summer = true;
                continue;
            }
            if (summer) {
                height += 1;
                summer = false;
                spring = true;
                continue;
            }
        }
        return height;
    }

    static int utopianTree2(int n) {
        int height = 1;
        for (int i = 0; i < n; i++) {
            if (i % 2 == 0) {
                height *= 2;
            } else {
                height += 1;
            }
        }
        return height;
    }

    static int utopianTree3(int n) {
        int height = 1;
        for (int i = 0; i < n; i++) {
            height = (i % 2 == 0 ? height * 2 : height + 1);
        }
        return height;
    }

    static int utopianTree4(int n) {
        return ((int) Math.pow(2, (n + 3) / 2)) + (((int) Math.pow(-1, n)) - 3) / 2;
    }

    static int utopianTree5(int n) {
        return (1 << (n >> 1) + 1) - 1 << (n & 1);
    }

}
