package _20181210_w006;

import java.util.Arrays;

public class _000034_CircularArrayRotation {

    public static void main(String[] args) {
/*
        int[] nkq ={};
        String[] nkq = scanner.nextLine().split(" ");
        int n = Integer.parseInt(nkq[0]);
        int k = Integer.parseInt(nkq[1]);
        int q = Integer.parseInt(nkq[2]);
        int[] a = new int[n];
        String[] aItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
        for (int i = 0; i < n; i++) {
            int aItem = Integer.parseInt(aItems[i]);
            a[i] = aItem;
        }
        int[] queries = new int[q];
        for (int i = 0; i < q; i++) {
            int queriesItem = scanner.nextInt();
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
            queries[i] = queriesItem;
        }
*/
        int[] a = {1, 2, 3};
        int k = 2;
        int[] q = {0, 1, 2};
        System.out.println(Arrays.toString(circularArrayRotation(a, k, q)));
    }

    // Complete the circularArrayRotation function below.
    static int[] circularArrayRotation(int[] a, int k, int[] queries) {
        int[] rotatedA = new int[a.length];
        for (int i = 0; i < a.length; i++) {
            rotatedA[(i + k) % a.length] = a[i];
        }
        int[] results = new int[queries.length];
        for (int i = 0; i < queries.length; i++) {
            if (queries[i] < rotatedA.length) {
                results[i] = rotatedA[queries[i]];
            }
        }
        return results;
    }

    private static int[] rotateArrayRight(int[] array, int steps) {
        int[] rotateArray = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            rotateArray[(i + steps) % array.length] = array[i];
//            System.out.print(i + " " + (i + steps) + " " + array.length + " " + ((i + steps) % array.length) + "\n");
        }
        return rotateArray;
    }

    private static int[] rotateArrayLeft(int[] array, int steps) {
        int[] rotateArray = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            rotateArray[(i + (array.length - steps)) % array.length] = array[i];
        }
        return rotateArray;
    }

}

