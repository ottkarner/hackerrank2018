package _20181210_w006;

import java.io.IOException;
import java.util.Scanner;

public class _000030_AngryProfessor {

    // Complete the angryProfessor function below.
    static String angryProfessor(int k, int[] a) {
        int arrivedOnTime = 0;
        for (int arriveTime : a) {
            if (arriveTime <= 0) {
                arrivedOnTime++;
            }
        }
        return arrivedOnTime < k ? "YES" : "NO";
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        System.out.println(angryProfessor(4, new int[]{-1, -1, 0, 0, 1, 1}));
        System.out.println(angryProfessor(5, new int[]{-1, -1, 0, 0, 1, 1}));
        System.out.println(angryProfessor(3, new int[]{-1, -3, 4, 2,}));
        System.out.println(angryProfessor(2, new int[]{0, -1, 2, 1}));

/*
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int t = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int tItr = 0; tItr < t; tItr++) {
            String[] nk = scanner.nextLine().split(" ");

            int n = Integer.parseInt(nk[0]);

            int k = Integer.parseInt(nk[1]);

            int[] a = new int[n];

            String[] aItems = scanner.nextLine().split(" ");
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            for (int i = 0; i < n; i++) {
                int aItem = Integer.parseInt(aItems[i]);
                a[i] = aItem;
            }

            String result = angryProfessor(k, a);

            bufferedWriter.write(result);
            bufferedWriter.newLine();
        }

        bufferedWriter.close();

        scanner.close();
*/

    }

}