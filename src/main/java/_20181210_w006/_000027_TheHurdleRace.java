package _20181210_w006;

public class _000027_TheHurdleRace {

    // Complete the hurdleRace function below.
    static int hurdleRace(int k, int[] heights) {
        int max = 0;
        for (int height : heights) {
            if (height > max) max = height;
        }
        return k < max ? max - k : 0;
    }

    public static void main(String[] args) {
        int k = 1;
        int[] heights = {1, 2, 3, 3, 2};
        System.out.println(hurdleRace(k, heights));
    }

}
