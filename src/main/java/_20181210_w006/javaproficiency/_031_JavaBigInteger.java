package _20181210_w006.javaproficiency;

import java.math.BigInteger;

public class _031_JavaBigInteger {

    public static void main(String[] args) {
        // Enter your code here. Read input from STDIN. Print output to STDOUT.
        String a = "1000";
        String b = "1000";
        if (a.matches("-?\\d+") && b.matches("-?\\d+")) {
            BigInteger A = new BigInteger(a);
            BigInteger B = new BigInteger(b);
            System.out.println(A.add(B));
            System.out.println(A.multiply(B));
        }
    }

}
