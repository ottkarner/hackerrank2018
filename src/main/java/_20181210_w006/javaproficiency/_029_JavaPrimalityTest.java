package _20181210_w006.javaproficiency;

import java.math.BigInteger;

public class _029_JavaPrimalityTest {

    public static void main(String[] args) {
        String s = "-13";
        if (s.matches("-?\\d+")) {
            if (new BigInteger(s).isProbablePrime(1)) {
                System.out.println("prime");
            } else {
                System.out.println("not prime");
            }
        }
    }

}
