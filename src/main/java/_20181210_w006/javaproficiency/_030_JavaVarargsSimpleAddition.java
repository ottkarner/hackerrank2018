package _20181210_w006.javaproficiency;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;

//Write your code here
class Add {
    int add(int... values) {
        String s = "";
        int sum = 0;
        for (int value : values) {
            s += value + "+";
            sum += value;
        }
        s = s.substring(0, s.length() - 1) + "=" + sum;
        System.out.println(s);
        return sum;
    }
}


public class _030_JavaVarargsSimpleAddition {

    public static void main(String[] args) {
        try {
//            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            int n1 = 1;//Integer.parseInt(br.readLine());
            int n2 = 2;//Integer.parseInt(br.readLine());
            int n3 = 3;//Integer.parseInt(br.readLine());
            int n4 = 4;//Integer.parseInt(br.readLine());
            int n5 = 5;//Integer.parseInt(br.readLine());
            int n6 = 6;//Integer.parseInt(br.readLine());
            Add ob = new Add();
            ob.add(n1, n2);
            ob.add(n1, n2, n3);
            ob.add(n1, n2, n3, n4, n5);
            ob.add(n1, n2, n3, n4, n5, n6);
            Method[] methods = Add.class.getDeclaredMethods();
            Set<String> set = new HashSet<>();
            boolean overload = false;
            for (int i = 0; i < methods.length; i++) {
                if (set.contains(methods[i].getName())) {
                    overload = true;
                    break;
                }
                set.add(methods[i].getName());
            }
            if (overload) {
                throw new Exception("Overloading not allowed");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

