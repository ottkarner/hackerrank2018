package _20181210_w006.tutorial30daysofcode;

import java.util.Arrays;
import java.util.Collections;

public class Day29_BitwiseAND {

    public static void main(String[] args) {
        int[][] testCases = {
                {5, 2},
                {8, 5},
                {2, 2}
        };
//        System.out.println(Arrays.deepToString(testCases));
        for (int[] testCase : testCases) {
            System.out.println(findMaxAAndB(testCase[0], testCase[1]));
        }
    }

    private static int findMaxAAndB(int n, int k) {
        int maxAAndB = 0;
        for (int a = 1; a <= n - 1; a++) {
            for (int b = a + 1; b <= n; b++) {
                int currentAAndB = a & b;
//                System.out.println("A=" + a + ", " + "B=" + b + ", A&B=" + currentAAndB);
                if (currentAAndB > maxAAndB) {
                    if (currentAAndB < k) {
                        maxAAndB = currentAAndB;
                        if (maxAAndB + 1 == k) {
                            return maxAAndB;
                        }
                    }
                }
            }
        }
        return maxAAndB;
    }

    private static int findMaxAAndB2(int n, int k) {
        if (((k - 1) | k) > n && k % 2 == 0) {
            return k - 2;
        } else {
            return k - 1;
        }
    }

    private static int findMaxAAndB3(int n, int k) {
        if (((k - 1) | k) <= n) {
            return k - 1;
        } else {
            return k - 2;
        }
    }

}
