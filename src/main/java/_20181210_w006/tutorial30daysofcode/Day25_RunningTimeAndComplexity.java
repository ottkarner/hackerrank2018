package _20181210_w006.tutorial30daysofcode;

public class Day25_RunningTimeAndComplexity {

    public static void main(String[] args) {
        int[] testCases = {12, 5, 7, 1};

        for (int testCase : testCases) {
            if (isPrime(testCase)) {
                System.out.println("Prime");
            } else {
                System.out.println("Not prime");
            }
        }
    }

    private static boolean isPrime(int number) {
        if (number < 2) {
            return false;
        } else {
            for (int i = 2; i <= (int) (Math.sqrt(number)); i++) {
                if (number % i == 0) {
                    return false;
                }
            }
        }
        return true;
    }
}
