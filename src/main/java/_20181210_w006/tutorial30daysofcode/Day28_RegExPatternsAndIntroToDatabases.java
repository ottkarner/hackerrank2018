package _20181210_w006.tutorial30daysofcode;

import java.util.*;

public class Day28_RegExPatternsAndIntroToDatabases {

//    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        String[] records = {
                "riya riya@gmail.com",
                "julia julia@julia.me",
                "julia sjulia@gmail.com",
                "julia julia@gmail.com",
                "samantha samantha@gmail.com",
                "tanya tanya@gmail.com",
                "riya ariya@gmail.com",
                "julia bjulia@julia.me",
                "julia csjulia@gmail.com",
                "julia djulia@gmail.com",
                "samantha esamantha@gmail.com",
                "tanya ftanya@gmail.com",
                "riya riya@live.com",
                "julia julia@live.com",
                "julia sjulia@live.com",
                "julia julia@live.com",
                "samantha samantha@live.com",
                "tanya tanya@live.com",
                "riya ariya@live.com",
                "julia bjulia@live.com",
                "julia csjulia@live.com",
                "julia djulia@live.com",
                "samantha esamantha@live.com",
                "tanya ftanya@live.com",
                "riya gmail@riya.com",
                "priya priya@gmail.com",
                "preeti preeti@gmail.com",
                "alice alice@alicegmail.com",
                "alice alice@gmail.com",
                "alice gmail.alice@gmail.com"
        };
//        int N = scanner.nextInt();
//        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
//        for (int NItr = 0; NItr < N; NItr++) {
        List<String> firstNames = new ArrayList<>();
        for (String record : records) {
//            String[] firstNameEmailID = scanner.nextLine().split(" ");
            String[] firstNameEmailID = record.split(" ");
            String firstName = firstNameEmailID[0];
            String emailID = firstNameEmailID[1];
            if (emailID.matches("[a-z]+(\\.[a-z]+)*@gmail.com")) {
                firstNames.add(firstName);
            }
        }
//        scanner.close();
        Collections.sort(firstNames);
        for (String firstName : firstNames) {
            System.out.println(firstName);
        }
    }

}

