package _20181210_w006.tutorial30daysofcode;

class Node {
    int data;
    Node next;

    Node(int d) {
        data = d;
        next = null;
    }
}

public class Day24_MoreLinkedLists {

    public static Node insert(Node head, int data) {
        Node p = new Node(data);
        if (head == null)
            head = p;
        else if (head.next == null)
            head.next = p;
        else {
            Node start = head;
            while (start.next != null)
                start = start.next;
            start.next = p;

        }
        return head;
    }

    public static void display(Node head) {
        Node start = head;
        while (start != null) {
            System.out.print(start.data + " ");
            start = start.next;
        }
        System.out.println();
    }

    public static Node removeDuplicates(Node head) {
        //Write your code here
        Node temp = head;
        while (temp != null && temp.next != null) {
            if (temp.data == temp.next.data) {
                // duplicate: remove and stay on this node
                temp.next = temp.next.next;
            } else {
                // not duplicate: move to next node
                temp = temp.next;
            }
        }
        return head;
    }

    public static Node removeDuplicates2(Node head) {
        //Write your code here
        Node temp = head;
        while (temp != null) {
            while (temp.next != null && temp.data == temp.next.data) {
                temp.next = temp.next.next;
            }
            temp = temp.next;
        }
        return head;
    }

    public static Node removeDuplicatesRecursive(Node head) {
        if (head == null || head.next == null) {
            return head;
        }
        if (head.data == head.next.data) {
            head.next = head.next.next;
            removeDuplicatesRecursive(head);
        } else {
            removeDuplicatesRecursive(head.next);
        }
        return head;
    }

    public static void main(String args[]) {
        int[] array = {1, 2, 2, 3, 3, 4};
        Node head = null;
        for (int data : array) {
            head = insert(head, data);
        }
/*
        Scanner sc = new Scanner(System.in);
        Node2 head = null;
        int T = sc.nextInt();
        while (T-- > 0) {
            int ele = sc.nextInt();
            head = insert(head, ele);
        }
*/
        display(head);
        head = removeDuplicatesRecursive(head);
        display(head);
    }

}
