package _20181210_w006.tutorial30daysofcode;

import java.util.Scanner;

public class Day26_NestedLogic {

    public static void main(String[] args) {
        // Enter your code here. Read input from STDIN. Print output to STDOUT.
        Scanner scanner = new Scanner(System.in);
        int returnDay = scanner.nextInt();
        int returnMonth = scanner.nextInt();
        int returnYear = scanner.nextInt();
        int expectedDay = scanner.nextInt();
        int expectedMonth = scanner.nextInt();
        int expectedYear = scanner.nextInt();
        scanner.close();
        System.out.println(calculateFine(
                returnDay, returnMonth, returnYear,
                expectedDay, expectedMonth, expectedYear
        ));
    }

    private static int calculateFine(
            int returnDay, int returnMonth, int returnYear,
            int expectedDay, int expectedMonth, int expectedYear
    ) {
        if (returnYear - expectedYear < 0) {
            return 0;
        } else if (returnYear - expectedYear == 0) {
            if (returnMonth - expectedMonth < 0) {
                return 0;
            } else if (returnMonth - expectedMonth == 0) {
                if (returnDay - expectedDay <= 0) {
                    return 0;
                } else {
                    return (returnDay - expectedDay) * 15;
                }
            } else {
                return (returnMonth - expectedMonth) * 500;
            }
        } else {
            return 10000;
        }
    }

}
