package _20181210_w006;

public class _000033_SaveThePrisoner {

    // Complete the saveThePrisoner function below.

    /* Wrong solution
    static int saveThePrisoner(int n, int m, int s) {
        if (n < m) m %= n;
        return s + m - 1;
    }
    */

    static int saveThePrisoner(int n, int m, int s) {
        if ((s + m - 1) % n == 0) {
            return n;
        } else {
            return (s + m - 1) % n;
        }
    }

    public static void main(String[] args) {
        int[][] testCases = {
                {5, 2, 1},
                {5, 2, 2},
                {7, 19, 2},
                {3, 7, 3},
                {352926151, 380324688, 94730870},
                {499999999, 999999997, 2},
                {499999999, 999999998, 2},
                {999999999, 999999999, 1}
        };
        for (int[] testCase : testCases) {
            int n = testCase[0];
            int m = testCase[1];
            int s = testCase[2];
            System.out.println(saveThePrisoner(n, m, s) + "\n");
        }
    }

}
