package _20181210_w006;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class _000026_PickingNumbers {
    public static void main(String[] args) {
/*
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));
        int n = Integer.parseInt(bufferedReader.readLine().trim());
        String[] aTemp = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");
        List<Integer> a = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            int aItem = Integer.parseInt(aTemp[i]);
            a.add(aItem);
        }
        int result = Result.pickingNumbers(a);
        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();
        bufferedReader.close();
        bufferedWriter.close();
*/
        List<Integer> a = Arrays.asList(new Integer[]{1, 1, 2, 2, 4, 4, 5, 5, 5});
        List<Integer> a2 = Arrays.asList(new Integer[]{4, 6, 5, 3, 3, 1});
        List<Integer> a3 = Arrays.asList(new Integer[]{1, 2, 2, 3, 1, 2});
        System.out.println(Result.pickingNumbers(a));
        System.out.println(Result.pickingNumbers(a2));
        System.out.println(Result.pickingNumbers(a3));
    }
}

class Result {
    /*
     * Complete the 'pickingNumbers' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts INTEGER_ARRAY a as parameter.
     */

    public static int pickingNumbers(List<Integer> list) {
        // Write your code here
        int maxCount = 0;

        if (list.size() >= 2) {
            Collections.sort(list);
            System.out.println(list);

            Integer lastValue = list.get(0);
            int count = 0;
            int lastCount = 0;
            for (Integer value : list) {
                if (value == lastValue) {
                    count++;
                    if (lastCount + count > maxCount) {
                        maxCount = lastCount + count;
                    }
                } else {
                    if (value - lastValue == 1) {
                        lastValue = value;
                        lastCount = count;
                        count = 1;
                        if (lastCount + count > maxCount) {
                            maxCount = lastCount + count;
                        }
                    } else {
                        lastValue = value;
                        lastCount = 0;
                        count = 1;
                    }
                }
            }

        }


        return maxCount;
    }
}

