package _20181210_w006;

public class _000031_BeautifulDaysAtTheMovies {

    // Complete the beautifulDays function below.
    static int beautifulDays(int startingDay, int endingDay, int divisor) {
        int countBeautifulDays = 0;
        for (int day = startingDay; day <= endingDay; day++) {
            int dayReverseValue = Integer.valueOf(
                    new StringBuilder(String.valueOf(day)).reverse().toString()
            );
            if (Math.abs(day - dayReverseValue) % divisor == 0) {
                countBeautifulDays++;
            }
        }
        return countBeautifulDays;
    }

    public static void main(String[] args) {
        int i = 20;
        int j = 23;
        int k = 6;
        System.out.println(beautifulDays(i, j, k));
    }

}
