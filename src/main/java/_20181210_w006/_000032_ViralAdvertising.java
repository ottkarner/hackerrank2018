package _20181210_w006;

public class _000032_ViralAdvertising {

    public static void main(String[] args) {
        System.out.println(viralAdvertising(3));
    }

    // Complete the viralAdvertising function below.
    static int viralAdvertising(int n) {
        int countLikes = 0;
        int countShared = 5;
        int countLikesCumulative = 0;
        for (int i = 1; i <= n; i++) {
            countLikes = (int) countShared / 2;
            countShared = countLikes * 3;
            countLikesCumulative += countLikes;
        }
        return countLikesCumulative;
    }

}
