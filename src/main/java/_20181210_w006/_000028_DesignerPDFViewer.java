package _20181210_w006;

public class _000028_DesignerPDFViewer {

    public static void main(String[] args) {
        int[] h;
        String word;

        h = new int[]{1, 3, 1, 3, 1, 4, 1, 3, 2, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5};
        word = "abc";
        System.out.println(designerPdfViewer2(h, word));

        h = new int[]{1, 3, 1, 3, 1, 4, 1, 3, 2, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 7};
        word = "zaba";
        System.out.println(designerPdfViewer2(h, word));
    }

    // Complete the designerPdfViewer function below.
    static int designerPdfViewer(int[] heights, String word) {
        int maxHeight = 0;
        for (int i = 0; i < word.length(); i++) {
            int height = heights[word.charAt(i) - 'a'];
            if (height > maxHeight) {
                maxHeight = height;
            }
        }
        return maxHeight * word.length();
    }

    static int designerPdfViewer2(int[] heights, String word) {
        int maxHeight = 0;
        for (char c : word.toCharArray()) {
            int height = heights[c - 'a'];
            if (height > maxHeight) {
                maxHeight = height;
            }
        }
        return maxHeight * word.length();
    }

}
