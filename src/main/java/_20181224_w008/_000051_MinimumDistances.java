package _20181224_w008;

import java.util.HashMap;
import java.util.Map;

public class _000051_MinimumDistances {

    public static void main(String[] args) {
        System.out.println(minimumDistances3(new int[]{3, 2, 1, 2, 3}));
        System.out.println(minimumDistances3(new int[]{7, 1, 3, 4, 1, 7}));
    }

    // Complete the minimumDistances function below.
    private static int minimumDistances(int[] a) {
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < a.length - 1; i++) {
            for (int j = i + 1; j < a.length; j++) {
                min = a[i] == a[j] && j - i < min ? j - i : min;
            }
        }
        return min < Integer.MAX_VALUE ? min : -1;
    }

    private static int minimumDistances2(int[] a) {
        int min = a.length;
        for (int i = 0; i < a.length - 1; i++) {
            for (int j = i + 1; j < a.length; j++) {
                if (a[i] == a[j]) {
                    if (min > (j - i)) {
                        min = j - i;
                        break;
                    }
                }
            }
        }
        return min < a.length ? min : -1;
    }

    private static int minimumDistances3(int[] a) {
        Map<Integer, Integer> map = new HashMap<>();
        int min = a.length + 1;
        for (int i = 0; i < a.length; i++) {
            if (map.containsKey(a[i]) && Math.abs(map.get(a[i]) - i) < min)
                min = Math.abs(map.get(a[i]) - i);
            map.put(a[i], i);
        }
        return min == a.length + 1 ? -1 : min;
    }

}