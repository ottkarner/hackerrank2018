package _20181224_w008.javaproficiency;

public class _034_JavaSubarray {

    public static void main(String[] args) {
/*
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }
        scanner.close();
*/
        int[] case1 = {1, 2, 3};
        int[] case2 = {1, -2, 4, -5, 1};
//        System.out.println(countNegativeSubArraysN3(case1));
        countNegativeSubArraysN3(case1);
        System.out.println();
        countNegativeSubArraysN2(case1);
//        System.out.println(countNegativeSubArraysN3(case2));
    }

    private static int countNegativeSubArraysN3(int[] arr) {
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = i; j < arr.length; j++) {
                int sum = 0;
                for (int k = i; k <= j; k++) {
                    sum += arr[k];
                }
                if (sum < 0) {
                    count++;
                }
                System.out.print(sum + " ");
            }
            System.out.println();
        }
        return count;
    }

    private static int countNegativeSubArraysN2(int[] arr) {
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            int sum = 0;
            for (int j = i; j < arr.length; j++) {
                sum += arr[j];
                if (sum < 0) {
                    count++;
                }
                System.out.print(sum + " ");
            }
            System.out.println();
        }
        return count;
    }

}

