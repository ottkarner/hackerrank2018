package _20181224_w008.javaproficiency;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class _035_JavaArraylist {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        List<List<Integer>> lines = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            int d = scanner.nextInt();
            List<Integer> values = new ArrayList<>(d);
            for (int j = 0; j < d; j++) {
                values.add(scanner.nextInt());
            }
            lines.add(values);
        }

        int q = scanner.nextInt();
        for (int i = 0; i < q; i++) {
            int x = scanner.nextInt();
            int y = scanner.nextInt();
            if (x > lines.size() || y > lines.get(x - 1).size()) {
                System.out.println("ERROR!");
            } else {
                System.out.println(lines.get(x - 1).get(y - 1));
            }
        }

        scanner.close();
    }

}