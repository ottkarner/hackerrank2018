package _20181224_w008;

public class _000055_HalloweenSale {

    public static void main(String[] args) {
        System.out.println(howManyGames(20, 3, 6, 80));
        System.out.println(howManyGames(20, 3, 6, 85));
        System.out.println(howManyGames(100, 19, 1, 180));
    }

    // Complete the howManyGames function below.
    private static int howManyGames(int p, int d, int m, int s) {
        // Return the number of games you can buy
        int count = 0;
        while (s >= p) {
            s -= p;
            count++;
            p = p - d > m ? p - d : m;
        }
        return count;
    }

}
