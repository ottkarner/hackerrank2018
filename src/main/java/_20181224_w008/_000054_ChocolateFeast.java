package _20181224_w008;

public class _000054_ChocolateFeast {

    public static void main(String[] args) {
        System.out.print(chocolateFeast(15, 3, 2) + " ");
        System.out.print(chocolateFeast(10, 2, 5) + " ");
        System.out.print(chocolateFeast(12, 4, 4) + " ");
        System.out.print(chocolateFeast(6, 2, 2) + " \n");
        System.out.print(chocolateFeastEffective(15, 3, 2) + " ");
        System.out.print(chocolateFeastEffective(10, 2, 5) + " ");
        System.out.print(chocolateFeastEffective(12, 4, 4) + " ");
        System.out.print(chocolateFeastEffective(6, 2, 2) + " \n");
    }

    // Complete the chocolateFeast function below.
    private static int chocolateFeastEffective(int money, int cost, int requiredWrappers) {
        int chocolates = money / cost;
        chocolates += (chocolates - 1) / (requiredWrappers - 1);
        return chocolates;
    }

    private static int chocolateFeast(int money, int cost, int requiredWrappers) {
        int chocolates = money / cost;
        int wrappers = chocolates;
        while (wrappers >= requiredWrappers) {
            int additionalChocolates = wrappers / requiredWrappers;
            chocolates += additionalChocolates;
            wrappers %= requiredWrappers;
            wrappers += additionalChocolates;
        }
        return chocolates;
    }

    private static int chocolateFeastOriginal(int n, int c, int m) {
        int chocolates = n / c;
        int wrappers = chocolates;
        while (true) {
            int additionalChocolates = wrappers / m;
            chocolates += additionalChocolates;
            if (wrappers < m) break;
            wrappers = wrappers % m;
            wrappers += additionalChocolates;
        }
        return chocolates;
    }

}
