package _20181224_w008;

public class _000050_FormingAMagicSquare {

    public static void main(String[] args) {
        System.out.println(formingMagicSquare(new int[][]{
                {5, 3, 4},
                {1, 5, 8},
                {6, 4, 2}
        }));
        System.out.println(formingMagicSquare(new int[][]{
                {4, 9, 2},
                {3, 5, 7},
                {8, 1, 5}
        }));
        System.out.println(formingMagicSquare(new int[][]{ // Testcase 3
                {4, 4, 7},
                {3, 1, 5},
                {1, 7, 9}
        }));
        System.out.println(formingMagicSquare(new int[][]{ // Testcase 4
                {2, 2, 7},
                {8, 6, 4},
                {1, 2, 9}
        }));
        System.out.println(formingMagicSquare(new int[][]{ // Testcase 13
                {9, 3, 3},
                {3, 3, 2},
                {1, 8, 1}
        }));
    }

    private static int formingMagicSquare(int[][] square) {
        // Get the square values of the square, magic square and magic square reflection
        int[] squareSideNumbers = getSquareSides(square);
        int[] magicSquareSides = getSquareSides(new int[][]{
                {8, 3, 4},
                {1, 5, 9},
                {6, 7, 2}
        });
        int[] reflectedMagicSquareSides = getSquareSides(new int[][]{
                {4, 3, 8},
                {9, 5, 1},
                {2, 7, 6}
        });

        // Find the minimum cost of the conversion of the sides
        // Compare the square to magic square and its reflection and all the rotations of both
        int minCost = Integer.MAX_VALUE;
        int cost = 0;
        for (int i = 0; i < 4; i++) {
//            System.out.println();
//            System.out.println(Arrays.toString(squareSideNumbers));
            cost = calculateSidesConversionCost(squareSideNumbers, magicSquareSides);
            minCost = minCost > cost ? cost : minCost;
//            System.out.println(Arrays.toString(magicSquareSides) + " -> cost: " + cost + " minCost: " + minCost);
            cost = calculateSidesConversionCost(squareSideNumbers, reflectedMagicSquareSides);
            minCost = minCost > cost ? cost : minCost;
//            System.out.println(Arrays.toString(reflectedMagicSquareSides) + " ->cost: " + cost + " minCost: " + minCost);

            magicSquareSides = rotateArrayRight(magicSquareSides, 3);
            reflectedMagicSquareSides = rotateArrayRight(reflectedMagicSquareSides, 3);
        }

        // Calculate additional cost if the number in the square's centre is not 5
        if (square[1][1] != 5) {
            minCost += Math.abs(5 - square[1][1]);
        }

        return minCost;
    }

    private static int calculateSidesConversionCost(int[] square1Sides, int[] square2Sides) {
        int cost = 0;
        for (int i = 0; i < square1Sides.length && i < square2Sides.length; i++) {
            if (square1Sides[i] != square2Sides[i]) {
                cost += Math.abs(square2Sides[i] - square1Sides[i]);
            }
        }
        return cost;
    }

    private static int[] rotateArrayRight(int[] array, int steps) {
        int[] rotateArray = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            rotateArray[(i + (steps - 1)) % array.length] = array[i];
        }
        return rotateArray;
    }

    private static int[] getSquareSides(int[][] square) {
        int[] squareSideNumbers = new int[8];
        for (int i = 0; i <= 2; i++) {
            squareSideNumbers[i] = square[0][i]; // Top row
        }
        squareSideNumbers[3] = square[1][2]; // Right middle
        for (int i = 4; i <= 6; i++) {
            squareSideNumbers[i] = square[2][6 - i]; // Bottom row
        }
        squareSideNumbers[7] = square[1][0]; // Left middle
        return squareSideNumbers;
    }

}

