package _20181224_w008;

public class _000052_Encryption {

    public static void main(String[] args) {
        System.out.println(encryption("haveaniceday"));
        System.out.println(encryption("if man was  meant to stay on the ground god would have given us roots"));
        System.out.println(encryption("feedthedog"));
        System.out.println(encryption("chillout"));
    }

    // Complete the encryption function below.
    private static String encryption(String s) {
        s = s.replaceAll("\\s+", "");
        System.out.println("\n" + s + " " + s.length());

        int column = (int) Math.ceil(Math.sqrt(s.length()));
        String encryptedS = "";
        for (int i = 0; i < column; i++) {
            for (int j = i; j < s.length(); j += column) {
                encryptedS += s.charAt(j);
            }
            encryptedS += " ";
        }
        return encryptedS;
    }

    // Complete the encryption function below.
    private static String encryptionUnoptimized(String s) {
        s = s.replaceAll("\\s+", "");
        System.out.println("\n" + s + " " + s.length());

        double sqrtL = Math.sqrt(s.length());
        int sqrtLFloor = (int) Math.floor(sqrtL);
        int sqrtLCeil = (int) Math.ceil(sqrtL);
        System.out.println(sqrtL + " " + sqrtLFloor + " " + sqrtLCeil);

        int row = sqrtLFloor;
        int column = sqrtLCeil;
        if (row * column < s.length()) {
            row = column;
        }

        String encryptedS = "";
        for (int i = 0; i < column; i++) {
            for (int j = i; j < s.length(); j += column) {
                encryptedS += s.charAt(j);
            }
            encryptedS += " ";
        }

        return encryptedS;
    }

}
