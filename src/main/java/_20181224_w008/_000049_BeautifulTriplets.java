package _20181224_w008;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class _000049_BeautifulTriplets {

    public static void main(String[] args) {
        System.out.println(beautifulTriplets(1, new int[]{2, 2, 3, 4, 5}));
        System.out.println(beautifulTriplets2(1, new int[]{2, 2, 3, 4, 5}));
        System.out.println(beautifulTriplets(3, new int[]{1, 2, 4, 5, 7, 8, 10, 11}));
        System.out.println(beautifulTriplets2(3, new int[]{1, 2, 4, 5, 7, 8, 10, 11}));
    }

    // Complete the beautifulTriplets function below.
    private static int beautifulTriplets(int d, int[] arr) {
        int count = 0;
        for (int i = 0; i < arr.length - 2; i++) {
            for (int j = 0; j < arr.length - 1; j++) {
                if (arr[j] - arr[i] == d) {
                    for (int k = 0; k < arr.length; k++) {
                        if (arr[k] - arr[j] == d) {
                            count++;
                            break;
                        }
                    }
                    break;
                }
            }
        }
        return count;
    }

    private static int beautifulTriplets2(int d, int[] arr) {
        int count = 0;
        for (int i = 0; i < arr.length - 2; i++) {
            if (Arrays.binarySearch(arr, arr[i] + d) > 0 && Arrays.binarySearch(arr, arr[i] + 2 * d) > 0) {
                count++;
            }
        }
        return count;
    }

}
