package _20181224_w008;

import java.io.*;
import java.util.Arrays;

public class _000053_BiggerIsGreater {

    public static void main(String[] args) throws IOException {
        String[] words = {
                "abcd",
                "ab", "bb", "hefg", "dhck", "dkhc",
                "lmno", "dcba", "dcbb", "abdc", "abcd", "fedcbabcd"
        };

//        String[] words = {"rebjvsszebhehuojrkkhszxltyqfdvayusylgmgkdivzlpmmtvbsavxvydldmsym"};
//        String[] words = {"zzzayybbaa"};
//        String[] words = {"9357742"};
//        String[] words = {"12342"};
//        String[] words = {"2751"};

        for (String word : words) {
            System.out.print(word + " -> ");
            System.out.println(biggerIsGreaterN3(word));
            System.out.print(word + " -> ");
            System.out.println(biggerIsGreaterN2(word));
        }

//        writeToFile();
    }

    // Complete the biggerIsGreater function below.
    private static String biggerIsGreaterN2(String word) {
        char[] chars = word.toCharArray();
        for (int i = chars.length - 1; i > 0; i--) {
            if (chars[i - 1] < chars[i]) {
                for (int k = chars.length - 1; k > i - 1; k--) {
                    if (chars[i - 1] < chars[k]) {
                        char tempChar = chars[i - 1];
                        chars[i - 1] = chars[k];
                        chars[k] = tempChar;
                        break;
                    }
                }
                Arrays.sort(chars, i, chars.length);
                return String.valueOf(chars);

            }
        }
        return "no answer";
    }

    private static String biggerIsGreaterN3(String word) {
        char[] chars = word.toCharArray();
        for (int i = chars.length - 1; i >= 1; i--) {
            for (int j = i - 1; j >= 0; j--) {
                if (chars[j] >= chars[i]) {
                    break;
                }
                if (chars[j] < chars[i]) {
                    Arrays.sort(chars, j + 1, chars.length);
                    for (int k = j + 1; k < chars.length; k++) {
                        if (chars[j] < chars[k]) {
                            char tempChar = chars[j];
                            chars[j] = chars[k];
                            chars[k] = tempChar;
                            return String.valueOf(chars);
                        }
                    }
                }
            }
        }
        return "no answer";
    }

    private static void writeToFile() throws IOException {
        String file = "C:\\Users\\devel\\Desktop\\hackerrank\\20181224_w008\\000053__input01.txt";
        String fileOutput = "C:\\Users\\devel\\Desktop\\hackerrank\\20181224_w008\\000053__output01_ott.txt";
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileOutput));
        String word = "";
        try {
            int n = Integer.valueOf(bufferedReader.readLine());
            for (int i = 0; i < n; i++) {
                word = bufferedReader.readLine();
                bufferedWriter.write(biggerIsGreaterN2(word));
                bufferedWriter.newLine();
            }
        } finally {
            bufferedWriter.close();
            bufferedReader.close();
        }
    }

}
