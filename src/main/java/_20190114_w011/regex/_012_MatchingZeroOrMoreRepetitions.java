package _20190114_w011.regex;

public class _012_MatchingZeroOrMoreRepetitions {

    public static void main(String[] args) {
        RegexTester2 regexTester2 = new RegexTester2();
        String regexPattern = "^\\d{2,}[a-z]*[A-Z]*$";

        regexTester2.checker("", regexPattern);
    }

}
