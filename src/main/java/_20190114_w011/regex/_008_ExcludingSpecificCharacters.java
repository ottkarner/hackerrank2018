package _20190114_w011.regex;

public class _008_ExcludingSpecificCharacters {

    public static void main(String[] args) {
        RegexTester2 regexTester2 = new RegexTester2();
        regexTester2.checker("think?", "^[^0-9][^aeiou][^bcDF][^\\s][^AEIOU][^.,]$");
    }

}
