package _20190114_w011.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class _003_MatchingDigitsAndNonDigitCharacters {

    public static void main(String[] args) {
        RegexTester2 tester = new RegexTester2();
        tester.checker("06-11-2015", "(\\d\\d\\D){2}\\d{4}");
    }

}

class RegexTester2 {

    public void checker(String testString, String regexPattern) {
        Pattern p = Pattern.compile(regexPattern);
        Matcher m = p.matcher(testString);
        System.out.println(m.find());
    }

}