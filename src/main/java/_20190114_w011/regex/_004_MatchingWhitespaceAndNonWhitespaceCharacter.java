package _20190114_w011.regex;

public class _004_MatchingWhitespaceAndNonWhitespaceCharacter {

    public static void main(String[] args) {
        RegexTester2 regexTester2 = new RegexTester2();
        regexTester2.checker("12 11 15", "(\\S\\S\\s){2}\\S\\S");
    }

}
