package _20190114_w011.regex;

public class _014_MatchingEndingItems {

    public static void main(String[] args) {
        RegexTester2 regexTester2 = new RegexTester2();
        String regexPattern = "^[a-zA-Z]*s$";

        regexTester2.checker("1ess", regexPattern);
    }

}
