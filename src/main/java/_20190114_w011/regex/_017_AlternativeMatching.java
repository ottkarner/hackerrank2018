package _20190114_w011.regex;

public class _017_AlternativeMatching {

    public static void main(String[] args) {
        RegexTester2 regexTester = new RegexTester2();
        String regexPattern = "^(Mr|Mrs|Ms|Dr|Er)\\.[a-zA-Z]+$";

        regexTester.checker("Mr.V.K. Doshi", regexPattern);
        regexTester.checker("Mr.VKDoshi.", regexPattern);
    }

}
