package _20190114_w011.regex;

public class _007_MatchingSpecificCharacters {

    public static void main(String[] args) {
        RegexTester2 regexTester2 = new RegexTester2();
        String regexPattern = "^[1-3][0-2][xs0][30Aa][xsu][.,]$";

        regexTester2.checker("1203x.", regexPattern);
        regexTester2.checker("3000s..", regexPattern);
        regexTester2.checker("13000u.", regexPattern);
    }

}
