package _20190114_w011.regex;

public class _013_MatchingOneOrMoreRepetitions {

    public static void main(String[] args) {
        RegexTester2 regexTester2 = new RegexTester2();
        String regexPattern = "^\\d+[A-Z]+[a-z]+$";

        regexTester2.checker("", regexPattern);
    }

}
