package _20190114_w011.regex;

public class _018_MatchingSameTextAgainAndAgain {

    public static void main(String[] args) {
        RegexTester2 regexTester = new RegexTester2();
        String regexPattern =
                "^([a-z])(\\w)(\\s)(\\W)(\\d)(\\D)([A-Z])([a-zA-Z])([aeiouAEIOU])(\\S)\\1\\2\\3\\4\\5\\6\\7\\8\\9\\10$";

        regexTester.checker("ab #1?AZa$ab #1?AZa$", regexPattern);
    }

}
