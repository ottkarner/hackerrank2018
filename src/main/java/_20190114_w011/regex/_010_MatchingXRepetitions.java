package _20190114_w011.regex;

public class _010_MatchingXRepetitions {

    public static void main(String[] args) {
        RegexTester2 regexTester2 = new RegexTester2();
        String regexPattern = "^[a-zA-Z02468]{40}[13579\\s]{5}$";

        regexTester2.checker("x4202v2A22A8a6aaaaaa2G2222m222qwertyYuIo1395779", regexPattern);
        regexTester2.checker("x4202v2A22A9a6aaaaaa2G2222m222qwertyYuIo13957", regexPattern);
    }

}
