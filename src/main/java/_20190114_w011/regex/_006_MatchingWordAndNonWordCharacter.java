package _20190114_w011.regex;

public class _006_MatchingWordAndNonWordCharacter {

    public static void main(String[] args) {
        RegexTester2 regexTester2 = new RegexTester2();
        regexTester2.checker("www.hackerrank.com", "\\w{3}\\W\\w{10}\\W\\w{3}");
    }

}
