package _20190114_w011.regex;

public class _009_MatchingCharacterRanges {

    public static void main(String[] args) {
        RegexTester2 regexTester2 = new RegexTester2();
        String regexPattern = "^[a-z][1-9][^a-z][^A-Z][A-Z]";

        regexTester2.checker("h4CkR", regexPattern);
        regexTester2.checker("h4CkRank", regexPattern);
        regexTester2.checker("q9$?WWe", regexPattern);
        regexTester2.checker("z9$?ZWe", regexPattern);
        regexTester2.checker("a4$?ZWe41.l;'a", regexPattern);
        regexTester2.checker("a0$?ZWe", regexPattern);
    }

}
