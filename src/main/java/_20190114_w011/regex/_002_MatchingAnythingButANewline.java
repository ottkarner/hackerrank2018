package _20190114_w011.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class _002_MatchingAnythingButANewline {

    public static void main(String[] args) {
        RegexTester tester = new RegexTester();
        String regexPattern;

        regexPattern = ".{3}\\..{3}\\..{3}\\..{3}";
        tester.check("123.456.abc.def", regexPattern);
        tester.check("1123.456.abc.def", regexPattern);

        regexPattern = "(.{3}\\.){3}.{3}";
        tester.check("123.456.abc.def", regexPattern);
        tester.check("1123.456.abc.def", regexPattern);
    }

}

class RegexTester {

    public void check(String testString, String regexPattern) {
        Pattern p = Pattern.compile(regexPattern);
        Matcher m = p.matcher(testString);
        boolean match = m.matches();
        System.out.format("%s", match);
        System.out.println();
    }

}