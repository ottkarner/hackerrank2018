package _20190114_w011.regex;

public class _015_MatchingWordBoundaries {

    public static void main(String[] args) {
        RegexTester2 regexTester2 = new RegexTester2();
        String regexPattern = "\\b[aeiouAEIOU][a-zA-Z]*\\b";

        regexTester2.checker("Iamdead-4u", regexPattern);
    }

}
