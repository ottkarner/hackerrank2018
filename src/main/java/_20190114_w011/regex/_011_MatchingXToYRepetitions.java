package _20190114_w011.regex;

public class _011_MatchingXToYRepetitions {

    public static void main(String[] args) {
        RegexTester2 regexTester2 = new RegexTester2();
        String regexPattern = "^\\d{1,2}[a-zA-Z]{3,}\\.{0,3}$";

        regexTester2.checker("3threeormorealphabets.", regexPattern);
        regexTester2.checker("3threeormorealphabetsACNSDNPINQCPIQNCPNQPCINQPICNPIQNCPQINCQPC", regexPattern);
        regexTester2.checker("12sacsacACSASACS...", regexPattern);
    }

}
