package _20190114_w011.regex;

public class _016_CapturingAndNonCapturingGroups {

    public static void main(String[] args) {
        RegexTester2 regexTester = new RegexTester2();
        String regexPattern = "(ok){3}";

        regexTester.checker("okokok! cya", regexPattern);
    }

}
