package _20190114_w011.javaproficiency;

import java.util.Arrays;
import java.util.Scanner;

public class _05x_Java1DArrayPart2 {

    public static void main(String[] args) {
        task();
//        taskWithScanner();
    }

    public static boolean canWin(int leap, int[] game) {
        // Return true if you can win the game; otherwise, return false.

        if (leap <= maxObstacle(leap, game)) return false;

        boolean isPossible = true;
        int pos = 0;
        int endPos = game.length - 1;

        while (isPossible && pos <= endPos) {
            if (pos >= endPos) return true;

            if (game[pos + leap] == 0) {
                pos += leap;
            }
        }


        return true;
    }

    private static int maxObstacle(int leap, int[] game) {
        int obstacle = 0, maxObstacle = 0;
        for (int i = 0; i < game.length - 1; i++) {
            obstacle = game[i + 1] == 1 ? obstacle + 1 : 0;
            maxObstacle = maxObstacle < obstacle ? obstacle : maxObstacle;
        }
        return maxObstacle;
    }

    private static void task() {
        int[] leaps = {3, 5, 3, 1};
        int[][] testCases = {
                {0, 0, 0, 0, 0}, // YES
                {0, 0, 0, 1, 1, 1}, // YES
                {0, 0, 1, 1, 1, 0}, // NO
                {0, 1, 0} // NO
        };

        for (int i = 0; i < testCases.length; i++) {
            int leap = leaps[i];
            int[] game = testCases[i];
            System.out.print("leap=" + leap + " game=" + Arrays.toString(game) + " -> ");
            System.out.println((canWin(leap, game)) ? "YES" : "NO");


        }
    }

    private static void taskWithScanner() {
        Scanner scan = new Scanner(System.in);
        int q = scan.nextInt();
        while (q-- > 0) {
            int n = scan.nextInt();
            int leap = scan.nextInt();

            int[] game = new int[n];
            for (int i = 0; i < n; i++) {
                game[i] = scan.nextInt();
            }

            System.out.println((canWin(leap, game)) ? "YES" : "NO");
        }
        scan.close();
    }

}
