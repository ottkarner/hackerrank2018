package _20190114_w011.javaproficiency;

import java.util.*;

public class _057_JavaDequeue {

    public static void main(String[] args) {
        System.out.println(solution());
        System.out.println(efficientSolution());
    }

    private static int solution() {
        Deque deque = new ArrayDeque<>();
        int[] values = {5, 3, 5, 2, 3, 2};
//        int[] values = {1, 2, 4, 4, 5, 6, 7};
//        int[] values = {1, 2, 3, 4, 5, 6};
//        int[] values = {7, 7, 7, 7, 7, 7};
        int n = values.length;
        int m = 3;

        int maxCount = 0;
        int subArrayUniqeIntCount = 0;
        for (int i = 0; i < n; i++) {
            int num = values[i];

            if (i >= m) {
                int removableInt = (int) deque.poll();
                if (!deque.contains(removableInt)) {
                    subArrayUniqeIntCount--;
                }

                // System.out.print("-> ");
            }

            if (!deque.contains(num)) {
                subArrayUniqeIntCount++;
            }
            deque.add(num);
            maxCount = maxCount < subArrayUniqeIntCount ? subArrayUniqeIntCount : maxCount;

            /*System.out.println(deque + " s:" + deque.size() +
                    " countUnique:" + subArrayUniqeIntCount +
                    " maxCount:" + maxCount);*/
        }

        return maxCount;
    }

    private static int efficientSolution() {
        Deque deque = new ArrayDeque<>();
        int[] values = {5, 3, 5, 2, 3, 2};
//        int[] values = {1, 2, 4, 4, 5, 6, 7};
//        int[] values = {1, 2, 3, 4, 5, 6};
//        int[] values = {7, 7, 7, 7, 7, 7};
        int n = values.length;
        int m = 3;

        int maxUniqueIntCount = 0;
        int uniqueIntCount;
        Map<Integer, Integer> subArrayIntegersCounts = new HashMap<>();
        Integer intCount;
        for (int i = 0; i < n; i++) {
            int num = values[i];

            if (i >= m) {
                int removableInt = (int) deque.removeLast();

                intCount = subArrayIntegersCounts.get(removableInt);
                if (intCount != null) {
                    if (intCount == 1) {
                        subArrayIntegersCounts.remove(removableInt);
                    } else {
                        subArrayIntegersCounts.put(removableInt, --intCount);
                    }
                }

                // System.out.print("-> ");
            }

            intCount = subArrayIntegersCounts.get(num);
            if (intCount == null) {
                subArrayIntegersCounts.put(num, 1);
            } else {
                subArrayIntegersCounts.put(num, ++intCount);
            }

            deque.addFirst(num);

            uniqueIntCount = subArrayIntegersCounts.size();
            maxUniqueIntCount = maxUniqueIntCount < uniqueIntCount ? uniqueIntCount : maxUniqueIntCount;

            /*System.out.println(deque + " s:" + deque.size() +
                    " countUnique:" + uniqueIntCount +
                    " maxUniqueIntCount:" + maxUniqueIntCount);*/
        }

        return maxUniqueIntCount;
    }

}
