package _20181203_w005.javaproficiency;

public class _028_JavaSingletonPattern {

    public static void main(String[] args) {
        Singleton singleton = Singleton.getSingleton();
        singleton.str = "Tere!";

        Singleton singleton2 = Singleton.getSingleton();
        System.out.println(singleton2.str);
    }

}

class Singleton {

    private static Singleton singleton;

    private Singleton() {
    }

    public static Singleton getSingleton() {
        if (singleton == null) singleton = new Singleton();
        return singleton;
    }

    public String str;

}
