package _20181203_w005;

public class _000025_CatsAndAMouse {

/*
    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));
        int q = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
        for (int qItr = 0; qItr < q; qItr++) {
            String[] xyz = scanner.nextLine().split(" ");
            int x = Integer.parseInt(xyz[0]);
            int y = Integer.parseInt(xyz[1]);
            int z = Integer.parseInt(xyz[2]);
            String result = catAndMouse(x, y, z);
            bufferedWriter.write(result);
            bufferedWriter.newLine();
        }
        bufferedWriter.close();
        scanner.close();
    }
*/

    public static void main(String[] args) {
        int[][] queries = new int[][]{
                {1, 2, 3},
                {1, 3, 2}
        };

        for (int[] query : queries) {
            int x = query[0];
            int y = query[1];
            int z = query[2];
            System.out.println(catAndMouse(x, y, z));
        }
    }

    // Complete the catAndMouse function below.
    static String catAndMouse(int x, int y, int z) {
        if (Math.abs(x - z) < Math.abs(y - z)) return "Cat A";
        else if (Math.abs(x - z) > Math.abs(y - z)) return "Cat B";
        else return "Mouse C";
    }

}
