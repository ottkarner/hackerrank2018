package _20181203_w005.tutorial30daysofcode;

import java.util.Scanner;

class Node {
    Node left, right;
    int data;

    Node(int data) {
        this.data = data;
        left = right = null;
    }
}

public class Day22_BinarySearchTrees {
    public static int getHeight(Node root) {
        //Write your code here
        if (root == null || (root.right == null && root.left == null)) return 0;
        int leftHeight = getHeight(root.left);
        int rightHeight = getHeight(root.right);
        return 1 + ((leftHeight >= rightHeight) ? leftHeight : rightHeight);
    }

    public static Node insert(Node root, int data) {
        if (root == null) {
            return new Node(data);
        } else {
            Node cur;
            if (data <= root.data) {
                cur = insert(root.left, data);
                root.left = cur;
            } else {
                cur = insert(root.right, data);
                root.right = cur;
            }
            return root;
        }
    }

    public static void main(String args[]) {
        int[] array = {3, 5, 2, 1, 4, 6, 7};
        Node root = null;
        for (int data : array) {
            root = insert(root, data);
        }
        int height = getHeight(root);
        System.out.println(height);
/*
        Scanner sc = new Scanner(System.in);
        int T = sc.nextInt();
        Node root = null;
        while (T-- > 0) {
            int data = sc.nextInt();
            root = insert(root, data);
        }
*/
    }

}

