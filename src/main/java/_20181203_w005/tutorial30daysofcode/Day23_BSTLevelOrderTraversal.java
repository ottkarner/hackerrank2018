package _20181203_w005.tutorial30daysofcode;

import java.util.LinkedList;
import java.util.Queue;

import static _20181203_w005.tutorial30daysofcode.Day22_BinarySearchTrees.insert;

// Last two imports the last day class and method - let's reuse those!

public class Day23_BSTLevelOrderTraversal {

    public static void main(String args[]) {
        int[] array = {3, 5, 4, 7, 2, 1};
        Node root = null;
        for (int data : array) {
            root = insert(root, data);
        }
        levelOrder(root);
    }

    static Queue<Node> queue = new LinkedList<>();

    static void enqueueNode(Node node) {
        queue.add(node);
    }

    static Node dequeueNode() {
        return queue.remove();
    }

    static void levelOrder(Node root) {
        //Write your code here
        if (root != null) {
            // enqueue current root
            enqueueNode(root);

            Node current;
            // while there are nodes to process
            while (!queue.isEmpty()) {
                // dequeue next node
                current = dequeueNode();
                System.out.print(current.data + " ");

                // enqueue child elements from next level in order

                // tree has non-empty left subtree
                if (current.left != null) {
                    enqueueNode(current.left);
                }
                // tree has non-empty right subtree
                if (current.right != null) {
                    enqueueNode(current.right);
                }
            }
        }
    }

}

