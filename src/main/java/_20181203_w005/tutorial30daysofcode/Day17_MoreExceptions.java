package _20181203_w005.tutorial30daysofcode;

import java.util.Scanner;

public class Day17_MoreExceptions {

    /*
        public static void main(String[] args) {
            Scanner in = new Scanner(System.in);
            int t = in.nextInt();
            while (t-- > 0) {
                int n = in.nextInt();
                int p = in.nextInt();
                Calculator myCalculator = new Calculator();
                try {
                    int ans = myCalculator.power(n, p);
                    System.out.println(ans);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
            in.close();
        }
    */

    public static void main(String[] args) {
        int[][] intPairs = new int[][]{
                {3, 5},
                {2, 4},
                {-1, -2},
                {-1, 3},
                {1, 0}
        };
        for (int[] intPair : intPairs) {
            int n = intPair[0];
            int p = intPair[1];
            Calculator myCalculator = new Calculator();
            try {
                int ans = myCalculator.power(n, p);
                System.out.println(ans);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

}

class Calculator {

    public int power(int n, int p) throws Exception {
        if (n < 0 || p < 0) {
            throw new Exception("n and p should be non-negative");
        } else if (n == 0 && p == 0) {
            throw new Exception("n and p can't be 0 at the same time");
        } else if (n > 0 && p == 0) {
            return 1;
        } else {
            int result = n;
            for (int i = 1; i < p; i++) {
                result *= n;
            }
            return result;
        }
    }

}
