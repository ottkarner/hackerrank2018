package _20181203_w005;

public class _000023_CountingValleys {

    /*
        private static final Scanner scanner = new Scanner(System.in);

        public static void main(String[] args) throws IOException {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

            int n = scanner.nextInt();
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            String s = scanner.nextLine();

            int result = countingValleys(n, s);

            bufferedWriter.write(String.valueOf(result));
            bufferedWriter.newLine();

            bufferedWriter.close();

            scanner.close();
        }
    */

    public static void main(String[] args) {
        Object[][] stepsNumbersAndPathsDescriptions = new Object[][]{
                {8, "UDDDUDUU"}
        };
        for (Object[] stepsNumberAndPathDescription : stepsNumbersAndPathsDescriptions) {
            int n = (int) stepsNumberAndPathDescription[0];
            String s = (String) stepsNumberAndPathDescription[1];
            System.out.println(countingValleys(n, s));
        }
    }

    // Complete the countingValleys function below.
    static int countingValleys(int n, String s) {
        char[] moves = s.toCharArray();
        int stepsFromSeaLevel = 0;
        int valleyCount = 0;
        for (char move : moves) {
            if (move == 'U') {
                if (stepsFromSeaLevel + 1 == 0) valleyCount++;
                stepsFromSeaLevel++;
            }
            if (move == 'D') {
                stepsFromSeaLevel--;
            }
        }
        return valleyCount;
    }

}