package _20190304_w018;

public class _000079_PalindromeIndex {

    public static void main(String[] args) {
        /*String[] strings = {
                "quyjjdcgsvvsgcdjjyq",
                "hgygsvlfwcwnswtuhmyaljkqlqjjqlqkjlaymhutwsnwcflvsgygh",
                "fgnfnidynhxebxxxfmxixhsruldhsaobhlcggchboashdlurshxixmfxxxbexhnydinfngf",
                "bsyhvwfuesumsehmytqioswvpcbxyolapfywdxeacyuruybhbwxjmrrmjxwbhbyuruycaexdwyfpaloyxbcpwsoiqtymhesmuseufwvhysb",
                "fvyqxqxynewuebtcuqdwyetyqqisappmunmnldmkttkmdlnmnumppasiqyteywdquctbeuwenyxqxqyvf",
                "mmbiefhflbeckaecprwfgmqlydfroxrblulpasumubqhhbvlqpixvvxipqlvbhqbumusaplulbrxorfdylqmgfwrpceakceblfhfeibmm",
                "tpqknkmbgasitnwqrqasvolmevkasccsakvemlosaqrqwntisagbmknkqpt",
                "lhrxvssvxrhl",
                "prcoitfiptvcxrvoalqmfpnqyhrubxspplrftomfehbbhefmotfrlppsxburhyqnpfmqlaorxcvtpiftiocrp",
                "kjowoemiduaaxasnqghxbxkiccikxbxhgqnsaxaaudimeowojk"
        };*/
        /*String[] strings = {
                "fyjwtatuieusvfqaeynaaiiaanyeaqfvsueutatwjyf",
                "qaaiyrpadovfjrmgkildtkseysejdtrpltptujlxxljutptlprtdjesyeskdlikgmrjfvodapryiaaq",
                "llhrxcreddwkcronujfkwbdswoowsdbwkfjunorckwdderxrhll",
                "qasfhkfcojhntlfkaydtepsfsleipymwsopposwmypielsfspetdykfltnhjocfkhfsaq",
                "broifqivnnvifiorb",
                "bglgcwnmpobohqefrglsaaaaslgrfeqhobopmnwcglgb",
                "bthvmywukfwrkslaiialskwfkuwymvhtb",
                "uxxdlselxmwyiguugtpsypfudffswvwyswyyiiyywsywvsffdufpysptguugiywmxlesldxxu",
                "rvscdpyljqglgmiktfndsmfnkgmubrruloqptgohsgneocoyyocoengshogtpqolurrbumgknfmsdntkimglgqjlypdcsvr",
                "qmdpbsswvmqtyhkobqeijjieqbokhytqmvwssbdmq"
        };*/
        /*for (String string : strings) {
            System.out.println(
                    palindromeIndex(string)
            );
        }*/
        // The result should be 0 !!!
        System.out.println(palindromeIndex2("wcwwc"));
        // The result should be 8
        System.out.println(palindromeIndex2("hgygsvlfwcwnswtuhmyaljkqlqjjqlqkjlaymhutwsnwcflvsgygh"));
        // The result should be 28
        System.out.println(palindromeIndex2("uxxdlselxmwyiguugtpsypfudffswvwyswyyiiyywsywvsffdufpysptguugiywmxlesldxxu"));
    }

    // Complete the palindromeIndex function below.
    static int palindromeIndex(String s) {
        if (s.length() == 1) return -1;
        if (s.length() == 2) return s.charAt(0) == s.charAt(1) ? -1 : 1;

        int removableCharIndex = -1;

        if (s.length() > 2) {
            int left = 0;
            int right = s.length() - 1;

            while (left < right) {

                if (s.charAt(left) != s.charAt(right)) {
                    if (removableCharIndex != -1) return -1;

                    if (s.charAt(left) == s.charAt(right - 1) &&
                            s.charAt(left + 1) == s.charAt(right - 2)) {
                        removableCharIndex = right;
                        right--;
                    } else if (s.charAt(right) == s.charAt(left + 1) &&
                            s.charAt(right - 1) == s.charAt(left + 2)) {
                        removableCharIndex = left;
                        left++;
                    } else return -1;
                } else {
                    left++;
                    right--;
                }

            }
        }

        return removableCharIndex;
    }

    static int palindromeIndex2(String s) {
        // Compare letters starting from
        // beginning of the string (left)
        // and the end of the string (right).
        int pos = -1;
        int left = 0;
        int right = s.length() - 1;
        while (left < right) {
            char cl = s.charAt(left);
            char cr = s.charAt(right);
            if (cl == cr) {
                // The letters matched.
                // Move left and right
                // towards each other.
                ++left;
                --right;
            }
            if (cl != cr) {
                // The letters did not match.
                if (pos > -1) {
                    // This is the second letter
                    // difference.
                    return -1;
                }
                if (cr == s.charAt(left + 1) &&
                        s.charAt(right - 1) == s.charAt(left + 2)) {
                    // We can find a matching letter
                    // if we skip one letter from
                    // the left. We checked that
                    // after skipping a letter
                    // two next letters form
                    // a palindrome.
                    pos = left;
                    ++left;
                } else if (cl == s.charAt(right - 1) &&
                        s.charAt(left + 1) == s.charAt(right - 2)) {
                    // We can find a matching letter
                    // if we skip one letter from
                    // the right.  We checked that
                    // after skipping a letter
                    // two next letters form
                    // a palindrome.
                    pos = right;
                    --right;
                } else {
                    // The string is not a palindrome.
                    return -1;
                }
            }
        }
        // If the string could be made a palindrome
        // by replacing one letter we will return
        // the letter position here.
        return pos;
    }

}
