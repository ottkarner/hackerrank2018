package _20190304_w018.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class _036_SplitThePhoneNumbers {

    public static void main(String[] args) {
        String[] strings = {
                "1 877 2638277",
                "91-011-23413627"
        };
        for (String string : strings) {
            System.out.println(produceResult(string));
        }
    }

    private static String produceResult(String string) {
        Pattern pattern = Pattern.compile("^(\\d{1,3})[ -](\\d{1,3})[ -](\\d{4,10})$");
        Matcher matcher = pattern.matcher(string);
        StringBuilder result = new StringBuilder();
        if (matcher.matches()) {
            result.append("CountryCode=" + matcher.group(1));
            result.append(",");
            result.append("LocalAreaCode=" + matcher.group(2));
            result.append(",");
            result.append("Number=" + matcher.group(3));
        }
        return result.toString();
    }

}
