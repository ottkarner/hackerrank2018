package _20190304_w018;

public class _000080_TwoStrings {

    public static void main(String[] args) {
        System.out.println(twoStrings("a", "art"));
        System.out.println(twoStrings("be", "cat"));
        System.out.println(twoStrings("hello", "world"));
        System.out.println(twoStrings("hi", "world"));
        System.out.println();
        System.out.println(twoStringsEffective("a", "art"));
        System.out.println(twoStringsEffective("be", "cat"));
        System.out.println(twoStringsEffective("hello", "world"));
        System.out.println(twoStringsEffective("hi", "world"));
    }

    // Complete the twoStrings function below.
    static String twoStrings(String s1, String s2) {
        if (s1.length() < s2.length()) {
            for (int i = 0; i < s1.length(); i++) {
                if (s2.indexOf(s1.charAt(i)) != -1) {
                    return "YES";
                }
            }
        } else {
            for (int i = 0; i < s2.length(); i++) {
                if (s1.indexOf(s2.charAt(i)) != -1) {
                    return "YES";
                }
            }
        }
        return "NO";
    }

    static String twoStringsEffective(String s1, String s2) {
        int[] s1Chars = new int[26];
        int[] s2Chars = new int[26];
        int length = s1.length() > s2.length() ? s1.length() : s2.length();
        for (int i = 0; i < length; i++) {
            if (i < s1.length()) {
                int s1CharsIndex = s1.charAt(i) - 'a';
                if (s2Chars[s1CharsIndex] == 1) return "YES";
                if (s1Chars[s1CharsIndex] == 0) s1Chars[s1CharsIndex] = 1;
            }
            if (i < s2.length()) {
                int s2CharsIndex = s2.charAt(i) - 'a';
                if (s1Chars[s2CharsIndex] == 1) return "YES";
                if (s2Chars[s2CharsIndex] == 0) s2Chars[s2CharsIndex] = 1;
            }
        }
        return "NO";
    }
}
