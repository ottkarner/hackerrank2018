package _20181126_w004.javaproficiency;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class _022_JavaRegex2_DuplicateWords {

    public static void main(String[] args) {

//        String regex = "/* Write a RegEx matching repeated words here. */";
//        String regex = "\\b(\\w+)\\s+\\1\\b";
//        String regex = "\\b(\\w+)(?:\\W+\\1\\b)+";
        String regex = "\\b(\\w+)(\\s+\\1\\b)+";
//        String regex = "\\b([a-zA-Z]+)(\\s+\\1\\b)+";

//        Pattern p = Pattern.compile(regex, /* Insert the correct Pattern flag here.*/);
        Pattern p = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);

        String[] sentences = new String[]{
                "Goodbye bye bye world world world",
                "Sam went went to to to his business",
                "Reya is is the the best player in eye eye game",
                "in inthe",
                "Hello hello Ab aB 1"
        };

        for (String sentence : sentences) {
            Matcher m = p.matcher(sentence);
            // Check for subsequences of sentence that match the compiled pattern
            while (m.find()) {
//                sentence = sentence.replaceAll("/* The regex to replace */", "/* The replacement. */");
                sentence = sentence.replaceAll(m.group(), m.group(1));
//                sentence = sentence.replaceAll("(?i)" + regex, "$1");
            }

            // Prints the modified sentence.
            System.out.println(sentence);
        }

    }

}
