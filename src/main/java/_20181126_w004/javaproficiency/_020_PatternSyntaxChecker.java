package _20181126_w004.javaproficiency;

import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class _020_PatternSyntaxChecker {

    public static void main(String[] args) {
        String[] patterns = new String[]{
                "([A-Z])(.+)",
                "[AZ[a-z](a-z)",
                "batcatpat(nat"
        };

        for (String pattern : patterns) {
            //Write your code
            boolean isValid = true;
            try {
                Pattern.compile(pattern);
            } catch (PatternSyntaxException e) {
                isValid = false;
            }
            System.out.println(isValid ? "Valid" : "Invalid");
        }
    }

}
