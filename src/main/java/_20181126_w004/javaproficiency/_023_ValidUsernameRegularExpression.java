package _20181126_w004.javaproficiency;

class UsernameValidator {
    /*
     * Write regular expression here.
     */
    public static final String regularExpression = "[a-zA-Z](\\w){7,29}";
}

public class _023_ValidUsernameRegularExpression {

    public static void main(String[] args) {
        String[] userNames = new String[]{
                "8",
                "Julia",
                "Samantha",
                "Samantha_21",
                "1Samantha",
                "Samantha?10_2A",
                "JuliaZ007",
                "Julia@007",
                "_Julia007",
                "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaab"
        };

        for (String userName : userNames) {
            System.out.print("User name: " + userName + " (lenght=" + userName.length() + ")");
            if (userName.matches(UsernameValidator.regularExpression)) {
                System.out.println("Valid");
            } else {
                System.out.println("Invalid");
            }
        }
    }

/*
    private static final Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        int n = Integer.parseInt(scan.nextLine());
        while (n-- != 0) {
            String userName = scan.nextLine();

            if (userName.matches(UsernameValidator.regularExpression)) {
                System.out.println("Valid");
            } else {
                System.out.println("Invalid");
            }
        }
    }
*/

}
