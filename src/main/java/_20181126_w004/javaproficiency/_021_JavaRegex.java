package _20181126_w004.javaproficiency;

public class _021_JavaRegex {
    public static void main(String[] args) {
        String[] ipAddresses = new String[]{
                "000.12.12.034",
                "121.234.12.12",
                "23.45.12.56",
                "000.12.234.23.23",
                "666.666.23.23",
                ".213.123.23.32",
                "23.45.22.32.",
                "I.Am.not.an.ip",
                "00.12.123.123123.123",
                "122.23",
                "Hello.IP"
        };
        for (String IP : ipAddresses) {
            System.out.println(IP.matches(new MyRegex().pattern));
        }
    }
}

class MyRegex {
    public String pattern2 =
            "^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
    public String pattern = "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";
    public String pattern3 =
            "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
//    public String pattern = "(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})";
}