package _20181126_w004.tutorial30daysofcode;

public class Day11_2DArrays {

    public static void main(String[] args) {
//        int[][] arr = new int[6][6];
        int[][] arr = new int[][]{
                {1, 1, 1, 0, 0, 0},
                {0, 1, 0, 0, 0, 0},
                {1, 1, 1, 0, 0, 0},
                {0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0}
        };
        int[][] arr2 = new int[][]{
                {1, 1, 1, 0, 0, 0},
                {0, 1, 0, 0, 0, 0},
                {1, 1, 1, 0, 0, 0},
                {0, 0, 2, 4, 4, 9},
                {0, 0, 0, 2, 0, 9},
                {0, 0, 1, 2, 4, 9}
        };
        System.out.println(maxHourglassSum(arr));
        System.out.println(maxHourglassSum(arr2));
    }

    private static int maxHourglassSum(int[][] A) {
        int max = Integer.MIN_VALUE;
        if (isValidArray(A, 6, -9, 9)) {
            for (int i = 0; i < A.length - 2; i++) {
                for (int j = 0; j < A[i].length - 2; j++) {
                    int hourGlassSum =
                            A[i][j] + A[i][j + 1] + A[i][j + 2] +
                                    A[i + 1][j + 1] +
                                    A[i + 2][j] + A[i + 2][j + 1] + A[i + 2][j + 2];
                    if (max < hourGlassSum) {
                        max = hourGlassSum;
                    }
                }
            }
        }
        return max;
    }

    private static boolean isValidArray(int[][] array, int allowedSize, int allowedMinValue, int allowedMaxValue) {
        if (array.length != allowedSize || array[0].length != allowedSize || allowedSize < 3) {
            return false;
        }
        for (int[] subArray : array) {
            for (int value : subArray) {
                if (value < allowedMinValue || value > allowedMaxValue) {
                    return false;
                }
            }
        }
        return true;
    }

}
