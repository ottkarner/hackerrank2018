package _20181126_w004.tutorial30daysofcode;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Day10_BinaryNumbers {

    public static void main(String[] args) {
        int n = 13;
        if (n >= 1 && n <= Math.pow(10, 6)) {
            List<String> binaryNumbersOfOnes = Arrays.asList(Integer.toBinaryString(n).split("0"));
            int maxNumberOfConsecutiveOnes = binaryNumbersOfOnes.get(0).length();
            for (String binaryNumber : binaryNumbersOfOnes) {
//                System.out.println(binaryNumber + " - lenght=" + binaryNumber.length());
                if (maxNumberOfConsecutiveOnes < binaryNumber.length()) {
                    maxNumberOfConsecutiveOnes = binaryNumber.length();
                }
            }
            System.out.println(maxNumberOfConsecutiveOnes);
/*
            System.out.println(
                    Collections.frequency(Arrays.asList(Integer.toBinaryString(n).split("")), "1")
            );
*/
        }
    }

}
