package _20181126_w004.tutorial30daysofcode;

public class Day16_Exceptions_StringToInteger {

    public static void main(String[] args) {
        String s = "11T";
        if (s.length() >= 1 && s.length() <= 6 && s.matches("[a-z0-9]+")) {
            try {
                System.out.println(Integer.valueOf(s));
            } catch (Exception e) {
                System.out.println("Bad String");
            }
        }
    }
}
