package _20181126_w004.tutorial30daysofcode;

public class Day14_Scope {

    public static void main(String[] args) {
        Difference difference = new Difference(new int[]{1, 2, 5});
        difference.computeDifference();
        System.out.print(difference.maximumDifference);
    }

}

class Difference {
    private int[] elements;
    public int maximumDifference;

    // Add your code here
    Difference(int[] array) {
        elements = array;
    }

    public void computeDifference() {
        for (int i = 0; i < elements.length - 1; i++) {
            for (int j = i + 1; j < elements.length; j++) {
                if (maximumDifference < Math.abs(elements[i] - elements[j])) {
                    maximumDifference = Math.abs(elements[i] - elements[j]);
                }
            }
        }
    }

} // End of Difference class