package _20181126_w004.tutorial30daysofcode;

public class Day12_Inheritance {

    public static void main(String[] args) {
        String firstName = "Heraldo";
        String lastName = "Memelli";
        int id = 8135627;
        int[] testScores = new int[]{100, 80};

        Student s = new Student(firstName, lastName, id, testScores);
        s.printPerson();
        System.out.println("Grade: " + s.calculate());
    }

}

class Student extends Person {

    private int[] testScores;

    /*
     *   Class Constructor
     *
     *   @param firstName - A string denoting the Person's first name.
     *   @param lastName - A string denoting the Person's last name.
     *   @param id - An integer denoting the Person's ID number.
     *   @param scores - An array of integers denoting the Person's test scores.
     */
    // Write your constructor here

    Student() {
        super();
    }

    Student(String firstName, String lastName, int id, int[] scores) {
        super(firstName, lastName, id, scores);
    }

    /*
     *   Method Name: calculate
     *   @return A character denoting the grade.
     */
    // Write your method here
    char calculate() {
        int sum = 0;
        for (int score : scores) sum += score;
        double average = (double) (sum / scores.length);
        if (average >= 90 && average <= 100) return 'O';
        else if (average >= 80 && average < 90) return 'E';
        else if (average >= 70 && average < 80) return 'A';
        else if (average >= 55 && average < 70) return 'P';
        else if (average >= 40 && average < 55) return 'D';
        else return 'T';
    }

}

class Person {

    String firstName;
    String lastName;
    int id;
    int[] scores;

    Person() {
//        this("IllegalFst", "IllegalLst", -123456, new int[]{0});
    }

    Person(String firstName, String lastName, int id, int[] scores) {
        if (firstName.length() >= 1 && firstName.length() <= 10 &&
                lastName.length() >= 1 && lastName.length() <= 10 &&
                String.valueOf(id).length() == 7 &&
                isValidScores(scores)) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.id = id;
            this.scores = scores;
        }
    }

    private boolean isValidScores(int[] uncheckedScores) {
        for (int score : uncheckedScores) {
            if (score < 0 || score > 100) return false;
        }
        return true;
    }

    void printPerson() {
        System.out.println("Name: " + lastName + ", " + firstName);
        System.out.println("ID: id");
    }

}