package _20181126_w004.tutorial30daysofcode;

import java.util.LinkedList;

class Node {
    int data;
    Node next;

    Node(int d) {
        data = d;
        next = null;
    }
}

public class Day15_LinkedList {

    public static Node insert(Node head, int data) {
        if (head == null)
            return new Node(data);
        head.next = insert(head.next, data);
        return head;
/*
        //Complete this method

        // This will handle cases where the head node is empty.
        // Which is just an edge case.
        if (head == null) {
            return new Node(data);
        }

        // This is actual recursion.
        // We check if the node's next is empty
        // i.e. the node is the last node or the tail of the LL
        // and if so, add the new node as to it's next.
        // This is the base case for recurison in the else clause.
        if (head.next == null) {
            head.next = new Node(data);
        } else {
            insert(head.next, data);
        }

        return head;
*/
    }

    public static void display(Node head) {
        Node start = head;
        while (start != null) {
            System.out.print(start.data + " ");
            start = start.next;
        }
    }

    public static void main(String[] args) {
        Node head = null;
        int[] values = {2, 3, 4, 1};

        int N = values.length;

        for (int ele : values) {
            head = insert(head, ele);
        }

        display(head);
    }

}
