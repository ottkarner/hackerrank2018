package _20181126_w004;

public class _000019_DayOfTheProgrammer {

    // Complete the dayOfProgrammer function below.
    static String dayOfProgrammer(int year) {
        int daysInFeb;
        if (isLeapYear(year)) {
            daysInFeb = 29;
        } else {
            daysInFeb = 28;
        }
        int sumFirstEightMonths = 31 + daysInFeb + 31 + 30 + 31 + 30 + 31 + 31;
        int dayOfTheProgrammerInSep = 256 - sumFirstEightMonths;
        if (year == 1918) {
            dayOfTheProgrammerInSep += 13;
        }
        return dayOfTheProgrammerInSep + ".09." + year;
    }

    private static boolean isLeapYear(int year) {
        if (year <= 1917) {
            if (year % 4 == 0) return true;
        } else if (year >= 1919) {
            if (year % 400 == 0 || year % 4 == 0 && year % 100 != 0) return true;
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(dayOfProgrammer(1918));
        System.out.println(dayOfProgrammer(1984));
        System.out.println(dayOfProgrammer(2017));
        System.out.println(dayOfProgrammer(2018));
        System.out.println(dayOfProgrammer(1800));
    }

}
