package _20181126_w004;

public class _000022_DrawingBook {
    /*
     * Complete the pageCount function below.
     */
    static int pageCount(int n, int p) {
        /*
         * Write your code here.
         */
        int pagesFromStart, pagesFromEnd;
        if (p % 2 == 0) {
            pagesFromStart = p / 2;
            pagesFromEnd = (n % 2 == 0 ? (n - p) / 2 : ((n - 1) - p) / 2);
        } else {
            pagesFromStart = (p - 1) / 2;
            pagesFromEnd = (n % 2 == 0 ? (n - (p - 1)) / 2 : (n - p) / 2);
        }
        return pagesFromStart < pagesFromEnd ? pagesFromStart : pagesFromEnd;
    }

    public static void main(String[] args) {
        int n = 5;
        int p = 4;
/*
        int n = 6;
        int p = 2;
*/
        System.out.println(pageCount(n, p));
    }

/*
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])*");

        int p = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])*");

        int result = pageCount(n, p);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
*/
}
