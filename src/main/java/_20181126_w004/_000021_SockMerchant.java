package _20181126_w004;

import java.util.ArrayList;
import java.util.List;

public class _000021_SockMerchant {
/*
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] ar = new int[n];

        String[] arItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int arItem = Integer.parseInt(arItems[i]);
            ar[i] = arItem;
        }

        int result = sockMerchant(n, ar);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
*/

    public static void main(String[] args) {
        int[] ar = new int[]{10, 20, 20, 10, 10, 30, 50, 10, 20};
        System.out.println(sockMerchant(ar.length, ar));
    }

    // Complete the sockMerchant function below.
    static int sockMerchant(int n, int[] socks) {
        int sockPairs = 0;
        if (socks.length >= 1 && socks.length <= 100) {
            List<Integer> appearedSocks = new ArrayList<>();
            for (int sock : socks) {
                if (sock >= 1 && sock <= 100) {
                    if (!appearedSocks.contains(sock)) {
                        appearedSocks.add(sock);
                        if (countValueInArray(sock, socks) % 2 == 0) {
                            sockPairs += countValueInArray(sock, socks) / 2;
                        } else {
                            sockPairs += (countValueInArray(sock, socks) - 1) / 2;
                        }
                    }
                }
            }
        }
        return sockPairs;
    }

    private static int countValueInArray(int searchValue, int[] array) {
        int count = 0;
        for (int value : array) {
            if (value == searchValue) {
                count++;
            }
        }
        return count;
    }

}

