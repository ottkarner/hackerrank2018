package _20181126_w004;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class _000020_BonAppetit {

    public static void main(String[] args) throws IOException {
/*
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String[] nk = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");
        int n = Integer.parseInt(nk[0]);
        int k = Integer.parseInt(nk[1]);
        List<Integer> bill = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                .map(Integer::parseInt)
                .collect(toList());
        int b = Integer.parseInt(bufferedReader.readLine().trim());
*/
        int k = 1;
        List<Integer> bill = Arrays.asList(new Integer[]{3, 10, 2, 9});
        int b = 12;

        bonAppetit(bill, k, b);
    }

    // Complete the bonAppetit function below.
    static void bonAppetit(List<Integer> bill, int k, int bCharged) {
        int billSum = 0;
        for (int item : bill) {
            billSum += item;
        }
        int billSumShared = billSum - bill.get(k);
        int bActual = billSumShared / 2;
        if (bCharged == bActual) {
            System.out.println("Bon Appetit");
        } else {
            System.out.println(bCharged - bActual);
        }
    }

}
