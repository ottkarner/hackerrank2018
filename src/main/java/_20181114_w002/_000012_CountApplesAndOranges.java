package _20181114_w002;

import java.util.*;

public class _000012_CountApplesAndOranges {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
//        String[] st = scanner.nextLine().split(" ");
//        int s = Integer.parseInt(st[0]);
//        int t = Integer.parseInt(st[1]);
//
//        String[] ab = scanner.nextLine().split(" ");
//        int a = Integer.parseInt(ab[0]);
//        int b = Integer.parseInt(ab[1]);
//
//        String[] mn = scanner.nextLine().split(" ");
//        int m = Integer.parseInt(mn[0]);
//        int n = Integer.parseInt(mn[1]);
//
//        int[] apples = new int[m];
//
//        String[] applesItems = scanner.nextLine().split(" ");
//        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
//
//        for (int i = 0; i < m; i++) {
//            int applesItem = Integer.parseInt(applesItems[i]);
//            apples[i] = applesItem;
//        }
//
//        int[] oranges = new int[n];
//
//        String[] orangesItems = scanner.nextLine().split(" ");
//        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
//
//        for (int i = 0; i < n; i++) {
//            int orangesItem = Integer.parseInt(orangesItems[i]);
//            oranges[i] = orangesItem;
//        }

        countApplesAndOranges(7, 11, 5, 15, new int[]{-2, 2, 1}, new int[]{5, -6});

//        scanner.close();
    }

    // Complete the countApplesAndOranges function below.
    static void countApplesAndOranges(int s, int t, int a, int b, int[] apples, int[] oranges) {
        boolean calculate = true;
        int max = (int) Math.pow(10, 5);
        if (!(a < s && s < t && t < b)) {
            calculate = false;
            return;
        }
        if (!isInRange(s, 1, max) || !isInRange(t, 1, max)) {
            calculate = false;
            return;
        }
        if (!isInRange(a, 1, max) || !isInRange(b, 1, max)) {
            calculate = false;
            return;
        }
        if (!isInRange(apples.length, 1, max) || !isInRange(oranges.length, 1, max)) {
            calculate = false;
            return;
        }
        if (!isInRange(apples, -max, max) || !isInRange(oranges, -max, max)) {
            calculate = false;
            return;
        }

        if (calculate) {
            System.out.print(countFruitsFallOnSamsHouse(a, apples, s, t));
            System.out.print(" ");
            System.out.print(countFruitsFallOnSamsHouse(b, oranges, s, t));
        }

    }

    private static int countFruitsFallOnSamsHouse(int treeLoc, int[] fruitsD, int houseLocStartPoint, int houseLocEndPoint) {
        int count = 0;
        for (int d : fruitsD) {
            if (isInRange(treeLoc + d, houseLocStartPoint, houseLocEndPoint)) {
                count++;
            }
        }
        return count;
    }

    private static boolean isInRange(int val, int min, int max) {
        if (val >= min && val <= max) {
            return true;
        }
        return false;
    }

    private static boolean isInRange(int[] arr, int min, int max) {
        for (int d : arr) {
            if (d >= min && d <= max) {
                return true; // see peaks vale olema!
            }
        }
        return false;
    }

}
