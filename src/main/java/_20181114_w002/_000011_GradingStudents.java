package _20181114_w002;

public class _000011_GradingStudents {
    public static void main(String[] args) {
        int arr[] = gradingStudents(new int[]{73, 67, 38, 33});
        for (int a : arr) {
            System.out.println(a);
        }

    }

    private static int[] gradingStudents(int[] grades) {
        boolean calculate = true;
        if (grades.length < 1 && grades.length > 60) {
            calculate = false;
        }
        for (int grade : grades) {
            if (grade < 0 || grade > 100) {
                calculate = false;
            }
        }

        if (calculate) {
            for (int i = 0; i < grades.length; i++) {
                if (grades[i] >= 38 && grades[i] % 5 >= 3) {
                    grades[i] += 5 - grades[i] % 5;
                }
            }
        }

        return grades;
    }
}
