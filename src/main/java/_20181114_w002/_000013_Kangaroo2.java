package _20181114_w002;

public class _000013_Kangaroo2 {
    public static void main(String[] args) {
//        String result = kangaroo(2, 2, 2, 2); // "", x1==x2
//        String result = kangaroo(2, 1, 2, 2); // "", x1==x1

//        String result = kangaroo(1, 2, 2, 2); // NO, v1==v2

//        String result = kangaroo(0, 2, 5, 3); // NO, x1<x2 && v1<v2

//        String result = kangaroo(2, 1, 1, 2); // YES, 1 jump
//        String result = kangaroo(0, 3, 4, 2); // YES, 4 jumps
//        String result = kangaroo(5, 5, 3, 6); // YES, 4 jumps
//        String result = kangaroo(13, 4, 3, 5); // YES, 10 jumps
//        String result = kangaroo(17, 7, 3, 9); // YES, 10 jumps
//        String result = kangaroo(4523, 8092, 9419, 8076); // YES, 206 jumps

//        String result = kangaroo(5, 5, 3, 8); // NO, 0 jumps
        String result = kangaroo(50, 2, 0, 8); // NO, 8 jumps

        System.out.println(result);
    }

    static String kangaroo(int x1, int v1, int x2, int v2) {
        if (x1 == x2) {
            return "";
        }

        int max = 1000;
        if (x1 < 0 || x2 < 0 || v1 < 1 || v2 < 1 || x1 > max || x2 > max || v1 > max || v2 > max) {
            return "";
        }

        if (x1 < x2 && v1 < v2 || x1 > x2 && v1 > v2 || v1 == v2) {
            return "NO";
        } else {
            int j1 = x1 + v1;
            int j2 = x2 + v2;
            while (x1 < x2 && j1 <= j2 || x1 > x2 && j1 >= j2) {
                if (j1 == j2) {
                    return "YES";
                }
                j1 += v1;
                j2 += v2;
            }
        }
        return "NO";

    }

}
