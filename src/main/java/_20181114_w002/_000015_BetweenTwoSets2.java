package _20181114_w002;

import java.util.Arrays;

public class _000015_BetweenTwoSets2 {

    public static void main(String[] args) {
        System.out.println(getTotalX(new int[]{2, 6}, new int[]{21, 31}));
        System.out.println(getTotalX(new int[]{2, 6}, new int[]{24, 36}) + " - should be 2 (6 and 12):");
        System.out.println("    6%2=0, 6%6=0, 24%6=0, 36%12=0");
        System.out.println("    12%2=0, 12%6=0, 24%12=0, 36%12=0");
        System.out.println(getTotalX(new int[]{2, 4}, new int[]{16, 32, 96}) + " - should be 3 (4, 8 and 16):");
        System.out.println("    4%2=0, 4%4=0, 16%4=0, 32%4=0, 96%4=0");
        System.out.println("    8%2=0, 8%4=0, 16%8=0, 32%8=0, 96%8=0");
        System.out.println("    16%2=0, 16%4=0, 16%16=0, 32%16=0, 96%16=0");
    }

    static int getTotalX(int[] a, int[] b) {
        /*
         * Write your code here.
         */
        Arrays.sort(a);
        Arrays.sort(b);

        int maxA = a[a.length - 1];
        int minB = b[0];
        if (maxA > minB) {
            return 0;
        }

        int maxB = b[b.length - 1];
        int count = 0;
        int potentialInteger = 0;
        start:
        while (potentialInteger <= maxB) {
            potentialInteger += maxA;
            for (int element : a) {
                if (potentialInteger % element > 0) {
                    continue start;
                }
            }
            for (int element : b) {
                if (element % potentialInteger > 0) {
                    continue start;
                }
            }
            count++;
        }
        return count;
    }

}
