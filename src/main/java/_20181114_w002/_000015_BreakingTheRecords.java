package _20181114_w002;

public class _000015_BreakingTheRecords {

    public static void main(String[] args) {
        printArray(breakingRecords(new int[]{12, 24, 10, 24})); // {1, 1}
        printArray(breakingRecords(new int[]{10, 5, 20, 20, 4, 5, 2, 25, 1})); // {2, 4}
        printArray(breakingRecords(new int[]{3, 4, 21, 36, 10, 28, 35, 5, 24, 42})); // {4, 0}
    }

    private static void printArray(int[] array) {
        for (int value : array) {
            System.out.print(value + " ");
        }
        System.out.println();
    }

    static int[] breakingRecords(int[] scores) {
        if (scores.length < 1 || scores.length > 1000) {
            return null;
        }

        int minScore = scores[0];
        int maxScore = scores[0];
        int countMinScoreBreak = 0;
        int countMaxScoreBreak = 0;
        for (int score : scores) {
            if (score < 0 || score > Math.pow(10, 8)) {
                return null;
            }
            if (score < minScore) {
                minScore = score;
                countMinScoreBreak++;
            }
            if (score > maxScore) {
                maxScore = score;
                countMaxScoreBreak++;
            }
        }

        return new int[]{countMaxScoreBreak, countMinScoreBreak};
    }

}
