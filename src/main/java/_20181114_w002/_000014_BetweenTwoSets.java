package _20181114_w002;

import java.util.Arrays;

public class _000014_BetweenTwoSets {
    public static void main(String[] args) {
        System.out.println(getTotalX(new int[]{2, 6}, new int[]{24, 36}) + " - should be 2 (6 and 12):");
        System.out.println("    6%2=0, 6%6=0, 24%6=0, 36%12=0");
        System.out.println("    12%2=0, 12%6=0, 24%12=0, 36%12=0");
        System.out.println(getTotalX(new int[]{2, 4}, new int[]{16, 32, 96}) + " - should be 3 (4, 8 and 16):");
        System.out.println("    4%2=0, 4%4=0, 16%4=0, 32%4=0, 96%4=0");
        System.out.println("    8%2=0, 8%4=0, 16%8=0, 32%8=0, 96%8=0");
        System.out.println("    16%2=0, 16%4=0, 16%16=0, 32%16=0, 96%16=0");
    }

    static int getTotalX(int[] a, int[] b) {
        /*
         * Write your code here.
         */
        Arrays.sort(a);
        Arrays.sort(b);

        int count = 0;
        int maxOfA = a[a.length - 1];
        int minOfB = b[0];
        int maxOfB = b[b.length - 1];
        if (maxOfA > minOfB) {
            return 0;
        } else {
            int potentialInteger = maxOfA;
            while (potentialInteger <= maxOfB) {
                boolean doCount = true;
                for (int i = 0; i < a.length; i++) {
                    if (potentialInteger % a[i] > 0) {
                        doCount = false;
                        break;
                    }
                }
                if (doCount) {
                    for (int i = 0; i < b.length; i++) {
                        if (b[i] % potentialInteger > 0) {
                            doCount = false;
                            break;
                        }
                    }
                }
                if (doCount) {
                    count++;
                }
                potentialInteger += maxOfA;
            }
        }
        return count;

    }
}
