package _20181114_w002;

public class _000012_CountApplesAndOranges2 {
    public static void main(String[] args) {
        countApplesAndOranges(7, 11, 5, 15, new int[]{-2, 2, 1}, new int[]{5, -6});
    }

    static void countApplesAndOranges(int s, int t, int a, int b, int[] apples, int[] oranges) {

        boolean calculate = true;

        // third constraint check
        if (!(a < s && s < t && t < b)) {
            calculate = false;
            return;
        }

        int max = (int) Math.pow(10, 5);

        // first constraint check
        if (!isInRange(s, 1, max)) {
            calculate = false;
            return;
        }
        if (!isInRange(t, 1, max)) {
            calculate = false;
            return;
        }
        if (!isInRange(a, 1, max)) {
            calculate = false;
            return;
        }
        if (!isInRange(b, 1, max)) {
            calculate = false;
            return;
        }
        if (!isInRange(apples.length, 1, max)) {
            calculate = false;
            return;
        }
        if (!isInRange(oranges.length, 1, max)) {
            calculate = false;
            return;
        }

        // second constraint check
        if (!isInRange(apples, -max, max)) {
            calculate = false;
            return;
        }
        if (!isInRange(oranges, -max, max)) {
            calculate = false;
            return;
        }

        if (calculate) {
            System.out.println(countFruits(a, apples, s, t));
            System.out.println(countFruits(b, oranges, s, t));
        }

    }

    static int countFruits(int treeLoc, int[] fruitsD, int houseLocStart, int houseLocEnd) {
        int count = 0;
        for (int d : fruitsD) {
            if (isInRange(treeLoc + d, houseLocStart, houseLocEnd)) {
                count++;
            }
        }
        return count;
    }

    static boolean isInRange(int val, int min, int max) {
        if (val >= min && val <= max) {
            return true;
        }
        return false;
    }

    static boolean isInRange(int[] arr, int min, int max) {
        for (int val : arr) {
            if (!isInRange(val, min, max)) {
                return false;
            }
        }
        return true;
    }

}
