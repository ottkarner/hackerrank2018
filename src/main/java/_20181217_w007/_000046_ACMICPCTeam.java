package _20181217_w007;

import java.util.Arrays;

public class _000046_ACMICPCTeam {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(acmTeam(new String[]{"10101", "11110", "00010"})));
        System.out.println(Arrays.toString(acmTeam(new String[]{"10101", "11100", "11010", "00101"})));
    }

    // Complete the acmTeam function below.
    static int[] acmTeam(String[] topic) {
        int max = 0, countMax = 0;
        for (int i = 0; i < topic.length - 1; i++) {
            for (int j = i + 1; j < topic.length; j++) {
                int count = 0;
                for (int k = 0; k < topic[i].length() && k < topic[j].length(); k++) {
                    if (topic[i].charAt(k) == '1' || topic[j].charAt(k) == '1') {
                        count++;
                    }
                }
                System.out.print("(" + i + "," + j + ") -> (" + topic[i] + "," + topic[j] + ")=" + count + "; ");
                if (max < count) {
                    max = count;
                    countMax = 0;
                }
                if (max == count) {
                    countMax++;
                }
            }
        }
        return new int[]{countMax, max};
    }
}
