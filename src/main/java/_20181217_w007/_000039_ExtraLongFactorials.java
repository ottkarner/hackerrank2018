package _20181217_w007;

import java.math.BigInteger;

public class _000039_ExtraLongFactorials {

    public static void main(String[] args) {
        extraLongFactorials(25);
        extraLongFactorials(30);
    }

    // Complete the extraLongFactorials function below.
    static void extraLongFactorials(int n) {
        BigInteger factorialBig = BigInteger.valueOf(1);
        for (int i = 1; i <= n; i++) {
            factorialBig = factorialBig.multiply(BigInteger.valueOf(i));
        }
        System.out.println(factorialBig);
    }

}
