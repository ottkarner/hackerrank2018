package _20181217_w007;

public class _000040_SherlockAndSquares {

    public static void main(String[] args) {
        System.out.println(squares(24, 49));
        System.out.println(squares(3, 9));
        System.out.println(squares(17, 24));

    }

    // Complete the squares function below.
    static int squares(int a, int b) {
        return (int) (Math.floor(Math.sqrt(b)) - Math.ceil(Math.sqrt(a)) + 1);
    }

/* Fails because of the timeout :(
    static int squares(int a, int b) {
        int countSquares = 0;
        for (int i = a; i <= b; i++) {
            int sqrtI = (int) Math.sqrt(i);
            if (sqrtI * sqrtI == i) {
                countSquares++;
            }
        }
        return countSquares;
    }
*/

}
