package _20181217_w007;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class _000045_EqualizeTheArray {

    public static void main(String[] args) {
        System.out.println(equalizeArray(new int[]{1}));
        System.out.println(equalizeArray(new int[]{1, 1, 1}));
        System.out.println(equalizeArray(new int[]{1, 2, 2, 3}));
        System.out.println(equalizeArray(new int[]{3, 3, 2, 1, 3}));
        System.out.println(equalizeArray2(new int[]{1}));
        System.out.println(equalizeArray2(new int[]{1, 1, 1}));
        System.out.println(equalizeArray2(new int[]{1, 2, 2, 3}));
        System.out.println(equalizeArray2(new int[]{3, 3, 2, 1, 3}));
    }

    // Complete the equalizeArray function below.
    static int equalizeArray(int[] arr) {
        Arrays.sort(arr);
        int count = 1, countsMax = 1;
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] != arr[i + 1]) {
                if (countsMax < count) {
                    countsMax = count;
                }
                count = 1;
            } else {
                count++;
            }
        }
        if (countsMax < count) {
            countsMax = count;
        }
        return arr.length - countsMax;
    }

    static int equalizeArray2(int[] arr) {
        int max = 1;
        Map<Integer, Integer> nums = new HashMap<>();
        for (int i : arr)
            if (!nums.containsKey(i))
                nums.put(i, 1);
            else {
                nums.put(i, nums.get(i) + 1);
                if (max < nums.get(i))
                    max = nums.get(i);
            }
        return arr.length - max;
    }

/*  // Doesn't work as expected
    static int equalizeArray3(int[] arr) {
        return arr.length - Arrays.stream(arr).max().getAsInt();
    }
*/

}