package _20181217_w007;

import java.util.*;

public class _000042_CutTheSticks {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(cutTheSticks(new int[]{})));
        System.out.println(cutTheSticks(new ArrayList<>()));
//        System.out.println(cutTheSticks(Arrays.asList())); // The list will be immutable

        System.out.println(Arrays.toString(cutTheSticks(new int[]{1, 2, 3})));
        System.out.println(cutTheSticks(new ArrayList<Integer>() {
            {
                add(1);
                add(2);
                add(3);
            }
        }));
//        System.out.println(cutTheSticks(Arrays.asList(1, 2, 3))); // The list will be immutable

        System.out.println(Arrays.toString(cutTheSticks(new int[]{5, 4, 4, 2, 2, 8})));
        System.out.println(cutTheSticks(new ArrayList<Integer>() {
            {
                add(5);
                add(4);
                add(4);
                add(2);
                add(2);
                add(8);
            }
        }));

        System.out.println(Arrays.toString(cutTheSticks(new int[]{1, 2, 3, 4, 3, 3, 2, 1})));
        System.out.println(cutTheSticks(new ArrayList<Integer>() {
            {
                add(1);
                add(2);
                add(3);
                add(4);
                add(3);
                add(3);
                add(2);
                add(1);
            }
        }));

//        System.out.println(Arrays.toString(cutTheSticksEffective(new int[]{})));
//        System.out.println(Arrays.toString(cutTheSticksEffective(new int[]{1, 2, 3})));
//        System.out.println(Arrays.toString(cutTheSticksEffective(new int[]{5, 4, 4, 2, 2, 8})));
//        System.out.println(Arrays.toString(cutTheSticksEffective(new int[]{1, 2, 3, 4, 3, 3, 2, 1})));
    }

    // Complete the cutTheSticks function below.
    private static int[] cutTheSticks(int[] sticks) {
        int[] sticksCuts = {sticks.length};
        int[] temp = {};

        while (sticks.length > 0) {

            // Find min length of sticks and the number of sticks with such length
            int minStick = sticks[0];
            int countMinSticks = 0;
            for (int i = 0; i < sticks.length; i++) {
                if (minStick > sticks[i]) {
                    minStick = sticks[i];
                    countMinSticks = 0;
                }
                if (minStick == sticks[i]) {
                    countMinSticks++;
                }
            }

            // Create a new sticks array with the sticks left after cut
            temp = new int[sticks.length - countMinSticks];
            int next = 0;
            for (int i = 0; i < temp.length; i++) {
                for (int j = next; j < sticks.length; j++) {
                    if (sticks[j] > minStick) {
                        temp[i] = sticks[j];
                        next = j + 1;
                        break;
                    }

                }
            }
            sticks = temp;

            // Add a new positive count of cuts
            int sticksCut = sticksCuts[sticksCuts.length - 1] - countMinSticks;
            if (sticksCut > 0) {
                sticksCuts = arrayAdd(sticksCuts, sticksCut);
            }

        }

        return sticksCuts;
    }

    private static List<Integer> cutTheSticks(List<Integer> sticks) {
//        List<Integer> sticksCuts = Arrays.asList(sticks.size()); // The list will be immutable
        List<Integer> sticksCuts = new ArrayList<Integer>();
        sticksCuts.add(sticks.size());

        while (sticks.size() > 0) {

            // Find min length of sticks and the number of sticks with such length
            int minStick = Collections.min(sticks);
            int countMinSticks = Collections.frequency(sticks, minStick);

            // Create a new sticks array with the sticks left after cut
            Iterator<Integer> iterator = sticks.iterator();
            while (iterator.hasNext()) {
                if (iterator.next().equals(minStick)) {
                    iterator.remove();
                }
            }

            // Add a new positive count of cuts
            Integer stickCut = sticksCuts.get(sticksCuts.size() - 1) - countMinSticks;
            if (stickCut > 0) {
                sticksCuts.add(stickCut);
            }

        }

        return sticksCuts;
    }

    private static int[] cutTheSticksEffective(int[] sticks) {
        Arrays.sort(sticks);
        int[] sticksCuts = new int[]{sticks.length};
        for (int i = 0; i < sticks.length - 1; i++) {
            if (sticks[i] != sticks[i + 1]) {
                sticksCuts = arrayAdd(sticksCuts, sticks.length - (i + 1));
            }
        }
        return sticksCuts;
    }

    private static int[] arrayAdd(int[] array, int addition) {
        int[] temp = new int[array.length + 1];
        for (int i = 0; i < array.length; i++) {
            temp[i] = array[i];
        }
        temp[temp.length - 1] = addition;
        array = temp;
        return array;
    }

}