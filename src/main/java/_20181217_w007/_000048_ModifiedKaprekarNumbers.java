package _20181217_w007;

public class _000048_ModifiedKaprekarNumbers {
    public static void main(String[] args) {
        kaprekarNumbers(1000, 2000);
        kaprekarNumbers(4000, 6000);
        originalKaprekarNumbers(4000, 6000);
        kaprekarNumbers(1, 100000);
        originalKaprekarNumbers(1, 100000);
        originalKaprekarNumbersBruteForce(1, 100000);
    }

    // Complete the kaprekarNumbers function below.
    private static void kaprekarNumbers(int p, int q) {
        int count = 0;
        for (long i = p; i <= q; i++) {
            int d = String.valueOf(i).length();
            String iSquare = String.valueOf((i * i));
            int l = 0;
            if (iSquare.length() > 1) {
                l = Integer.valueOf(iSquare.substring(0, iSquare.length() - d));
            }
            int r = Integer.valueOf(iSquare.substring(iSquare.length() - d));
            if ((i == l + r) && (r > 0)) {
                count++;
                System.out.print(i + " ");
            }
//            System.out.println(i + "^2=" + iSquare + " -> " + l + "+" + r + "=" + (l + r));
        }
        if (count == 0) {
            System.out.println("INVALID RANGE");
        } else {
            System.out.println("- Modified Kaprekar numbers in the range [" + p + "," + q + "]: " + count);
        }
    }

    private static void originalKaprekarNumbers(int p, int q) {
        int count = 0;
        for (long i = p; i <= q; i++) {
            String iSquare = String.valueOf(i * i);
            if (i == 1) {
                count++;
                System.out.print(i + " ");
            }
            if (iSquare.length() > 1) {
                for (int j = 1; j < iSquare.length(); j++) {
                    long l = Long.valueOf(iSquare.substring(0, j));
                    long r = Long.valueOf(iSquare.substring(j));
                    if (l < i && r < i && l + r == i && r > 0) {
                        count++;
                        System.out.print(i + " ");
                    }
//                    System.out.println(i + "^2=" + iSquare + " -> " + l + "+" + r + "=" + (l + r));
                }
            }
        }
        if (count == 0) {
            System.out.println("INVALID RANGE");
        } else {
            System.out.println("- Original Kaprekar numbers in the range [" + p + "," + q + "]: " + count);
        }
    }

    private static void originalKaprekarNumbersBruteForce(int p, int q) {
        int count = 0;
        for (long i = p; i <= q; i++) {
            long iSquare = i * i;
            String iSquareStr = String.valueOf(iSquare);
            long l = 0, r = iSquare;
            if (l + r == i) {
                count++;
                System.out.print(i + " ");
            }
//            System.out.println(i + "^2=" + iSquare + " -> " + l + "+" + r + "=" + (l + r));
            if (iSquareStr.length() > 1) {
                for (int j = 1; j < iSquareStr.length(); j++) {
                    l = Long.valueOf(iSquareStr.substring(0, j));
                    r = Long.valueOf(iSquareStr.substring(j));
                    if (l < i && r < i && l + r == i && r > 0) {
                        count++;
                        System.out.print(i + " ");
                    }
//                    System.out.println(i + "^2=" + iSquare + " -> " + l + "+" + r + "=" + (l + r));
                }
            }
        }
        if (count == 0) {
            System.out.println("INVALID RANGE");
        } else {
            System.out.println("- Original Kaprekar numbers in the range [" + p + "," + q + "]: " + count);
        }
    }

}
