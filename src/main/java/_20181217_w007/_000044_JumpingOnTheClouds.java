package _20181217_w007;

public class _000044_JumpingOnTheClouds {

    public static void main(String[] args) {
        System.out.println(jumpingOnClouds(new int[]{0, 1, 0, 0, 0, 1, 0}));
        System.out.println(jumpingOnClouds2(new int[]{0, 1, 0, 0, 0, 1, 0}));
        System.out.println(jumpingOnClouds3(new int[]{0, 1, 0, 0, 0, 1, 0}));
        System.out.println(jumpingOnClouds4(new int[]{0, 1, 0, 0, 0, 1, 0}));
        System.out.println(jumpingOnClouds(new int[]{0, 0, 1, 0, 0, 1, 0}));
        System.out.println(jumpingOnClouds2(new int[]{0, 0, 1, 0, 0, 1, 0}));
        System.out.println(jumpingOnClouds3(new int[]{0, 0, 1, 0, 0, 1, 0}));
        System.out.println(jumpingOnClouds4(new int[]{0, 0, 1, 0, 0, 1, 0}));
        System.out.println(jumpingOnClouds(new int[]{0, 0, 0, 0, 1, 0}));
        System.out.println(jumpingOnClouds2(new int[]{0, 0, 0, 0, 1, 0}));
        System.out.println(jumpingOnClouds3(new int[]{0, 0, 0, 0, 1, 0}));
        System.out.println(jumpingOnClouds4(new int[]{0, 0, 0, 0, 1, 0}));
        System.out.println(jumpingOnClouds(new int[]{0, 0}));
        System.out.println(jumpingOnClouds2(new int[]{0, 0}));
        System.out.println(jumpingOnClouds3(new int[]{0, 0}));
        System.out.println(jumpingOnClouds4(new int[]{0, 0}));
    }

    // Complete the jumpingOnClouds function below.
    static int jumpingOnClouds(int[] c) {
        int jumpsCount = 0;
        int i = 0;
        while (i < c.length - 1) {
            if (c.length > 2 && i < c.length - 2) {
                if (c[i + 2] == 0) {
                    jumpsCount++;
                    i += 2;
                    continue;
                }
            }
            if (c[i + 1] == 0) {
                jumpsCount++;
                i++;
            }
        }
        return jumpsCount;
    }

    static int jumpingOnClouds2(int[] c) {
        int jumpsCount = 0;
        int i = 0;
        while (i < c.length - 1) {
            if (i + 2 < c.length && c[i + 2] == 0) {
                i += 2;
            } else {
                if (c[i + 1] == 0) {
                    i++;
                }
            }
            jumpsCount++;
        }
        return jumpsCount;
    }

    static int jumpingOnClouds3(int[] c) {
        int jumps = 0;
        for (int i = 0; i < c.length - 1; i++) {
            jumps++;
            if (i + 2 < c.length && c[i + 2] == 0) {
                i++;
            }
        }
        return jumps;
    }

    static int jumpingOnClouds4(int[] c) {
        int jumps = 0;
        for (int i = 0; i < c.length - 1; jumps++) {
            i += (i + 2 < c.length && c[i + 2] == 0) ? 2 : 1;
        }
        return jumps;
    }

}
