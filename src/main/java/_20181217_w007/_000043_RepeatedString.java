package _20181217_w007;

public class _000043_RepeatedString {

    public static void main(String[] args) {
        System.out.println(repeatedString("abcac", 10));
        System.out.println(repeatedString2("abcac", 10));
        System.out.println(repeatedString("aba", 10));
        System.out.println(repeatedString2("aba", 10));
        System.out.println(repeatedString("a", 1000000000000L));
        System.out.println(repeatedString2("a", 1000000000000L));
        System.out.println(repeatedString("aba", 2));
        System.out.println(repeatedString2("aba", 2));
    }

    // Complete the repeatedString function below.
    static long repeatedString(String s, long n) {
        long countA = 0;
        int length = s.length();
        if (n > length) {
            long reminder = n % length;
            long numberOfFits = (n - reminder) / length;
            countA = countCharInString(s, 'a');
            countA *= numberOfFits;
            countA += countCharInString(s.substring(0, (int) reminder), 'a');
        } else {
            countA = countCharInString(s.substring(0, (int) n), 'a');
        }
        return countA;
    }

    private static long countCharInString(String s, char a) {
        long count = 0;
        for (char c : s.toCharArray()) {
            if (c == a) {
                count++;
            }
        }
        return count;
    }

    static long repeatedString2(String s, long n) {
        long countA = 0;
        long countRemainingA = 0;
        long reminder = n % s.length();
        for (int i = s.length(); i-- > 0; ) {
            if (s.charAt(i) == 'a') {
                ++countA;
                if (i < reminder) {
                    ++countRemainingA;
                }
            }
        }
        return ((n - reminder) / s.length() * countA) + countRemainingA;
    }

}
