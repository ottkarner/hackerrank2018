package _20181217_w007;

public class _000037_FindDigits {

    public static void main(String[] args) {
        int[] t = {111, 12, 1012};
        for (int n : t) {
            System.out.println(findDigits3(n));
        }
    }

    // Complete the findDigits function below.
    static int findDigits(int n) {
        char[] cDigits = String.valueOf(n).toCharArray();
        int countDivisors = 0;
        for (char cDigit : cDigits) {
            int digit = Integer.valueOf(String.valueOf(cDigit));
            if (digit != 0 && n % digit == 0) {
                countDivisors++;
            }
        }
        return countDivisors;
    }

    static int findDigits2(int n) {
        int r = n;
        int count = 0;
        while (r > 0) {
            if (r % 10 != 0 && n % (r % 10) == 0) count++;
            r /= 10;
        }
        return count;
    }

    static int findDigits3(int n) {
        int counter = 0;
        int comparator = n;
        while (comparator != 0) {
            int divisor = comparator % 10;
            if (divisor != 0) {
                if (n % divisor == 0) {
                    counter++;
                }
            }
            comparator /= 10;
        }
        return counter;
    }

}