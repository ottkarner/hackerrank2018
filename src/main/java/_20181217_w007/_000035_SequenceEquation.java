package _20181217_w007;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class _000035_SequenceEquation {

//    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

/*
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));
        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
        int[] p = new int[n];
        String[] pItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
        for (int i = 0; i < n; i++) {
            int pItem = Integer.parseInt(pItems[i]);
            p[i] = pItem;
        }
        int[] result = permutationEquation(p);
        for (int i = 0; i < result.length; i++) {
            bufferedWriter.write(String.valueOf(result[i]));
            if (i != result.length - 1) {
                bufferedWriter.write("\n");
            }
        }
        bufferedWriter.newLine();
        bufferedWriter.close();
        scanner.close();
*/

//        int[] p = {5, 2, 1, 3, 4};
        int[] p = {4, 3, 5, 1, 2};
        for (int i : permutationEquation(p)) {
            System.out.println(i);
        }
    }

    // Complete the permutationEquation function below.
    static int[] permutationEquation(int[] p) {
        int[] pp = new int[p.length];
        for (int i = 0; i < p.length; i++) {
            for (int j = 0; j < p.length; j++) {
                if (i + 1 == p[j]) {
                    for (int k = 0; k < p.length; k++) {
                        if (j + 1 == p[k]) {
                            pp[i] = k + 1;
                        }
                    }
                }
            }
        }
        return pp;
    }

    static int[] permutationEquation2(int[] p) {
        int[] pp = new int[p.length];
        for (int i = 0; i < p.length; i++) {
            for (int j = 0; j < p.length; j++) {
                if (i + 1 == p[p[j] - 1]) {
                    pp[i] = j + 1;
                }
            }
        }
        return pp;
    }

    static int[] permutationEquation3(int[] p) {
        int[] pp = new int[p.length];
        for (int x : p) {
            pp[p[p[x - 1] - 1] - 1] = x;
        }
        return pp;
    }

}