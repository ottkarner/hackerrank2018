package _20181217_w007;

public class _000036_JumpingOnTheCloudsRevisited {

    public static void main(String[] args) {
        System.out.println(jumpingOnClouds(new int[]{0, 0, 1, 0}, 2));
        System.out.println(jumpingOnClouds(new int[]{0, 0, 1, 0, 0, 1, 1, 0}, 2));
        System.out.println(jumpingOnClouds(new int[]{1, 1, 1, 0, 1, 1, 0, 0, 0, 0}, 3));
    }

    // Complete the jumpingOnClouds function below.
    static int jumpingOnClouds(int[] c, int k) {
        int e = 100;
        int i = 0;
        while (i < c.length) {
            e--;
            if (c[i] == 1) {
                e -= 2;
            }
            i = (i + k) % c.length;
            if (i == 0 || e == 0) {
                break;
            }
        }
        return e;
    }

    static int jumpingOnClouds2(int[] c, int k) {
        int e = 100;
        int i = 0;
        do {
            i = (i + k) % c.length;
            e -= c[i] * 2 + 1;
        } while (i != 0 && e != 0);
        return e;
    }

}
