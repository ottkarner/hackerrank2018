package _20181217_w007;

public class _000038_AppendAndDelete {

    public static void main(String[] args) {
        System.out.println(appendAndDelete("abc", "def", 6));
        System.out.println(appendAndDelete("hackerhappy", "hackerrank", 9));
        System.out.println(appendAndDelete("hackerhappy", "hackerrank", 10));
        System.out.println(appendAndDelete("aba", "aba", 9));
        System.out.println(appendAndDelete("ashley", "ash", 2));
    }

    // Complete the appendAndDelete function below.
    static String appendAndDelete(String s, String t, int k) {
        if (k >= (s.length() + t.length())) {
            return "Yes";
        } else {
            int differenceStart = 0;
            while (differenceStart < s.length() &&
                    differenceStart < t.length() &&
                    s.charAt(differenceStart) == t.charAt(differenceStart)) {
                differenceStart++;
            }
            int difference = (s.length() - differenceStart) + (t.length() - differenceStart);
            if (k >= difference && (k - difference) % 2 == 0) {
                return "Yes";
            } else {
                return "No";
            }
        }
    }

}
