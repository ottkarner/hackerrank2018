package _20181217_w007;

public class _000041_LibraryFine {

    public static void main(String[] args) {
        System.out.println(libraryFine(1, 1, 2018, 1, 1, 2017));
        System.out.println(libraryFine(1, 1, 2018, 31, 12, 2017));
        System.out.println(libraryFine(9, 6, 2015, 6, 6, 2015));
    }

    // Complete the libraryFine function below.
    static int libraryFine(int d1, int m1, int y1, int d2, int m2, int y2) {
        if (y1 - y2 < 0) {
            return 0;
        } else if (y1 - y2 == 0) {
            if (m1 - m2 < 0) {
                return 0;
            } else if (m1 - m2 == 0) {
                if (d1 - d2 <= 0) {
                    return 0;
                } else {
                    return (d1 - d2) * 15;
                }
            } else {
                return (m1 - m2) * 500;
            }
        } else {
            return 10000;
        }
    }

    static int libraryFine2(int d1, int m1, int y1, int d2, int m2, int y2) {
        if (y1 > y2) {
            return 10000;
        } else if (y1 == y2) {
            if (m1 > m2) {
                return (m1 - m2) * 500;
            } else if (m1 == m2) {
                if (d1 > d2) {
                    return (d1 - d2) * 15;
                }
            }
        }
        return 0;
    }

    static int libraryFine3(int d1, int m1, int y1, int d2, int m2, int y2) {
        if (y1 > y2) return 10000;
        if (y1 < y2) return 0;
        if (m1 > m2) return (m1 - m2) * 500;
        if (m1 < m2) return 0;
        if (d1 > d2) return (d1 - d2) * 15;
        return 0;
    }

}
