package _20181217_w007;

public class _000047_TaumAndBday {

    public static void main(String[] args) {
        int[][] testCases = {
                {10, 10, 1, 1, 1},
                {5, 9, 2, 3, 4},
                {3, 6, 9, 1, 1},
                {7, 7, 4, 2, 1},
                {3, 3, 1, 9, 2}
        };
        for (int[] testCase : testCases) {
            System.out.println(taumBday(testCase[0], testCase[1], testCase[2], testCase[3], testCase[4]));
            System.out.println(taumBday2(testCase[0], testCase[1], testCase[2], testCase[3], testCase[4]));
        }
//        System.out.println((long) Math.pow(10, 9));
//        System.out.println(Integer.MAX_VALUE);
    }

    // Complete the taumBday function below.
    private static long taumBday(long b, long w, long bc, long wc, long z) {
        if (bc + z < wc) {
            return b * bc + w * (bc + z);
        }
        if (bc > wc + z) {
            return b * (wc + z) + w * wc;
        }
        return b * bc + w * wc;
    }

    private static long taumBday2(long b, long w, long bc, long wc, long z) {
        long nbc = bc < wc + z ? bc : wc + z;
        long nwc = wc < bc + z ? wc : bc + z;
        return b * nbc + w * nwc;
    }

}
